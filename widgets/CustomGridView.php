<?php

namespace app\widgets;

use Yii;
use yii\grid\GridView;
use yii\helpers\Html;

class CustomGridView extends GridView
{
	public $create=false;
	public $bulkdelete=false;
	public $export=false;
	public $import=false;
    /**
     * Initializes the grid view.
     * This method will initialize required property values and instantiate [[columns]] objects.
     */
    public function init()
    {
		$filterPageSizeHtml='';
		if($this->filterModel!=null){
			$this->filterSelector = '#' . Html::getInputId($this->filterModel, 'pageSize');
			$this->filterSelector = '#filter-pageSize';
			$filterPageSizeHtml='<div class="col-xs-2 col-sm-2 col-md-1 pull-right text-right">
				'.Html::activeDropDownList($this->filterModel, 'pageSize', Yii::$app->params['pageSizeArray'], ['id'=>'filter-pageSize', 'class'=>'form-control input-sm']).'
			</div>';
		}
		$Btns='';
		if($this->create==true){
			$Btns.=Html::a(Yii::t('app','New'),['create'],['class'=>'btn btn-sm btn-success pull-left']);
		}
		if($this->bulkdelete==true){
			echo '
<script>			
function deleteBulk(){
	if(confirm("Are you sure you want to delete selected bookings")){
		var allVals = [];
		$(".cb:checked").each(function() {
		   allVals.push($(this).val());
		 });
		 $("#bulk-idz").val(allVals);
		document.getElementById("bulkForm").submit();
	}
}
</script>';
			$Btns.='<div class="pull-left" style="margin-left:10px;">'.Html::beginForm().'
				<input type="hidden" id="bulk-action" name="action" value="bdelete" />
	            <input type="hidden" id="bulk-idz" name="selection" value="">
				<button type="submit" class="btn btn-sm btn-danger" onclick="javascript:deleteBulk();">'.Yii::t('app','Delete Selected').'</button>
				'.Html::endForm().'
				</div>
			';
		}
		if($this->import==true){
			$Btns.=Html::a(Yii::t('app','Import'),['import'],['class'=>'btn btn-sm btn-success pull-left']);
		}
		if($this->export==true && $this->filterModel!=null){
			$this->filterModel->export=1;
			$Btns.='<div class="pull-left" style="margin-left:10px;">'.Html::beginForm().'
				<button type="submit" class="btn btn-sm btn-primary">'.Yii::t('app','Export').'</button>
				'.Html::activeHiddenInput($this->filterModel, 'export').'
				'.Html::endForm().'
				</div>
			';
		}
		$this->layout='
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-xs-6"><div class="clearfix">'.$Btns.'</div></div>
					'.$filterPageSizeHtml.'
				</div>
			</div>
			<div class="box-body">
				<div class="table-scrollable">{items}</div>
			</div>
			<div class="box-footer clearfix">
				<div class="row">
					<div class="col-sm-5">{summary}</div>
					<div class="col-sm-7" align="right">{pager}</div>
				</div>
			</div>
		</div>';
        parent::init();
    }
}
