<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=bookingelite_live_1',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    'enableSchemaCache' => false,
   /* 'schemaCacheDuration' => 3600,
    'schemaCache' => 'cache',*/
];
