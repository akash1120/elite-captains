<?php

return [
    'adminEmail' => 'admin@example.com',
    'siteUrl' => 'http://local.elite-captains/',
    'siteName' => 'Elite Captains Booking',

    'supportEmail' => 'cs.akashahmed@gmail.com',
    'bookingAdminEmail' => 'cs.akashahmed@gmail.com',
    /*
    'supportEmail' => 'no-reply@booking.elitecaptains.ae',
    'bookingAdminEmail' => 'bookings@elitecaptains.ae',
    'bookingAdminEmail' => 'malick.naeem@gmail.com',
	*/

    'user.passwordResetTokenExpire' => 36000,

    'cache_abs_path' => dirname(dirname(__DIR__)) . '/local.elite-captains/images/cache/',
    'cache_rel_path' => 'images/cache/',

    'avatar_abs_path' => dirname(dirname(__DIR__)) . '/local.elite-captains/images/avatar/',
    'avatar_rel_path' => 'images/avatar/',

    'boat_abs_path' => dirname(dirname(__DIR__)) . '/local.elite-captains/uploads/boats/',
    'boat_rel_path' => 'uploads/boats/',

    'defaultPhoto' => 'images/default_image.png',
    'default_image' => 'images/default_image.png',
    'default_avatar' => 'images/avatar.png',

    // Testing Credentials
    // MID: 10037618
    // Secret: rV6b1tW0s26aGK3VWiuxLOfWielpfQBGMISNbONDQUOxERsBaX9VMpR5DduS35zPNrDZfrC6lrD0j6Bt3KoQ9gVMtpFxvfmjG0iW

	'paytabsId' => '10037618',
	'paytabsSecret' => 'rV6b1tW0s26aGK3VWiuxLOfWielpfQBGMISNbONDQUOxERsBaX9VMpR5DduS35zPNrDZfrC6lrD0j6Bt3KoQ9gVMtpFxvfmjG0iW',
	'app_vat' => '1000922233444',
	'app_phone' => '+971 234 56765',

	'cityIdz' => [
		'abudhabi' => 1,
		'dubai' => 2,
	],
	
	'goldenSharePackageId'=>3,
	'packageDurationArr'=>[
		'365' => 'One Year',
		'182' => 'Half Year',
	],
	
	'isLicensedArr' => [
		'1' => 'Yes',
		'0' => 'No',
	],
	
	'statusArr' => [
		'1' => 'Enable',
		'0' => 'Disable',
	],
	
	'pageSizeArray' => [
		'25' => 25,
		'50' => 50,
		'75' => 75,
		'100' => 100,
	],
	
	'noticeClass' => [
		'info' => 'Blue',
		'success' => 'Green',
		'danger' => 'Red',
		'warning' => 'Yellow',
	],
	
	'sharingPackageId' => 3,
    'statusFreeze' => []
];
function formatDate($date){
    return \Yii::$app->formatter->asDate($date, 'php:D, d M, Y');
}
