<?php
namespace app\assetfiles;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/bootstrap-toastr/toastr.min.css',
        'plugins/sweetalert2/sweetalert2.min.css',
        'css/site.css?v=1.0.0',
    ];
    public $js = [
        'plugins/bootstrap-toastr/toastr.min.js',
        'plugins/sweetalert2/sweetalert2.min.js',
		'plugins/jquery.blockui.min.js',
		'js/custom.js',
		'js/ajax-modal-popup.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
		'yii\bootstrap\BootstrapPluginAsset',
		'yii\bootstrap\BootstrapThemeAsset',
		'app\assetfiles\FontAwesomeAsset',
		'app\assetfiles\ThemeAsset',
    ];
}
