<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use app\models\FreezeContract;
use app\models\Contracts;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CronController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex()
    {
       
       /* $cheques = PaymentCheques::find()->where(['and',['<','DATE(date)',date("Y-m-d")],['status'=>'pending']])->asArray()->all();
        if(isset($cheques) && !empty($cheques)){
            foreach ($cheques as $key => $cheque){
                $cheque_detail = PaymentCheques::findOne($cheque['id']);
                if(!empty($cheque_detail))  $contract = Contracts::findOne($cheque_detail->payment->invoice->contract_id);

                if (!empty($contract)) {
                    if ($contract->status == 'active') {
                        \Yii::$app->db->createCommand('UPDATE contracts SET status = "declined" WHERE id ='.$contract->id)->query();
                    }
                }
                if ($cheque_detail->status == 'pending') {
                    \Yii::$app->db->createCommand('UPDATE payment_cheques SET status = "declined" WHERE id ='.$cheque_detail->id)->query();
                }
            }
        }
        echo "hello";*/
        $current_date = date('Y-m-d');

        /*freeze contracts start*/
        $going_to_freez = FreezeContract::find()
            ->where(['=', 'start_date', $current_date])
            ->andWhere(['status' => 'active'])
            ->all();

        foreach ($going_to_freez as $key => $item){
            $contract_details = Contracts::findOne($item['contract_id']);
            if( strtotime($contract_details->current_expiry) >= strtotime($item['start_date'])){
                \Yii::$app->db->createCommand('UPDATE contracts SET is_freeze = 1 WHERE id ='.$item['contract_id'])->query();
            }
        }

        /*freeze contracts end*/

        /*Un freeze contracts start*/
        $going_to_unfreeze = FreezeContract::find()
            ->where(['=', 'end_date', $current_date])
            ->andWhere(['status' => 'active'])
            ->all();

        foreach ($going_to_unfreeze as $key => $item1){
            $contract_details = Contracts::findOne($item1['contract_id']);
            if( strtotime($contract_details->current_expiry) >= strtotime($item1['end_date'])){
                \Yii::$app->db->createCommand('UPDATE contracts SET is_freeze = 0 WHERE id ='.$item1['contract_id'])->query();
            }
            \Yii::$app->db->createCommand('UPDATE contracts SET status = "cancelled" WHERE id ='.$item1['id'])->query();
        }

        /*Un freeze contracts start*/
        $going_to_unfreeze = FreezeContract::find()
            ->where(['<=', 'end_date', $current_date])
            ->andWhere(['status' => 'active'])
            ->all();

        foreach ($going_to_unfreeze as $key => $item1){
            \Yii::$app->db->createCommand('UPDATE freeze_contract SET status = "completed" WHERE id ='.$item1['id'])->query();
        }
        /*Un freeze contracts end*/

        /*Closed contracts start*/
        $going_to_closed = Contracts::find()
            ->where(['<=', 'current_expiry', $current_date])
            ->andWhere(['status' => 'active'])
            ->all();


        foreach ($going_to_closed as $key => $item2){


                \Yii::$app->db->createCommand('UPDATE contracts SET status = "closed" WHERE id ='.$item2['id'])->query();

        }
        /*Closed contracts end*/

    }

    public function actionContractscron()
    {
        $msg = "First line of text\nSecond line of text";

// use wordwrap() if lines are longer than 70 characters
        $msg = wordwrap($msg,70);

// send email
        mail("akash@wistech.biz","My subject",$msg);
    }
}
