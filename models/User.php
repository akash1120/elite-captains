<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;

class User extends ActiveRecord implements IdentityInterface
{
	public $oldPackageId,$new_password;
	public $sub_user_id,$sub_city_id,$sub_username,$sub_new_password,$sub_email,$sub_mobile,$sub_is_licensed,$sub_image,$sub_imageSrc,$sub_notice,$sub_notice_class;
	
	public $oldimage,$imagefile,$oldsubimage,$subimagefile;
	public $allowedImageSize = 512000;
	public $allowedImageTypes=['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];
	
    const STATUS_ACTIVE = 1;
    const STATUS_BLOCKED = 0;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }
	
    /**
     * @inheritdoc
     */
	public function behaviors()
    {
        return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
                    return date("Y-m-d H:i:s"); 
                },
			],
			'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
		];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['user_type','city_id','username','email','mobile','postal_code','address','first_name','last_name'], 'required'],
			[['user_type','parent','city_id','is_licensed','package_id','status','sub_user_id','sub_city_id','sub_is_licensed'], 'integer'],
			[['username', 'email', 'mobile', 'notice', 'notice_class', 'sub_username', 'sub_email', 'sub_mobile', 'sub_notice', 'sub_notice_class','postal_code','address','first_name','last_name','vat_number'], 'string'],
			[['package_start', 'package_end','date_of_birth','registration_no'], 'safe'],
			[['email'], 'email'],
			[['new_password','sub_new_password'], 'string', 'min' => 6],
			['email','unique','filter' => ['trashed' => 0],'message'=>Yii::t('app','Email "{value}" is already registered')],
			['username','unique','filter' => ['trashed' => 0],'message'=>Yii::t('app','Username "{value}" is already registered')],
            ['user_type', 'default', 'value' => 0],
            ['status', 'default', 'value' => self::STATUS_BLOCKED],
            ['status', 'in', 'range' => [self::STATUS_BLOCKED,self::STATUS_ACTIVE]],
            [['image','sub_image'], 'file', 'extensions'=>implode(",",$this->allowedImageTypes)],
        ];
    }
	
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city_id' => Yii::t('app', 'City'),
            'package_id' => Yii::t('app', 'Package'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'mobile' => Yii::t('app', 'Mobile'),
            'notice' => Yii::t('app', 'Note'),
			'notice_class' => Yii::t('app', 'Note Color'),
            'new_password' => Yii::t('app', 'Password'),
            'is_licensed' => Yii::t('app', 'Licensed'),
			'txtLicensed' => Yii::t('app', 'Licensed'),
            'txtStatus' => Yii::t('app', 'Status'),
            'sub_city_id' => Yii::t('app', 'City'),
            'sub_username' => Yii::t('app', 'Username'),
            'sub_email' => Yii::t('app', 'Email'),
            'sub_mobile' => Yii::t('app', 'Mobile'),
            'sub_new_password' => Yii::t('app', 'Password'),
            'sub_is_licensed' => Yii::t('app', 'Licensed'),
            'sub_notice' => Yii::t('app', 'Note'),
			'sub_notice_class' => Yii::t('app', 'Note Color'),
        ];
    }
	
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
		return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE, 'trashed' => 0]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
		if($token!=null){
			return static::findOne(['auth_key' => $token, 'status' => self::STATUS_ACTIVE, 'trashed' => 0]);
		}else{
			return null;
		}
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE, 'trashed' => 0]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
			'trashed' => 0
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
		return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
	
    /**
     * return html status
     */
	public function getTxtStatus()
	{
		return $this->status==1 ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Blocked</span>';
	}
	
    /**
     * return html is licensed
     */
	public function getTxtLicensed()
	{
		return $this->is_licensed==1 ? '<span class="label label-success">Yes</span>' : '<span class="label label-warning">No</span>';
	}

    /**
     * Get city row
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * Get parent user row
     * @return \yii\db\ActiveQuery
     */
    public function getParentUser()
    {
        return $this->hasOne(User::className(), ['id' => 'parent']);
    }

    /**
     * Get package row
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        if($this->parent==0){
            $user_id = $this->id;
        }else{
            $user_id = $this->parent;
        }
        $current_contract = Contracts::find()->where([
            'user_id' => $user_id,
            //'status' => 'active'
        ])->orderBy(['id' => SORT_DESC])->one();

        if(isset($current_contract->package_id) && $current_contract->package_id <> null){
            return Package::findOne($current_contract->package_id);
        }
		/*if($this->parent==0){
			return Package::findOne($this->package_id);
		}else{
			return Package::findOne($this->parentUser->package_id);
		}*/
    }
	
    /**
     * Get package history
     * @return \yii\db\ActiveQuery
     */
    public function getPackageHistory()
    {
        return $this->hasMany(UserPackage::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubUsers()
    {
        return $this->hasMany(User::className(), ['parent' => 'id']);
    }
	
    /**
     * Get Sub Users Usernames
     * @return string
     */
	public function getSubUserNames()
	{
		$usrArr=[];
		if($this->subUsers!=null){
			foreach($this->subUsers as $subUser)
			{
				$usrArr[]=$subUser->username;
			}
		}
		return ($usrArr!=null && count($usrArr)>1 ? implode(", ",$usrArr) : (isset($usrArr[0]) ? ' & '.$usrArr[0] : ''));
	}
	
    /**
     * Get user idz sub and self
     * @return \yii\db\ActiveQuery
     */
	public function getUserIdz()
	{
		if($this->parent==0){
			$userIdz[]=$this->id;
			$subUsers=$this->subUsers;
			if($subUsers!=null){
				foreach($subUsers as $subUser){
					$userIdz[]=$subUser->id;
				}
			}
			return $userIdz;
		}else{
			$parentUser=$this->parentUser;
			$userIdz[]=$this->parent;
			$subUsers=$parentUser->subUsers;
			if($subUsers!=null){
				foreach($subUsers as $subUser){
					$userIdz[]=$subUser->id;
				}
			}
			return $userIdz;
		}
	}
	
    /**
     * Get All the users email, parent/sub
     * @return string
     */
	public function getUserEmails()
	{
		if($this->parent==0){
			$emailIdz[]=$this->email;
			$subUsers=$this->subUsers;
			if($subUsers!=null){
				foreach($subUsers as $subUser){
					$emailIdz[]=$subUser->email;
				}
			}
			return $emailIdz;
		}else{
			$parentUser=$this->parentUser;
			$emailIdz[]=$parentUser->email;
			$subUsers=$parentUser->subUsers;
			if($subUsers!=null){
				foreach($subUsers as $subUser){
					$emailIdz[]=$subUser->email;
				}
			}
			return $emailIdz;
		}
	}
	
    /**
     * Get package start date
     * @return string
     */
	public function getParentPackageStartDate()
	{
		if($this->parent==0){
			return $this->package_start;
		}else{
			$parentUser=$this->parentUser;
			return $parentUser->package_start;
		}
	}

    /**
     * Get package expiry date
     * @return string
     */
	public function getParentUserId()
	{
		if($this->parent==0){
			return $this->id;
		}else{
			$parentUser=$this->parentUser;
			return $parentUser->id;
		}
	}
	
    /**
     * Get package expiry date
     * @return string
     */
	public function getParentPackageEndDate()
	{

        if($this->parent==0){
            $user_id = $this->id;
        }else{
            $user_id = $this->parent;
        }
        $current_contract = Contracts::find()->where([
            'user_id' => $user_id,
           // 'status' => 'active'
        ])->orderBy(['id' => SORT_DESC])->one();
        if( isset($current_contract->end_date) && $current_contract->end_date <> null){
            if($current_contract->current_expiry <> null) {
                return $current_contract->current_expiry;
            }else{
                return $current_contract->end_date;
            }
        }
       // return '-';
		/*if($this->parent==0){
			return $this->package_end;
		}else{
			$parentUser=$this->parentUser;
			return $parentUser->package_end;
		}*/
	}

	public function getExpiryStatus()
	{
		$html='';
		$expiry_date=$this->parentPackageEndDate;
		if($expiry_date<date("Y-m-d")){
			$txtLabel= 'Expired';
			$txtLabelCls= 'warning';
		}else{
			$txtLabel= 'Expiry';
			$txtLabelCls= 'info';
		}
		if(isset($expiry_date)){
            $html='<br /><small><span class="label label-'.$txtLabelCls.'">'.$txtLabel.': '.Yii::$app->formatter->asDate($expiry_date).'</span></small>';
        }else{
            //$html='<br /><small><span class="label label-'.$txtLabelCls.'">'.$txtLabel.': '.Yii::$app->formatter->asDate($expiry_date).'</span></small>';
        }

		//$html='<br /><small><span class="label label-'.$txtLabelCls.'">'.$txtLabel.': '.Yii::$app->formatter->asDate($expiry_date).'</span></small>';
		return $html;
	}

	public function getRenewedPackage()
	{
		return UserPackage::find()
			->where([
				'and',
				['user_id'=>$this->parentUserId,'status'=>1,'trashed'=>0],
				['>','start_date',$this->parentPackageEndDate]
			])
			->orderBy(['start_date'=>SORT_ASC])->asArray()->one();
	}

	public function getRenewedStatus()
	{
		$html='';
		return $html;
		$renewedPackage = $this->renewedPackage;
		if($renewedPackage!=null){
			$html='<br /><small><span class="label label-success">Renewd: '.Yii::$app->formatter->asDate($renewedPackage['end_date']).'</span></small>';
		}
		return $html;
	}

    /**
     * @return boolean
     */
    public function getPackageService($keyword)
    {
		$permission=false;
		$services=ArrayHelper::map(Service::find()->all(),'keyword','id');
		$packageId=$this->package_id;
		if($this->parent!=0){
			$parentUser=User::findOne($this->parent);
			$packageId=$parentUser->package_id;
		}
		
		$result=PackageToService::find()->where(['package_id'=>$packageId,'service_id'=>$services[$keyword]])->one();
		if($result!=null){
			$permission=true;
		}
        return $permission;
    }
	
    /**
     * return login count
     */
	public function getLoginCount()
	{
		return UserLoginHistory::find()->where(['user_id'=>$this->id])->count('id');
	}
	
    /**
     * return last login date
     */
	public function getLastloginDate()
	{
		$result=UserLoginHistory::find()->where(['user_id'=>$this->id, 'login'=>1])->orderBy('id desc')->asArray()->limit(1)->offset(1)->one();
		if($result!=null){
			return $result->created_at;
		}else{
			return null;
		}
	}
	
    /**
     * @return image path
     */
	public function getImageSrc()
	{
		$image=Yii::$app->params['default_avatar'];
		if($this->image!=null && file_exists(Yii::$app->params['avatar_abs_path'].$this->image)){
			$image=Yii::$app->helperFunction->resize(Yii::$app->params['avatar_abs_path'], $this->image, 128, 128);
		}
		return $image;
	}
	
    /**
     * @return string | html notice for current user
     */
	public function getNoticeText()
	{
		$notice='';
		if($this->notice!=null && $this->notice!=''){
			$notice = $this->notice;
		}else{
			if($this->parentUser!=null){
				$notice = $this->parentUser->notice;
			}
		}
		return $notice!='' ? '<div style="padding: 15px;margin: 0;padding-bottom: 0;"><div class="callout callout-'.($this->notice_class!=null && $this->notice_class!='' ? $this->notice_class : 'info').'" style="margin: 0;"><i class="fa fa-bullhorn"></i>&nbsp;&nbsp;'.$notice.'</div></div>' : '';
	}
	
    /**
	 * Logs activity of user
     * @return boolean
     */
	public function LogActivity($source,$source_id,$action_type,$descp=null)
	{
		$model=new UserActivity;
		$model->source=$source;
		$model->source_id=$source_id;
		$model->action_type=$action_type;
		if($model->save()){
			return true;
		}else{
			return false;
		}
	}

    /**
     * @inheritdoc
     */
	public function beforeValidate()
	{	
		//Uploading image
		if(UploadedFile::getInstance($this, 'image')){
			$this->imagefile = UploadedFile::getInstance($this, 'image');
			// if no file was uploaded abort the upload
			if (!empty($this->imagefile) && file_exists($this->imagefile->tempName)) {
				$pInfo=pathinfo($this->imagefile->name);
				$ext = $pInfo['extension'];
				if (in_array($ext,$this->allowedImageTypes)) {
					// Check to see if any PHP files are trying to be uploaded
					$content = file_get_contents($this->imagefile->tempName);
					if (preg_match('/\<\?php/i', $content)) {
						$this->addError('image', Yii::t('app', 'Invalid file provided!'));
						return false;
					}else{
						if (filesize($this->imagefile->tempName) <= $this->allowedImageSize) {
							// generate a unique file name
							$this->image = Yii::$app->controller->generateName().".{$ext}";
						}else{
							$this->addError('image', Yii::t('app', 'File size is too big!'));
							return false;
						}
					}
				}else{
					$this->addError('image', Yii::t('app', 'Invalid file provided!'));
					return false;
				}
			}
		}
		if($this->image==null && $this->oldimage!=null){
			$this->image=$this->oldimage;
		}
		
		//Uploading Sub image
		if(UploadedFile::getInstance($this, 'sub_image')){
			$this->subimagefile = UploadedFile::getInstance($this, 'sub_image');
			// if no file was uploaded abort the upload
			if (!empty($this->subimagefile) && file_exists($this->subimagefile->tempName)) {
				$pInfo=pathinfo($this->subimagefile->name);
				$ext = $pInfo['extension'];
				if (in_array($ext,$this->allowedImageTypes)) {
					// Check to see if any PHP files are trying to be uploaded
					$content = file_get_contents($this->subimagefile->tempName);
					if (preg_match('/\<\?php/i', $content)) {
						$this->addError('sub_image', Yii::t('app', 'Invalid file provided!'));
						return false;
					}else{
						if (filesize($this->subimagefile->tempName) <= $this->allowedImageSize) {
							// generate a unique file name
							$this->sub_image = Yii::$app->controller->generateName().".{$ext}";
						}else{
							$this->addError('sub_image', Yii::t('app', 'File size is too big!'));
							return false;
						}
					}
				}else{
					$this->addError('image', Yii::t('app', 'Invalid file provided!'));
					return false;
				}
			}
		}
		if($this->sub_image==null && $this->oldsubimage!=null){
			$this->sub_image=$this->oldsubimage;
		}
		return parent::beforeValidate();
	}
	
    /**
     * @inheritdoc
     */
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if($this->new_password!=null){
				$this->password=$this->new_password;
			}
			if($this->auth_key==null){
				$this->generateAuthKey();
			}
			return true;
		} else {
			return false;
		}
	}

    /**
     * @inheritdoc
     */
	public function afterSave($insert, $changedAttributes)
	{
		if ($this->imagefile!== null && $this->image!=null) {
			if($this->oldimage!=null && $this->image!=$this->oldimage && file_exists(Yii::$app->params['avatar_abs_path'].$this->oldimage)){
				unlink(Yii::$app->params['avatar_abs_path'].$this->oldimage);
			}
			$this->imagefile->saveAs(Yii::$app->params['avatar_abs_path'].$this->image);
		}


		$checkPackageHistory=UserPackage::find()
		->where([
			'user_id'=>$this->id,
			'package_id'=>$this->package_id,
			'start_date'=>$this->package_start,
			'end_date'=>$this->package_end,
		])
		->one();
		if($checkPackageHistory==null){
			$userPackage=new UserPackage;
			$userPackage->user_id=$this->id;
			$userPackage->package_id=$this->package_id;
			$userPackage->start_date=$this->package_start;
			$userPackage->end_date=$this->package_end;
			$userPackage->save();
		}
		if($this->parent==0 && $this->package_id==Yii::$app->params['sharingPackageId']){
			$isNew=false;
			if($this->sub_user_id!=null && $this->sub_user_id>0){
				$subUser=User::findOne($this->sub_user_id);
				if($subUser==null){
					$isNew=true;
				}
			}else{
				$isNew=true;
			}
			if($isNew==true){
				$subUser=new User;
			}
			$subUser->user_type=$this->user_type;
			$subUser->parent=$this->id;
			$subUser->city_id=$this->sub_city_id;
			$subUser->username=$this->sub_username;
			$subUser->password=$this->sub_new_password;
			$subUser->image=$this->sub_image;
			$subUser->email=$this->sub_email;
			$subUser->mobile=$this->sub_mobile;
			$subUser->is_licensed=$this->sub_is_licensed;
			$subUser->status=$this->status;
			$subUser->package_id=$this->package_id;
			$subUser->package_start=$this->package_start;
			$subUser->package_end=$this->package_end;
			$subUser->notice=$this->sub_notice;
			$subUser->notice_class=$this->sub_notice_class;
			$subUser->generateAuthKey();
			if($subUser->save()){
				if ($this->subimagefile!== null && $this->sub_image!=null) {
					if($this->oldsubimage!=null && $this->sub_image!=$this->oldsubimage && file_exists(Yii::$app->params['avatar_abs_path'].$this->oldsubimage)){
						unlink(Yii::$app->params['avatar_abs_path'].$this->oldsubimage);
					}
					$this->subimagefile->saveAs(Yii::$app->params['avatar_abs_path'].$this->sub_image);
				}
			}
		}
		if (!Yii::$app->user->isGuest) {
			if($insert){
				Yii::$app->user->identity->LogActivity('user',$this->id,'create');
			}else{
				Yii::$app->user->identity->LogActivity('user',$this->id,'update');
			}
		}
		parent::afterSave($insert, $changedAttributes);
	}
	
    /**
     * Mark record as deleted and hides fron list.
     * @return boolean
     */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand("update ".self::tableName()." set trashed='1',trashed_at='".date("Y-m-d H:i:s")."',trashed_by='".Yii::$app->user->identity->id."' where id='".$this->id."'")->execute();
		Yii::$app->user->identity->LogActivity('user',$this->id,'delete');
		return true;
	}
}
