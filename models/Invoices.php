<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%invoices}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $invoice_id
 * @property int $contract_id
 * @property string $invoice_date
 * @property string $subtotal
 * @property string $tax
 * @property string $total
 * @property string $status
 * @property int $created_by
 * @property string $invoice_description
 * @property string $admin_notes
 * @property string $client_notes
 * @property string $invoice_terms
 * @property string $billing_cycle
 * @property string $last_payment_date
 * @property string $update_at
 * @property string $create_at
 */
class Invoices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%invoices}}';
    }
    public $saved_addons = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'invoice_id', 'contract_id', 'subtotal', 'tax', 'total', 'created_by'], 'required'],
            [['user_id', 'invoice_id', 'contract_id', 'created_by'], 'integer'],
            [['invoice_date', 'last_payment_date', 'update_at', 'create_at','joining_fee', 'invoice_description', 'admin_notes', 'client_notes', 'invoice_terms'], 'safe'],
            [['status'], 'string'],
            [['subtotal', 'tax', 'total', 'billing_cycle'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
                        'user_id' => Yii::t('app', 'User ID'),
                        'invoice_id' => Yii::t('app', 'Invoice ID'),
                        'contract_id' => Yii::t('app', 'Contract ID'),
                        'invoice_date' => Yii::t('app', 'Invoice Date'),
                        'subtotal' => Yii::t('app', 'Subtotal'),
                        'tax' => Yii::t('app', 'Tax'),
                        'total' => Yii::t('app', 'Total'),
                        'status' => Yii::t('app', 'Status'),
            			'txtStatus' => Yii::t('app', 'Status'),
			            'created_by' => Yii::t('app', 'Created By'),
                        'invoice_description' => Yii::t('app', 'Invoice Description'),
                        'admin_notes' => Yii::t('app', 'Admin Notes'),
                        'client_notes' => Yii::t('app', 'Client Notes'),
                        'invoice_terms' => Yii::t('app', 'Invoice Terms'),
                        'billing_cycle' => Yii::t('app', 'Billing Cycle'),
                        'last_payment_date' => Yii::t('app', 'Last Payment Date'),
                        'update_at' => Yii::t('app', 'Update At'),
                        'create_at' => Yii::t('app', 'Create At'),
                    ];
    }
	
    /**
     * return html status
     */
	public function getTxtStatus()
	{
	    if($this->status=='paid'){
	        return '<span class="label label-success">Paid</span>';
        }else if($this->status=='pending'){
            return  '<span class="label label-warning">Pending</span>';
        }else if($this->status=='partial'){
            return  '<span class="label label-warning">Partial</span>';
        } else{
            return '<span class="label label-danger">Cancelled</span>';
        }
	}

    /**
     * @param bool  $insert
     * @param array $changedAttributes
     */
	public function afterSave($insert, $changedAttributes)
    {
        $invoice_number = new InvoiceNumbers();
        $invoice_number->number = $this->invoice_id;
        $invoice_number->save();
    }

    /**
     * @return user
     */
    public function getUser(){
	    return $this->hasOne(User::className(),['id' => 'user_id']);
    }
    /**
     * @return contract
     */
    public function getContract(){
        return $this->hasOne(Contracts::className(),['id' => 'contract_id']);
    }
    /**
     * @return billing_cycle
     */
    public function getPaymentTerms(){
        return $this->hasOne(PaymentTerms::className(),['id' => 'billing_cycle']);
    }

    /**
     * Send booking email.
     * @return boolean
     */
    public function sendInvoiceEmail($model,$terms)
    {
        $model->user->email = 'akash@wistech.biz';
        //Send Emails to member
        $resp =\Yii::$app->mailer->compose(['html' => 'newInvoice-html', 'text' => 'newInvoice-html'], ['invoice' => $model,'terms' => $terms])
            ->setFrom([\Yii::$app->params['supportEmail'] => Yii::$app->params['siteName'] . ''])
            /*->setTo(Yii::$app->user->identity->userEmails)*/
            ->setTo($model->user->email)
            ->setSubject('New Invoice INV-'.$model->invoice_id)
            ->send();
        return true;
    }

}
