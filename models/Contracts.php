<?php

namespace app\models;

use app\helpers\HelperFunction;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%Contracts}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $package_id
 * @property string $start_date
 * @property string $end_date
 * @property int $payment_method_id
 * @property int $payment_term_id
 * @property string $check_number
 * @property string $check_date
 * @property string $card_number
 * @property string $card_expiry_date
 * @property string $cvv
 * @property int $free_days
 * @property string $status
 * @property int $created_by
 * @property string $update_at
 * @property string $create_at
 */
class Contracts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contracts}}';
    }

    public $addons = [];
    public $cities = [];
    public $saved_addons = [];
    public $saved_cities;
    public $addon_id;
    public $addons_quantity;
    public $addons_price;
    public $renewal_in;
    public $renewal_duration;
    public $already_existing_contract;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'package_id', 'payment_method_id', 'payment_term_id', 'created_by', 'price', 'free_days', 'start_date', 'end_date'], 'required'],
            [['user_id', 'package_id', 'payment_method_id', 'payment_term_id', 'created_by','remaining_freeze_days','is_freeze','sub_user'], 'integer'],
            [['addon_id', 'addons', 'addons_quantity', 'addons_price', 'check_date','joining_fee', 'card_expiry_date', 'update_at', 'create_at','addon_name','frequency','recurring_amount','current_expiry','first_contract_check','remaining_freeze_days','is_freeze','sub_user','renewal_in','renewal_duration','already_existing_contract','duration','cities'], 'safe'],
            [['status', 'terms', 'client_notes', 'admin_notes','frequency','recurring_amount','current_expiry'], 'string'],
            [['check_number', 'card_number', 'cvv'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User',
            'package_id' => 'Package',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'payment_method_id' => 'Payment Method',
            'payment_term_id' => 'Payment Term',
            'check_number' => 'Check Number',
            'check_date' => 'Check Date',
            'card_number' => 'Card Number',
            'card_expiry_date' => 'Card Expiry Date',
            'cvv' => 'Cvv',
            'free_days' => 'Freeze Days',
            'status' => 'Status',
            'txtStatus' => 'Status',
            'created_by' => 'Created By',
            'update_at' => 'Update At',
            'create_at' => 'Create At',
            'remaining_freeze_days' => 'Remaining Freeze Days',
            'current_expiry' => 'Current Expiry',
            'renewal_in' => 'Renewal In',
        ];
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            return $this->created_by = Yii::$app->user->id;
        }
        return false;
    }
    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if($this->user->new_member_check == 0){
            $this->first_contract_check = 1;
        }


        $current_user = $this->user_id ;
        if(  $this->user->parent != 0){
            $current_user = $this->user->parent;
        }
        $this->already_existing_contract = 1;
        if(!isset($this->id)) {
            $active_contract = Contracts::find()->where([
                'user_id' => $current_user,
                'status' => 'active'
            ])->all();
            if (count($active_contract) > 0) {
                $this->addError('', 'You already have an active contract. Please contact administrator for support');
                return false;
            }
            $this->already_existing_contract = 0;
        }

        if($this->isNewRecord){
            $this->remaining_freeze_days = $this->free_days;
        }
        if($this->current_expiry <> null) {
            //$this->current_expiry = $this->end_date;
        }else{
            $this->current_expiry = $this->end_date;
        }

        if($this->package_id==Yii::$app->params['sharingPackageId']){
            if($this->user_id == $this->sub_user){
                $this->sub_user = 0;
            }

        }



        /*$paymentTerm = PaymentTerms::findOne($this->payment_term_id);

        //frequency_days
        $end_date= strtotime($this->end_date); // or your date as well
        $start_date = strtotime($this->start_date);
        $datediff = $end_date - $start_date;

        $total_days =  round($datediff / (60 * 60 * 24));

        $this->frequency =  round($total_days/$paymentTerm->no_of_days);*/
        return true;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool
     */
    public function afterSave($insert, $changedAttributes)
    {

        if ($this->addons <> null) {

            ContarctAddons::deleteAll(['contract_id' => $this->id, 'customer_id' => $this->user_id]);
            $extra_addons = 0;
            foreach ($this->addons as $addon) {

                $contractAddons = new ContarctAddons();
                $contractAddons->contract_id = $this->id;
                $contractAddons->customer_id = $this->user_id;
                $contractAddons->addon_id = $addon['addon_id'];
                $contractAddons->qunatity = $addon['addons_quantity'];
                $contractAddons->price = $addon['addons_price'];

                if (!$contractAddons->save()) {
                    return false;
                }
            }
        }


        if ($this->cities <> null) {

            ContractCities::deleteAll(['contract_id' => $this->id]);
            foreach ($this->cities as $city) {
                $contractCities = new ContractCities();
                $contractCities->contract_id = $this->id;
                $contractCities->city_id = $city;

                if (!$contractCities->save()) {
                    return false;
                }
            }
        }

        /*on update end date , the current_expiry also updated*/
        if($this->current_expiry <> null) {
            $freez_days = $this->free_days - $this->remaining_freeze_days;
            if ($freez_days > 0) {
                $date_current_expiry = date('Y-m-d', strtotime($this->end_date . ' + ' . $freez_days . ' days'));
            }else {
                $date_current_expiry = date('Y-m-d', strtotime($this->end_date));
            }
            $connection = \Yii::$app->db;
            $connection->createCommand("update " . self::tableName() . " set current_expiry='" . $date_current_expiry . "' where id=" . $this->id)->execute();
        }

        /*on update end date , the current_expiry also updated*/
        /*   if($this->current_expiry <> null) {
               //$this->current_expiry = $this->end_date;
               if(isset($this->id) && $this->id <> null){
                   $days = HelperFunction::dateDifference($this->current_expiry, $this->end_date);
                   if($days >= 0) {
                       $date = date('Y-m-d', strtotime($this->current_expiry . ' + ' . $days . ' days'));
                       $connection = \Yii::$app->db;
                       $connection->createCommand("update " . self::tableName() . " set current_expiry='".$date."' where id=" . $this->id)->execute();
                   }
               }
           }*/

        if($this->already_existing_contract == 0) {
            $invoice = new Invoices();

            //Create Invoice
            $invoice = new Invoices();

            $last_invoice_number = 0;
            $invoice_number = InvoiceNumbers::find()->orderBy(['id' => SORT_DESC])->one();
            if ($invoice_number <> null) {
                $last_invoice_number = $invoice_number->number;
            }
            $price = $this->price;
            if($this->user->new_member_check == 0) {
                $price =  $this->price + $this->joining_fee;
            }


            $vat = HelperFunction::calculate_vat($price);

            $invoice->user_id = $this->user_id;
            $invoice->invoice_id = $last_invoice_number + 1;  //TODO: create a table and get inovice number from there
            $invoice->contract_id = $this->id;
            $invoice->invoice_date = date('Y-m-d');
            $invoice->subtotal = (string)$price;
            $invoice->tax = (string)$vat;
            $invoice->total = (string)($price + $vat);
            $invoice->status = "pending";
            $invoice->created_by = Yii::$app->user->id;
            $invoice->invoice_description = "";
            $invoice->admin_notes = $this->admin_notes;
            $invoice->client_notes = $this->client_notes;
            $invoice->invoice_terms = $this->terms;
            $invoice->billing_cycle = $this->payment_term_id;
            $invoice->last_payment_date = "";
            if($this->user->new_member_check == 0) {
                $invoice->joining_fee = $this->joining_fee;
            }


            if (!$invoice->save()) {
                return false;
            }
        }

        //user table update
        //update invoice status
        $user = User::findOne($this->user_id);
        $user->package_id = $this->package_id;
        $user->package_start = $this->start_date;
        $user->package_end = $this->end_date;
        // $user->current_expiry = $this->current_expiry;
        if($this->user->new_member_check == 0){
            $user->new_member_check = 1;
        }
        if (!$user->save()) {
            return false;
        }

        if($this->package_id==Yii::$app->params['sharingPackageId']){
            if($this->user_id != $this->sub_user){
                $subUser = User::findOne($this->sub_user);
                $subUser->package_id = $this->package_id;
                $subUser->package_start = $this->start_date;
                $subUser->package_end = $this->end_date;
                //$subUser->current_expiry = $this->current_expiry;
                $subUser->parent = $this->user_id;
                if (!$subUser->save()) {
                    return false;
                }
            }

        }

    }
    /**
     * return html status
     */
    public function getTxtStatus()
    {
        if($this->status=='active'){
            return '<span class="label label-success">Active</span>';

        }else if($this->status=='pending'){
            return  '<span class="label label-warning">Pending</span>';
        }else if($this->status=='declined'){
            return  '<span class="label label-warning">Declined</span>';
        }else if($this->status=='hold'){
            return  '<span class="label label-warning">Hold</span>';
        }else if($this->status=='cancel'){
            return  '<span class="label label-danger">Cancel</span>';
        } else{
            return '<span class="label label-danger">closed</span>';
        }
    }

    /**
     * return User
     */
    public function getUser(){
        return $this->hasOne(User::className(),['id' => 'user_id']);
    }
    /**
     * return Package
     */
    public function getPackage(){
        return $this->hasOne(Package::className(),['id' => 'package_id']);
    }
    /**
     * return Method
     */
    public function getPaymentMethod(){
        return $this->hasOne(PaymentMethods::className(),['id' => 'payment_method_id']);
    }
    /**
     * return Method
     */
    public function getPaymentTerm(){
        return $this->hasOne( PaymentTerms::className(),['id' => 'payment_term_id']);
    }
    public function getContractCities(){
        return $this->hasMany( ContractCities::className(),['contract_id' => 'id']);
    }
}
