<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%booking_to_addon}}".
 *
 * @property int $id
 * @property int $booking_id
 * @property int $addon_id
 */
class BookingToAddon extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%booking_to_addon}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['booking_id', 'addon_id'], 'required'],
            [['booking_id', 'addon_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
			'booking_id' => Yii::t('app', 'Booking ID'),
			'addon_id' => Yii::t('app', 'Addon ID'),
		];
    }

    /**
     * Get booking row
     * @return \yii\db\ActiveQuery
     */
    public function getBooking()
    {
        return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
    }

    /**
     * Get addon row
     * @return \yii\db\ActiveQuery
     */
    public function getAddon()
    {
        return $this->hasOne(Addon::className(), ['id' => 'addon_id']);
    }
}
