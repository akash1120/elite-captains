<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%package_to_service}}".
 *
 * @property int $id
 * @property int $package_id
 * @property int $service_id
 */
class PackageToService extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%package_to_service}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['package_id', 'service_id'], 'required'],
            [['package_id', 'service_id'], 'integer'],
        ];
    }

    /**
     * Get package row
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        return $this->hasOne(Package::className(), ['id' => 'package_id']);
    }

    /**
     * Get service row
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
			'package_id' => Yii::t('app', 'Package ID'),
			'service_id' => Yii::t('app', 'Service ID'),
		];
    }
}
