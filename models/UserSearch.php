<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form of `app\models\User`.
 */
class UserSearch extends User
{
	public $pageSize;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_type', 'parent', 'city_id', 'package_id', 'is_licensed', 'status', 'freeze_finish_alert', 'freeze_limit', 'created_by', 'updated_by', 'trashed', 'trashed_by', 'pageSize'], 'integer'],
            [['username', 'email', 'mobile', 'ip', 'auth_key', 'password_hash', 'image', 'freeze_start', 'freeze_end', 'password_reset_token', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
			],
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_type' => $this->user_type,
            'parent' => $this->parent,
            'city_id' => $this->city_id,
            'package_id' => $this->package_id,
            'is_licensed' => $this->is_licensed,
            'status' => $this->status,
            'trashed' => 0,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'mobile', $this->mobile]);

        return $dataProvider;
    }
}
