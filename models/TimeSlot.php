<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "{{%time_slot}}".
 *
 * @property int $id
 * @property int $marina_id
 * @property string $title
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property int $trashed
 * @property int $trashed_at
 * @property int $trashed_by
 */
class TimeSlot extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%time_slot}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['marina_id', 'title'], 'required'],
            [['marina_id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_at', 'trashed_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'marina_id' => Yii::t('app', 'Marina'),
			'title' => Yii::t('app', 'Title'),
			'status' => Yii::t('app', 'Status'),
			'txtStatus' => Yii::t('app', 'Status'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'trashed' => Yii::t('app', 'Trashed'),
			'trashed_at' => Yii::t('app', 'Trashed At'),
			'trashed_by' => Yii::t('app', 'Trashed By'),
		];
    }
	
    /**
     * @inheritdoc
     */
	public function behaviors()
    {
        return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
                    return date("Y-m-d H:i:s"); 
                },
			],
			'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
		];
     }

    /**
     * Get marina row
     * @return \yii\db\ActiveQuery
     */
    public function getMarina()
    {
        return $this->hasOne(Marina::className(), ['id' => 'marina_id']);
    }
	
    /**
     * return html status
     */
	public function getTxtStatus()
	{
		return $this->status==1 ? '<span class="label label-success">Published</span>' : '<span class="label label-warning">Pending</span>';
	}

    /**
     * @inheritdoc
     */
	public function afterSave($insert, $changedAttributes)
	{
		if($insert){
			Yii::$app->user->identity->LogActivity('timeslot',$this->id,'create');
		}else{
			Yii::$app->user->identity->LogActivity('timeslot',$this->id,'update');
		}
		parent::afterSave($insert, $changedAttributes);
	}
	
    /**
     * Mark record as deleted and hides fron list.
     * @return boolean
     */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand("update ".self::tableName()." set trashed='1',trashed_at='".date("Y-m-d H:i:s")."',trashed_by='".Yii::$app->user->identity->id."' where id='".$this->id."'")->execute();
		Yii::$app->user->identity->LogActivity('timeslot',$this->id,'delete');
		return true;
	}
}
