<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\Invoices;
use app\models\InvoicesSearch;
use yii\web\NotFoundHttpException;

/**
 * InvoicesController implements the CRUD actions for Invoices model.
 */
class InvoicesController extends DefaultController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','create','view','update','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
				'denyCallback' => function ($rule, $action) {
					if(Yii::$app->request->isAjax){
						throw new \yii\web\HttpException(401, Yii::t('app','Please login.'));
					}else{
						return $this->redirect(['site/login']);
					}
				}
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Invoices models.
     * @return mixed
     */
    public function actionIndex()
    {
		$this->checkAdmin();
        $searchModel = new InvoicesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Invoices model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		$this->checkAdmin();
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Invoices model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$this->checkAdmin();
        $model = new Invoices();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
            	Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
				return $this->redirect(['index']);
			}else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								Yii::$app->getSession()->addFlash('error', $val);
							}
						}
					}
				}
			}
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Invoices model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$this->checkAdmin();
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
            	Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
            	return $this->redirect(['index']);
			}else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								Yii::$app->getSession()->addFlash('error', $val);
							}
						}
					}
				}
			}
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Invoices model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		$this->checkAdmin();
        $this->findModel($id)->softDelete();
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Record deleted successfully'));
        return $this->redirect(Yii::$app->request->referrer ?: ['index']);
    }

    /**
     * Finds the Invoices model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invoices the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Invoices::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
