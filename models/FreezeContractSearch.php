<?php

namespace app\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FreezeContract;

/**
 * FreezeContractSearch represents the model behind the search form of `app\models\FreezeContract`.
 */
class FreezeContractSearch extends FreezeContract
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'contract_id'], 'integer'],
            [['start_date', 'end_date', 'status', 'date','pause_date','created_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {


        $query = FreezeContract::find();
        $current_user = Yii::$app->user->identity->id ;

        if( Yii::$app->user->identity->parent != 0){
            $current_user = Yii::$app->user->identity->parent;
        }

        if(Yii::$app->user->identity->user_type == 0){
            $query = FreezeContract::find()->
            where(['user_id' =>  $current_user]);
        }else{
            $query = FreezeContract::find();
        }

        // add conditions that should always apply here

        /*$dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
            ],
        ]);*/

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
           // return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'contract_id' => $this->contract_id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'date' => $this->date,
            'pause_date' => $this->pause_date
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
