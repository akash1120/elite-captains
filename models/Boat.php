<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%boat}}".
 *
 * @property int $id
 * @property int $city_id
 * @property int $marina_id
 * @property string $title
 * @property string $descp
 * @property int $rank
 * @property string $image
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property int $trashed
 * @property string $trashed_at
 * @property int $trashed_by
 */
class Boat extends ActiveRecord
{
	public $addon_idz,$time_slot_idz;
	public $oldimage,$imagefile;
	public $allowedImageSize = 512000;
	public $allowedImageTypes=['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%boat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'marina_id', 'title'], 'required'],
            [['city_id', 'marina_id', 'rank', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['descp'], 'string'],
            [['created_at', 'updated_at', 'trashed_at'], 'safe'],
            [['title'], 'string', 'max' => 55],
            [['image'], 'file', 'extensions'=>implode(",",$this->allowedImageTypes)],
			[['addon_idz','time_slot_idz'], 'each', 'rule'=>['integer']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
			'city_id' => Yii::t('app', 'City'),
			'marina_id' => Yii::t('app', 'Marina'),
			'title' => Yii::t('app', 'Title'),
			'descp' => Yii::t('app', 'Detail'),
			'rank' => Yii::t('app', 'Rank'),
			'image' => Yii::t('app', 'Image'),
            'imageSrc' => Yii::t('app', 'Image'),
			'addon_idz' => Yii::t('app', 'Addons'),
			'time_slot_idz' => Yii::t('app', 'Timings'),
			'status' => Yii::t('app', 'Status'),
			'txtStatus' => Yii::t('app', 'Status'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'trashed' => Yii::t('app', 'Trashed'),
			'trashed_at' => Yii::t('app', 'Trashed At'),
			'trashed_by' => Yii::t('app', 'Trashed By'),
		];
    }
	
    /**
     * @inheritdoc
     */
	public function behaviors()
    {
        return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
                    return date("Y-m-d H:i:s"); 
                },
			],
			'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
		];
     }
	
    /**
     * return html status
     */
	public function getTxtStatus()
	{
		return $this->status==1 ? '<span class="label label-success">Published</span>' : '<span class="label label-warning">Pending</span>';
	}

    /**
     * Get city row
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * Get marina row
     * @return \yii\db\ActiveQuery
     */
    public function getMarina()
    {
        return $this->hasOne(Marina::className(), ['id' => 'marina_id']);
    }
	
    /**
     * Get addons
     * @return \yii\db\ActiveQuery
     */
    public function getAddons()
    {
        return $this->hasMany(BoatToAddon::className(), ['boat_id' => 'id']);
    }
	
    /**
     * Get addons text for index list
     * @return \yii\db\ActiveQuery
     */
	public function getAddonsText()
	{
		$str='';
		if($this->addons!=null){
			foreach($this->addons as $addon){
				if($str!='')$str.='<br />';
				$str.=$addon->addon->title;
			}
		}
		return $str;
	}
	
    /**
     * Get addons
     * @return \yii\db\ActiveQuery
     */
    public function getTimeSlots()
    {
        return $this->hasMany(BoatToTimeSlot::className(), ['boat_id' => 'id'])->orderBy(['id' => SORT_ASC]);
    }
	
    /**
     * Get time slots text for index list
     * @return \yii\db\ActiveQuery
     */
	public function getTimeSlotsText()
	{
		$str='';
		if($this->timeSlots!=null){
			foreach($this->timeSlots as $timeSlot){
				if($str!='')$str.='<br />';
				$str.=$timeSlot->timeSlot->title;
			}
			$str='<small>'.$str.'</small>';
		}
		return $str;
	}
	
    /**
     * Checks photo exits
     * @return photo path
     */
	public function getImageSrc()
	{
		$photo=Yii::$app->params['default_image'];
		if($this->image!=null && file_exists(Yii::$app->params['boat_abs_path'].$this->image)){
			$photo=Yii::$app->params['siteUrl'].Yii::$app->params['boat_rel_path'].$this->image;
		}
		return $photo;
	}

    /**
     * @inheritdoc
     */
	public function beforeValidate()
	{	
		//Uploading image
		if(UploadedFile::getInstance($this, 'image')){
			$this->imagefile = UploadedFile::getInstance($this, 'image');
			// if no file was uploaded abort the upload
			if (!empty($this->imagefile)) {
				$pInfo=pathinfo($this->imagefile->name);
				$ext = $pInfo['extension'];
				if (in_array($ext,$this->allowedImageTypes)) {
					// Check to see if any PHP files are trying to be uploaded
					$content = file_get_contents($this->imagefile->tempName);
					if (preg_match('/\<\?php/i', $content)) {
						$this->addError('image', Yii::t('app', 'Invalid file provided!'));
						return false;
					}else{
						if (filesize($this->imagefile->tempName) <= $this->allowedImageSize) {
							// generate a unique file name
							$this->image = Yii::$app->controller->generateName().".{$ext}";
						}else{
							$this->addError('image', Yii::t('app', 'File size is too big!'));
							return false;
						}
					}
				}else{
					$this->addError('image', Yii::t('app', 'Invalid file provided!'));
					return false;
				}
			}
		}
		if($this->image==null && $this->oldimage!=null){
			$this->image=$this->oldimage;
		}
		return parent::beforeValidate();
	}

    /**
     * @inheritdoc
     */
	public function afterSave($insert, $changedAttributes)
	{
		if ($this->imagefile!== null && $this->image!=null) {
			if($this->oldimage!=null && $this->image!=$this->oldimage && file_exists(Yii::$app->params['boat_abs_path'].$this->oldimage)){
				unlink(Yii::$app->params['boat_abs_path'].$this->oldimage);
			}
			$this->imagefile->saveAs(Yii::$app->params['boat_abs_path'].$this->image);
		}
		
		if($this->addon_idz!=null){
		    BoatToAddon::deleteAll(['and', 'boat_id = :boat_id', ['not in', 'addon_id', $this->addon_idz]], [':boat_id' => $this->id]);
			foreach($this->addon_idz as $key=>$val){
				$sectorRow=BoatToAddon::find()->where(['boat_id'=>$this->id,'addon_id'=>$val])->one();
				if($sectorRow==null){
					$sectorRow=new BoatToAddon;
					$sectorRow->boat_id=$this->id;
					$sectorRow->addon_id=$val;
					$sectorRow->save();
				}
			}
		}
		
		if($this->time_slot_idz!=null){
		    BoatToTimeSlot::deleteAll(['and', 'boat_id = :boat_id', ['not in', 'time_slot_id', $this->time_slot_idz]], [':boat_id' => $this->id]);
			foreach($this->time_slot_idz as $key=>$val){
				$sectorRow=BoatToTimeSlot::find()->where(['boat_id'=>$this->id,'time_slot_id'=>$val])->one();
				if($sectorRow==null){
					$sectorRow=new BoatToTimeSlot;
					$sectorRow->boat_id=$this->id;
					$sectorRow->time_slot_id=$val;
					$sectorRow->save();
				}
			}
		}
		
		if($insert){
			Yii::$app->user->identity->LogActivity('boat',$this->id,'create');
		}else{
			Yii::$app->user->identity->LogActivity('boat',$this->id,'update');
		}
		parent::afterSave($insert, $changedAttributes);
	}
	
    /**
     * Mark record as deleted and hides fron list.
     * @return boolean
     */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand("update ".self::tableName()." set trashed='1',trashed_at='".date("Y-m-d H:i:s")."',trashed_by='".Yii::$app->user->identity->id."' where id='".$this->id."'")->execute();
		Yii::$app->user->identity->LogActivity('boat',$this->id,'delete');
		return true;
	}
}