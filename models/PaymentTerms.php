<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%payment_terms}}".
 *
 * @property int $id
 * @property string $name
 * @property int $no_of_days
 * @property int $status
 */
class PaymentTerms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_terms}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_of_days', 'status'], 'integer'],
            [['name','short_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
                        'name' => 'Name',
                        'no_of_days' => 'No Of Days',
                        'status' => 'Status',
            			'txtStatus' => 'Status',
			        ];
    }
	
    /**
     * return html status
     */
	public function getTxtStatus()
	{
		return $this->status==1 ? '<span class="label label-success">Published</span>' : '<span class="label label-warning">Pending</span>';
	}
}
