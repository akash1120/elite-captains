<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "{{%request}}".
 *
 * @property int $id
 * @property string $item_type
 * @property integer $item_id
 * @property string $descp
 * @property string $requested_date
 * @property string $freeze_start
 * @property string $freeze_end
 * @property integer $status
 * @property string $freeze_end
 * @property string $remarks
 * @property string $admin_action_date
 * @property string $active_show_till
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property int $trashed
 * @property string $trashed_at
 * @property int $trashed_by
 */
class Request extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_type'], 'required'],
            [['item_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['descp','remarks'], 'string'],
            [['freeze_start', 'freeze_end', 'requested_date', 'admin_action_date', 'active_show_till', 'created_at', 'updated_at'], 'safe'],
            [['item_type'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'item_type' => Yii::t('app', 'Item Type'),
            'item_id' => Yii::t('app', 'Item ID'),
            'descp' => Yii::t('app', 'Description'),
            'requested_date' => Yii::t('app', 'Requested Date'),
            'freeze_start' => Yii::t('app', 'Freeze Start'),
            'freeze_end' => Yii::t('app', 'Freeze End'),
            'status' => Yii::t('app', 'Status'),
            'remarks' => Yii::t('app', 'Remarks'),
            'admin_action_date' => Yii::t('app', 'Admin Acted'),
            'created_at' => Yii::t('app', 'Submitted'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
	
    /**
     * @inheritdoc
     */
	public function behaviors()
    {
        return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
                    return date("Y-m-d H:i:s"); 
                },
			],
			'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
		];
     }
	
    /**
     * return html status
     */
	public function getTxtStatus()
	{
		return $this->status==1 ? '<span class="label label-success">Completed</span>' : ($this->status==2 ? '<span class="label label-warning">Not Completed</span>' : '<span class="label label-info">In Process</span>');
	}

    /**
     * @inheritdoc
     */
	public function afterSave($insert, $changedAttributes)
	{
		if($insert){
			Yii::$app->user->identity->LogActivity('request',$this->id,'create');
		}else{
			Yii::$app->user->identity->LogActivity('request',$this->id,'update');
		}
		parent::afterSave($insert, $changedAttributes);
	}
	
    /**
     * Mark record as deleted and hides fron list.
     * @return boolean
     */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand("update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",[':trashed'=>1,':trashed_at'=>date("Y-m-d H:i:s"),':trashed_by'=>Yii::$app->user->identity->id,':id'=>$this->id])->execute();
		Yii::$app->user->identity->LogActivity('request',$this->id,'delete');
		return true;
	}
}
