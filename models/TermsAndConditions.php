<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%terms_and_conditions}}".
 *
 * @property int $id
 * @property string $terms
 * @property string $client_notes
 * @property string $admin_notes
 */
class TermsAndConditions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%terms_and_conditions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['terms', 'client_notes', 'admin_notes','joining_fee'], 'safe'],
                [['terms', 'client_notes', 'admin_notes'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
                        'terms' => Yii::t('app', 'Terms'),
                        'client_notes' => Yii::t('app', 'Client Notes'),
                        'admin_notes' => Yii::t('app', 'Admin Notes'),
                    ];
    }
}
