<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%marina_to_addon}}".
 *
 * @property int $id
 * @property int $marina_id
 * @property int $addon_id
 */
class MarinaToAddon extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%marina_to_addon}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['marina_id', 'addon_id'], 'required'],
            [['marina_id', 'addon_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
			'marina_id' => Yii::t('app', 'Marina ID'),
			'addon_id' => Yii::t('app', 'Addon ID'),
		];
    }

    /**
     * Get marina row
     * @return \yii\db\ActiveQuery
     */
    public function getMarina()
    {
        return $this->hasOne(Marina::className(), ['id' => 'marina_id']);
    }

    /**
     * Get addon row
     * @return \yii\db\ActiveQuery
     */
    public function getAddon()
    {
        return $this->hasOne(Addon::className(), ['id' => 'addon_id']);
    }
}
