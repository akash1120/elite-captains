<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contract_cities".
 *
 * @property int $id
 * @property int $city_id
 * @property int $contract_id
 */
class ContractCities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contract_cities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id', 'contract_id'], 'required'],
            [['city_id', 'contract_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'contract_id' => 'Contract ID',
        ];
    }
}
