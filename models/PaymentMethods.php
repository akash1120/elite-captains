<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%payment_methods}}".
 *
 * @property int $id
 * @property string $name
 * @property string $short_code
 * @property int $status
 */
class PaymentMethods extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_methods}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['name', 'short_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
                        'name' => 'Name',
                        'short_code' => 'Short Code',
                        'status' => 'Status',
            			'txtStatus' => 'Status',
			        ];
    }
	
    /**
     * return html status
     */
	public function getTxtStatus()
	{
		return $this->status==1 ? '<span class="label label-success">Published</span>' : '<span class="label label-warning">Pending</span>';
	}
}
