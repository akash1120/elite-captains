<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "{{%package}}".
 *
 * @property int $id
 * @property string $title
 * @property int $authorized_captains
 * @property int $no_of_booking
 * @property int $price
 * @property int $duration
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property int $trashed
 * @property string $trashed_at
 * @property int $trashed_by
 */
class Package extends ActiveRecord
{
	public $addon_idz,$service_idz;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%package}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'duration', 'price'], 'required'],
            [['authorized_captains', 'no_of_booking', 'price', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['addon_idz', 'created_at', 'updated_at', 'trashed_at','half_price','half_year_duration'], 'safe'],
            [['title'], 'string', 'max' => 32],
			['service_idz', 'each', 'rule'=>['integer']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'authorized_captains' => Yii::t('app', 'Authorized Capt.'),
			'no_of_booking' => Yii::t('app', 'Future Bookings'),
			'duration' => Yii::t('app', 'Duration'),
			'txtDuration' => Yii::t('app', 'Duration'),
			'price' => Yii::t('app', 'Price'),
			'addon_idz' => Yii::t('app', 'Addons'),
			'service_idz' => Yii::t('app', 'Restrictions'),
			'status' => Yii::t('app', 'Status'),
			'txtStatus' => Yii::t('app', 'Status'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'trashed' => Yii::t('app', 'Trashed'),
			'trashed_at' => Yii::t('app', 'Trashed At'),
			'trashed_by' => Yii::t('app', 'Trashed By'),
		];
    }
	
    /**
     * @inheritdoc
     */
	public function behaviors()
    {
        return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
                    return date("Y-m-d H:i:s"); 
                },
			],
			'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
		];
     }
	
    /**
     * return html status
     */
	public function getTxtStatus()
	{
		return $this->status==1 ? '<span class="label label-success">Published</span>' : '<span class="label label-warning">Pending</span>';
	}
	
    /**
     * return duration
     */
	public function getTxtDuration()
	{
		return Yii::$app->params['packageDurationArr'][$this->duration];
	}
	
    /**
     * Get addons
     * @return \yii\db\ActiveQuery
     */
    public function getAddons()
    {
        return $this->hasMany(PackageToAddon::className(), ['package_id' => 'id']);
    }
	
    /**
     * Get addons text for index list
     * @return \yii\db\ActiveQuery
     */
	public function getAddonsText()
	{
		$str='';
		if($this->addons!=null){
			foreach($this->addons as $addon){
				if($str!='')$str.='<br />';
				$str.=$addon->free_qty.' Free '.$addon->addon->title;
			}
		}
		return $str;
	}
	
    /**
     * Get addons
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(PackageToService::className(), ['package_id' => 'id']);
    }
	
    /**
     * Get services text for index list
     * @return \yii\db\ActiveQuery
     */
	public function getServicesText()
	{
		$str='';
		if($this->services!=null){
			foreach($this->services as $service){
				if($str!='')$str.='<br />';
				$str.=$service->service->title;
			}
		}
		return $str;
	}

    /**
     * @inheritdoc
     */
	public function afterSave($insert, $changedAttributes)
	{
		if($this->addon_idz!=null){
			foreach($this->addon_idz as $key=>$val){
				$sectorRow=PackageToAddon::find()->where(['package_id'=>$this->id,'addon_id'=>$key])->one();
				if($sectorRow==null){
					$sectorRow=new PackageToAddon;
					$sectorRow->package_id=$this->id;
				}
				$sectorRow->addon_id=$key;
				$sectorRow->free_qty=$val;
				$sectorRow->save();
			}
		}
		
		PackageToService::deleteAll(['and', 'package_id = :package_id', ['not in', 'service_id', $this->service_idz]], [':package_id' => $this->id]);
		if($this->service_idz!=null){
			foreach($this->service_idz as $key=>$val){
				$sectorRow=PackageToService::find()->where(['package_id'=>$this->id,'service_id'=>$val])->one();
				if($sectorRow==null){
					$sectorRow=new PackageToService;
					$sectorRow->package_id=$this->id;
					$sectorRow->service_id=$val;
					$sectorRow->save();
				}
			}
		}
		
		if($insert){
			Yii::$app->user->identity->LogActivity('package',$this->id,'create');
		}else{
			Yii::$app->user->identity->LogActivity('package',$this->id,'update');
		}
		parent::afterSave($insert, $changedAttributes);
	}
	
    /**
     * Mark record as deleted and hides fron list.
     * @return boolean
     */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand("update ".self::tableName()." set trashed='1',trashed_at='".date("Y-m-d H:i:s")."',trashed_by='".Yii::$app->user->identity->id."' where id='".$this->id."'")->execute();
		Yii::$app->user->identity->LogActivity('package',$this->id,'delete');
		return true;
	}
}
