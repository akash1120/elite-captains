<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * BookingForm is the model behind the booking search form.
 */
class BookingForm extends Model
{
    public $city_id;
    public $marina_id;
    public $date;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['city_id', 'marina_id', 'date'], 'required'],
            [['city_id','marina_id'], 'integer'],
            [['date'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'city_id' => Yii::t('app', 'City'),
			'marina_id' => Yii::t('app', 'Marina'),
			'date' => Yii::t('app', 'Date'),
		];
    }
}
