<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * RequestSimple is the model behind the request type simple form.
 */
class RequestSimple extends Model
{
    public $comments;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['comments'], 'required'],
            [['comments'], 'string'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'comments' => 'Message or any other request',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function send()
    {
        if ($this->validate()) {
			$req=new Request;
			$req->item_type='simple';
			$req->descp=nl2br($this->comments);
			$req->save();
			
			Yii::$app->mailer->compose(['html' => 'requestSuggestion-html', 'text' => 'requestSuggestion-text'], ['request' => $this])
				->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
				->setReplyTo(Yii::$app->user->identity->email)
				->setTo(Yii::$app->controller->getSetting('suggestionEmail'))
				->setSubject('New Feedback - ' . Yii::$app->params['siteName'])
				->send();
			
			$templateId=Yii::$app->controller->getSetting('feedback_response');
			$template=EmailTemplate::findOne($templateId);
			if($template!=null){
				$vals = [
					'{captainName}' => Yii::$app->user->identity->username,
				];
				$htmlBody=$template->searchReplace($template->template_html,$vals);
				$textBody=$template->searchReplace($template->template_text,$vals);
				$message=Yii::$app->mailer->compose()
					->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
					->setReplyTo(Yii::$app->controller->getSetting('suggestionEmail'))
					->setSubject('Feedback Sent - ' . Yii::$app->params['siteName'])
					->setHtmlBody($htmlBody)
					->setTextBody($textBody);
					
				if(Yii::$app->user->identity->parent!=0){
					$message->setTo(Yii::$app->user->identity->parentUser->email);
					$message->send();
					if(Yii::$app->user->identity->parentUser->subUsers!=null){
						foreach(Yii::$app->user->identity->parentUser->subUsers as $subUser){
							$message->setTo($subUser->email);
							$message->send();
						}
					}
				}else{
					$message->setTo(Yii::$app->user->identity->email);
					$message->send();
					if(Yii::$app->user->identity->subUsers!=null){
						foreach(Yii::$app->user->identity->subUsers as $subUser){
							$message->setTo($subUser->email);
							$message->send();
						}
					}
				}
			}
            return true;
        }
        return false;
    }
}
