<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserNote;

/**
 * UserNoteSearch represents the model behind the search form about `app\models\UserNote`.
 */
class UserNoteSearch extends UserNote
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['comment', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserNote::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort' => ['defaultOrder' => ['updated_at'=>SORT_DESC],]
        ]);

        $this->load($params);
		
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'trashed' => 0,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
