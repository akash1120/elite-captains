<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Invoices;

/**
 * InvoicesSearch represents the model behind the search form of `app\models\Invoices`.
 */
class InvoicesSearch extends Invoices
{
	public $pageSize;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'invoice_id', 'contract_id', 'created_by'], 'integer'],
            [['invoice_date', 'subtotal', 'tax', 'total', 'status', 'invoice_description', 'admin_notes', 'client_notes', 'invoice_terms', 'billing_cycle', 'last_payment_date', 'update_at', 'create_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        
        $query = Invoices::find();
        $current_user = Yii::$app->user->identity->id ;

       if( Yii::$app->user->identity->parent != 0){
           $current_user = Yii::$app->user->identity->parent;
       }


        if(Yii::$app->user->identity->user_type == 0){
            $query = Invoices::find()->
            //where(['user_id' =>  Yii::$app->user->identity->id ]);
            where(['user_id' =>  $current_user]);
        }else{
            $query = Invoices::find();
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
			],
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'invoice_id' => $this->invoice_id,
            'contract_id' => $this->contract_id,
            'invoice_date' => $this->invoice_date,
            'created_by' => $this->created_by,
            'last_payment_date' => $this->last_payment_date,
            'update_at' => $this->update_at,
            'create_at' => $this->create_at,
        ]);

        $query->andFilterWhere(['like', 'subtotal', $this->subtotal])
            ->andFilterWhere(['like', 'tax', $this->tax])
            ->andFilterWhere(['like', 'total', $this->total])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'invoice_description', $this->invoice_description])
            ->andFilterWhere(['like', 'admin_notes', $this->admin_notes])
            ->andFilterWhere(['like', 'client_notes', $this->client_notes])
            ->andFilterWhere(['like', 'invoice_terms', $this->invoice_terms])
            ->andFilterWhere(['like', 'billing_cycle', $this->billing_cycle]);

        return $dataProvider;
    }
}
