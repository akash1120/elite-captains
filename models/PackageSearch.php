<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Package;

/**
 * PackageSearch represents the model behind the search form of `app\models\Package`.
 */
class PackageSearch extends Package
{
	public $pageSize;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'authorized_captains', 'no_of_booking', 'price', 'duration', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by', 'pageSize'], 'integer'],
            [['title', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        
        $query = Package::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
			],
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'duration' => $this->duration,
            'authorized_captains' => $this->authorized_captains,
            'no_of_booking' => $this->no_of_booking,
            'status' => $this->status,
            'trashed' => 0,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
