<?php

namespace app\models;

use app\helpers\HelperFunction;
use function GuzzleHttp\Psr7\str;
use Yii;

/**
 * This is the model class for table "freeze_contract".
 *
 * @property int $id
 * @property int $user_id
 * @property int $contract_id
 * @property string $start_date
 * @property string $end_date
 * @property string $status
 * @property string $date
 * @property int $created_by
 */
class FreezeContract extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'freeze_contract';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contract_id', 'start_date', 'end_date'], 'required'],
            [['user_id', 'contract_id'], 'integer'],
            [['start_date', 'end_date', 'date', 'created_by','pause_date'], 'safe'],
            [['status'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'contract_id' => Yii::t('app', 'Contract ID'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'status' => Yii::t('app', 'Status'),
            'date' => Yii::t('app', 'Date'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }

    /**
     * @return bool|int|string
     */
    public function beforeValidate()
    {

        if (parent::beforeValidate()) {
            //return $this->date = date('y-m-d');
        }
        return true;

        //  return false;
    }


    public function beforeSave($insert)
    {
        $this->date = date('y-m-d');
        // Get Contract Information
        $contract = Contracts::findOne($this->contract_id);





        if($this->status == 'active') {

            $contract_end_date = date('Y-m-d', strtotime($contract->end_date));
            if ($contract->current_expiry <> null) {
                $contract_end_date = date('Y-m-d', strtotime($contract->current_expiry));
            }
            if ((strtotime($this->start_date) < strtotime($contract->start_date)) || (strtotime($this->start_date) >= strtotime($contract_end_date)) || (strtotime($this->end_date) > strtotime($contract_end_date)) || (strtotime($this->end_date) < strtotime($contract->start_date))) {
                Yii::$app->session->setFlash('error', "Please select start date and end date of freez days according to your current contract expiry ");
                return false;
            }

            $active_freez = FreezeContract::find()->where([
                'contract_id' => $this->contract_id,
                'status' => 'active'
            ])->orderBy(['id' => SORT_DESC])->all();

            if ($active_freez <> null && count($active_freez) > 0) {

                Yii::$app->session->setFlash('error', "You already have an active freeze days.");
                return false;
            }




        }
        if($this->status == 'paused') {

            if(isset($this->id)){
                $freez_entry = FreezeContract::findOne($this->id);
                if($freez_entry->status == 'paused'){
                    Yii::$app->session->setFlash('error', "This is already in paused status");
                    return false;
                }
            }

            $this->pause_date = date('Y-m-d');
            return true;
        }

        // Check if user has active booking during the selected freeze days
        $checkingBookings = Booking::find()
            ->where(['>=', 'booking_date', $this->start_date])
            ->andWhere(['<=', 'booking_date', $this->end_date])
            ->andWhere(['user_id' => $contract->user->id])
            ->andWhere(['trashed' => 0])
            ->all();

        // If there is selected booking during freeze days, Return
        if ($checkingBookings <> null && count($checkingBookings) > 0) {
            Yii::$app->session->setFlash('error', "You have an active booking during the selected freeze days! Please delete your booking to freeze contract in selected dates");
            return false;
        }

        // Calculate number of days and check if these are less than remaining freeze days
        $days = HelperFunction::dateDifference($this->start_date, $this->end_date);
        if($days >= 0){
            $days = $days + 1;
        }
        if ($days <= $contract->remaining_freeze_days) {
            $this->user_id = ($contract <> null) ? $contract->user_id : "0";
            $this->created_by = Yii::$app->user->identity->id;
            return true;
        }

        Yii::$app->session->setFlash('error', "Your freez days limit is less.");
        return false;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {

        $current_date = strtotime(date('Y-m-d'));
        $contract_details = Contracts::findOne($this->contract_id);
        $days = HelperFunction::dateDifference($this->start_date, $this->end_date);
        if($days >= 0){
            $days = $days + 1;
        }
        if ($this->status == 'active') {

            $contract_details->current_expiry = date('Y-m-d', strtotime($contract_details->current_expiry . ' + ' . $days . ' days'));
            $contract_details->remaining_freeze_days = $contract_details->remaining_freeze_days - $days;
            if ($current_date <= strtotime($this->end_date) && $current_date >= strtotime($this->start_date)) {
                $contract_details->is_freeze = 1;
            }


        } else if($this->status == 'paused') {
            $days_remaining = HelperFunction::dateDifference(date('Y-m-d'), $this->end_date);
            if($days_remaining >= 0){
                $days_remaining = $days_remaining + 1;
            }
            if($days_remaining > 0) {
                if ($days_remaining >= $days) {
                    $contract_details->remaining_freeze_days = $contract_details->remaining_freeze_days + $days;
                    $contract_details->current_expiry = date('Y-m-d', strtotime($contract_details->current_expiry . ' - ' . $days . ' days'));
                } else {
                    $new_add_days = $days - $days_remaining;
                    $new_add_days = $days_remaining;
                    $contract_details->remaining_freeze_days = $contract_details->remaining_freeze_days + $new_add_days;
                    $contract_details->current_expiry = date('Y-m-d', strtotime($contract_details->current_expiry . ' - ' . $days_remaining . ' days'));
                }
            }
            $contract_details->is_freeze = 0;
        }else if($this->status == 'completed') {
            $contract_details->is_freeze = 0;
        }
        $contract_details->save();
    }

    /**
     * return User
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
