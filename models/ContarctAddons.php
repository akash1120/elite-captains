<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%contarct_addons}}".
 *
 * @property int $id
 * @property int $contract_id
 * @property int $customer_id
 * @property int $qunatity
 * @property string $price
 */
class ContarctAddons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contarct_addons}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contract_id', 'customer_id', 'addon_id'], 'required'],
            [['contract_id', 'customer_id', 'qunatity'], 'integer'],
            [['price'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'contract_id' => Yii::t('app', 'Contract ID'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'qunatity' => Yii::t('app', 'Qunatity'),
            'price' => Yii::t('app', 'Price'),
        ];
    }

    /**
     * return Package
     */
    public function getAddon(){
        return $this->hasOne(Package::className(),['id' => 'addon_id']);
    }
}
