<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Payments;

/**
 * PaymentsSearch represents the model behind the search form of `app\models\Payments`.
 */
class PaymentsSearch extends Payments
{
    public $pageSize;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'invoice_id', 'payment_method_id', 'user_id', 'received_by'], 'integer'],
            [['payment_ref', 'trax_ref', 'status', 'amount', 'note', 'date', 'response_code', 'pt_invoice_id', 'transaction_id', 'frequency', 'currency'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payments::find();

        $current_user = Yii::$app->user->identity->id ;

        if( Yii::$app->user->identity->parent != 0){
            $current_user = Yii::$app->user->identity->parent;
        }
        if(Yii::$app->user->identity->user_type == 0){
            $query = Payments::find()->
            where(['user_id' => $current_user]);
        }else{
            $query = Payments::find();
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'invoice_id' => $this->invoice_id,
            'payment_method_id' => $this->payment_method_id,
            'user_id' => $this->user_id,
            'received_by' => $this->received_by,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'payment_ref', $this->payment_ref])
            ->andFilterWhere(['like', 'trax_ref', $this->trax_ref])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'amount', $this->amount])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'response_code', $this->response_code])
            ->andFilterWhere(['like', 'pt_invoice_id', $this->pt_invoice_id])
            ->andFilterWhere(['like', 'transaction_id', $this->transaction_id])
            ->andFilterWhere(['like', 'frequency', $this->frequency])
            ->andFilterWhere(['like', 'currency', $this->currency]);

        return $dataProvider;
    }
}
