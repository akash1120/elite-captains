<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%user_login_history}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $ip
 * @property string $http_referrer
 * @property string $created_at
 * @property int $login
 */
class UserLoginHistory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_login_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ip', 'created_at'], 'required'],
            [['user_id', 'login'], 'integer'],
            [['http_referrer'], 'string'],
            [['created_at'], 'safe'],
            [['ip'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'User ID'),
			'ip' => Yii::t('app', 'Ip'),
			'http_referrer' => Yii::t('app', 'Http Referrer'),
			'created_at' => Yii::t('app', 'Created At'),
			'login' => Yii::t('app', 'Login'),
		];
    }
}
