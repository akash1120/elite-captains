<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%package_to_addon}}".
 *
 * @property int $id
 * @property int $package_id
 * @property int $addon_id
 * @property int $free_qty
 */
class PackageToAddon extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%package_to_addon}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['package_id', 'addon_id', 'free_qty'], 'required'],
            [['package_id', 'addon_id', 'free_qty'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
			'package_id' => Yii::t('app', 'Package ID'),
			'addon_id' => Yii::t('app', 'Addon ID'),
			'free_qty' => Yii::t('app', 'Free Qty'),
		];
    }

    /**
     * Get package row
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        return $this->hasOne(Package::className(), ['id' => 'package_id']);
    }

    /**
     * Get addon row
     * @return \yii\db\ActiveQuery
     */
    public function getAddon()
    {
        return $this->hasOne(Addon::className(), ['id' => 'addon_id']);
    }
}
