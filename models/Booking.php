<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%booking}}".
 *
 * @property int $id
 * @property string $reference_no
 * @property int $user_id
 * @property int $city_id
 * @property int $marina_id
 * @property int $boat_id
 * @property string $booking_date
 * @property int $time_slot_id
 * @property int $booking_type
 * @property string $booking_comments
 * @property int $is_archive
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property int $trashed
 * @property string $trashed_at
 * @property int $trashed_by
 */
class Booking extends ActiveRecord
{
    public $addon_idz;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%booking}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'city_id', 'marina_id', 'boat_id', 'booking_date', 'time_slot_id'], 'required'],
            [['user_id', 'city_id', 'marina_id', 'boat_id', 'time_slot_id', 'booking_type', 'is_archive', 'created_by', 'updated_by', 'trashed', 'trashed_by','contract_id'], 'integer'],
            [['reference_no', 'booking_date', 'addon_idz', 'created_at', 'updated_at', 'trashed_at','captain','food','donut'], 'safe'],
            [['reference_no', 'booking_comments'], 'string'],
            ['booking_date', 'checkPackageValidation'],
        ];
    }

    /**
     * check package restrictions
     */
    public function checkPackageValidation($attribute, $params)
    {

        $current_user = Yii::$app->user->identity->id ;
        $current_user_in = Yii::$app->user->identity->id ;

        if( Yii::$app->user->identity->parent != 0){
            $current_user = Yii::$app->user->identity->parent;
            $current_user_in = [Yii::$app->user->identity->id,Yii::$app->user->identity->parent] ;
        }
        $current_contract = Contracts::find()->where([
            'user_id' => $current_user,
            'status' => 'active'
        ])->orderBy(['id' => SORT_DESC])->one();


        $active_contract = Contracts::find()->where([
            'user_id' => $current_user,
            'status' => 'active'
        ])->all();

        if ($active_contract == null || count($active_contract) == 0) {
            $this->addError('', 'You do not have any active contract. Please contact administrator for support');
            return false;
        }


        /*check for joining fee*/
        /*if($current_contract <> null){
            if($current_contract->first_contract_check == 1 && (int)$current_contract->joining_fee > 0){
                //get invoice data
                $get_invoice_data= Invoices::find()->where(['contract_id' => $current_contract->id])->one();
                // get payments against invoice
                $previous_payments= Payments::find()->where(['invoice_id' => $get_invoice_data->id])->asArray()->all();
                $paid_amount = 0;
                if(isset($previous_payments) && $previous_payments <> null){
                    foreach ($previous_payments as $key => $payment){
                        $paid_amount = $paid_amount + $payment['amount'] + $payment['discount'];
                    }
                }
                //check joining fee
                if((int)$current_contract->joining_fee > (int)$paid_amount){
                    $this->addError('', 'Please pay your joining fee first!');
                    return false;
                }
            }

        }*/

        $active_freez = FreezeContract::find()->where([
            'contract_id' => $current_contract->id,
            'status' => 'active'
        ])->orderBy(['id' => SORT_DESC])->one();

        if ($active_freez <> null) {
            if ((strtotime($this->booking_date) >= strtotime($active_freez->start_date)) && (strtotime($this->booking_date) <= strtotime($active_freez->end_date))) {
                $this->addError('', 'You have active freeze days during the selected booking!');
                return false;
            }
        }



        $contract_end_date = date('Y-m-d', strtotime($current_contract->end_date));
        if ($current_contract->current_expiry <> null) {
            $contract_end_date = date('Y-m-d', strtotime($current_contract->current_expiry));
        }

        //same attributes restriction
        $alreadyBooking = self::find()->where(['city_id' => $this->city_id, 'marina_id' => $this->marina_id, 'boat_id' => $this->boat_id, 'booking_date' => $this->booking_date, 'time_slot_id' => $this->time_slot_id, 'trashed' => 0]);
        if ($alreadyBooking->exists()) {
            $this->addError('', 'This boat is already booked on this time, please choose another boat or date');
            return false;
        }

        if (Yii::$app->user->identity->user_type == 0) {
            //Check if booking is in past
            if ($this->booking_date < date("Y-m-d")) {
                $this->addError('', 'Your booking date can not be in past');
                return false;
            }

            //Check if booking is for today
            if ($this->booking_date == date("Y-m-d")) {
                $this->addError('', 'It is too late to book today');
                return false;
            }

            //Check if Expired
           // if (date("Y-m-d") > Yii::$app->user->identity->parentPackageEndDate) {
            if (date("Y-m-d") > $contract_end_date) {
                $this->addError('', 'Your Package is expired, Please contact, administration to renew your account.');
                return false;
            }

            //Check if booking is after expiry date
           // if ($this->booking_date > Yii::$app->user->identity->parentPackageEndDate) {
            if ($this->booking_date > $contract_end_date) {
                $this->addError('', 'Your booking date should be before your package expiry date. i.e ' . Yii::$app->formatter->asDate($contract_end_date));
                return false;
            }

            $userIdz = Yii::$app->user->identity->userIdz;

            //one day restriction
           // $bookingCount = self::find()->where(['and', ['in', 'user_id', Yii::$app->user->identity->id], ['=', 'DATE(booking_date)', $this->booking_date], ['trashed' => 0]])->count('id');
            $bookingCount = self::find()->where(['and', ['in', 'user_id', $current_user_in], ['=', 'DATE(booking_date)', $this->booking_date], ['trashed' => 0]])->count('id');
            if ($bookingCount >= 1) {
                $this->addError('', 'You already have a booking on ' . Yii::$app->formatter->asDate($this->booking_date));
                return false;
            }

            $package = Yii::$app->user->identity->package;
            $allowedActiveBooking = $package->no_of_booking;
            if ($package->id == Yii::$app->params['sharingPackageId']) {
                $allowedActiveBooking /= 2;
            }

            //active booking restriction
           // $bookingCount = self::find()->where(['and', ['in', 'user_id', Yii::$app->user->identity->id], ['>=', 'DATE(booking_date)', date("Y-m-d")], ['trashed' => 0]])->count('id');
            $bookingCount = self::find()->where(['and', ['in', 'user_id', $current_user_in], ['>=', 'DATE(booking_date)', date("Y-m-d")], ['trashed' => 0]])->count('id');

            if ($bookingCount >= $allowedActiveBooking) {
                $this->addError('', 'You already reached maximum number of bookings allowed by your package.');
                return false;
            }

            $isWeekend = Yii::$app->controller->isWeekend($this->booking_date);

            //Once a week (Weekdays) booking
            if (Yii::$app->user->identity->getPackageService('onceaweek') == true) {
                if ($isWeekend == true) {
                    $this->addError('', 'Your package allows you to book only in Weekdays (Sun to Thu)');
                    return false;
                }
                list($startDate, $endDate) = Yii::$app->controller->getWeekStartEnd($this->booking_date);
                $WeekBooking = self::find()->where(['and', ['in', 'user_id', $userIdz], ['>=', 'DATE(booking_date)', $startDate], ['<=', 'DATE(booking_date)', $endDate], ['trashed' => 0]])->count('id');
                if ($WeekBooking > 0) {
                    $this->addError('', 'Your package allows you to book once in a week, and you already have a booking in selected week');
                    return false;
                }
            }
            //Weekdays (Sun to Thu) booking
            if (Yii::$app->user->identity->getPackageService('weekdays') == true) {
                if ($isWeekend == true) {
                    $this->addError('', 'Your package allows you to book only in Weekdays (Sun to Thu)');
                    return false;
                }
            }

            //Weekdays & ( Fri or Sat) booking
            if (Yii::$app->user->identity->getPackageService('fivedaysoneweekendday') == true) {
                if ($isWeekend == true) {
                    $whichWeekendDay = Yii::$app->controller->WeekendDay($this->booking_date);
                    list($y, $m, $d) = explode("-", $this->booking_date);
                    if ($whichWeekendDay == 5) {
                       // $row = self::find()->where(['and', ['in', 'user_id', Yii::$app->user->identity->id], ['booking_date' => date("Y-m-d", mktime(0, 0, 0, $m, ($d + 1), $y)), 'trashed' => 0]]);
                        $row = self::find()->where(['and', ['in', 'user_id', $current_user_in], ['booking_date' => date("Y-m-d", mktime(0, 0, 0, $m, ($d + 1), $y)), 'trashed' => 0]]);
                        if ($row->exists()) {
                            $this->addError('', 'You already have a booking on saturday, you can not book on friday');
                            return false;
                        }
                    } elseif ($whichWeekendDay == 6) {
                       // $row = self::find()->where(['and', ['in', 'user_id', Yii::$app->user->identity->id], ['booking_date' => date("Y-m-d", mktime(0, 0, 0, $m, ($d - 1), $y)), 'trashed' => 0]]);
                        $row = self::find()->where(['and', ['in', 'user_id', $current_user_in], ['booking_date' => date("Y-m-d", mktime(0, 0, 0, $m, ($d - 1), $y)), 'trashed' => 0]]);
                        if ($row->exists()) {
                            $this->addError('', 'You already have a booking on friday, you can not book on saturday');
                            return false;
                        }
                    }
                    if ($package->id == Yii::$app->params['sharingPackageId']) {
                        if ($whichWeekendDay == 5) {
                            $row = self::find()->where(['and', ['in', 'user_id', $userIdz], ['booking_date' => date("Y-m-d", mktime(0, 0, 0, $m, $d, $y)), 'trashed' => 0]]);
                            if ($row->exists()) {
                                $this->addError('', 'You already have a booking on ' . Yii::$app->formatter->asDate($this->booking_date));
                                return false;
                            }
                        } elseif ($whichWeekendDay == 6) {
                            $row = self::find()->where(['and', ['in', 'user_id', $userIdz], ['booking_date' => date("Y-m-d", mktime(0, 0, 0, $m, $d, $y)), 'trashed' => 0]]);
                            if ($row->exists()) {
                                $this->addError('', 'You already have a booking on ' . Yii::$app->formatter->asDate($this->booking_date));
                                return false;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'reference_no' => Yii::t('app', 'Ref#'),
            'user_id' => Yii::t('app', 'User'),
            'userName' => Yii::t('app', 'User'),
            'city_id' => Yii::t('app', 'City'),
            'marina_id' => Yii::t('app', 'Marina'),
            'boat_id' => Yii::t('app', 'Boat'),
            'booking_date' => Yii::t('app', 'Date'),
            'time_slot_id' => Yii::t('app', 'Time'),
            'booking_type' => Yii::t('app', 'Booking Type'),
            'booking_comments' => Yii::t('app', 'Booking Comments'),
            'is_archive' => Yii::t('app', 'Archived'),
            'txtAddons' => Yii::t('app', 'Addons'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'trashed' => Yii::t('app', 'Trashed'),
            'trashed_at' => Yii::t('app', 'Trashed At'),
            'trashed_by' => Yii::t('app', 'Trashed By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function ($event) {
                    return date("Y-m-d H:i:s");
                },
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * Get user row
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Get username
     * @return string
     */
    public function getUserName()
    {
        return $this->user->username;
    }

    /**
     * Get city row
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * Get marina row
     * @return \yii\db\ActiveQuery
     */
    public function getMarina()
    {
        return $this->hasOne(Marina::className(), ['id' => 'marina_id']);
    }

    /**
     * Get boat row
     * @return \yii\db\ActiveQuery
     */
    public function getBoat()
    {
        return $this->hasOne(Boat::className(), ['id' => 'boat_id']);
    }

    /**
     * Get time slot row
     * @return \yii\db\ActiveQuery
     */
    public function getTimeSlot()
    {
        return $this->hasOne(TimeSlot::className(), ['id' => 'time_slot_id']);
    }

    /**
     * Get addons
     * @return \yii\db\ActiveQuery
     */
    public function getAddons()
    {
        return $this->hasMany(BookingToAddon::className(), ['booking_id' => 'id']);
    }

    public function getCaptaindata()
    {
        return $this->hasOne(Addon::className(), ['id' => 'captain']);
    }
    public function getFooddata()
    {
        return $this->hasOne(Addon::className(), ['id' => 'food']);
    }
    public function getDonutdata()
    {
        return $this->hasOne(Addon::className(), ['id' => 'donut']);
    }

    /**
     * Get addons html string
     * @return string
     */
    public function getTxtAddons()
    {
        //Check free addon, how to calculate, and make sure it is for current package and within the start and expirey of current package
        $str = '';
        if ($this->boat->addons != null) {
            foreach ($this->boat->addons as $addon) {
                if ($str != '') $str .= '<br />';
                $bookingAddon = BookingToAddon::find()->where(['booking_id' => $this->id, 'addon_id' => $addon->addon_id]);

                $str .= $addon->addon->title . ($bookingAddon->exists() ? ' <i class="fa fa-check"></i>' : ' <i class="fa fa-plus"></i>');
            }
        }
        return $str;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->addon_idz != null) {
            foreach ($this->addon_idz as $key => $val) {
                $checkIfBoatAddon = BoatToAddon::find()->where(['boat_id' => $this->boat_id, 'addon_id' => $val])->one();
                if ($checkIfBoatAddon != null) {
                    $childRow = new BookingToAddon;
                    $childRow->booking_id = $this->id;
                    $childRow->addon_id = $val;
                    $childRow->save();
                }
            }
        }
        if ($insert) {
            $reference = 'ECB' . '-' . date('y') . '-' . sprintf('%03d', $this->id);
            $this->reference_no = $reference;

            $connection = \Yii::$app->db;
            $connection->createCommand(
                "update " . self::tableName() . " set reference_no=:reference_no where id=:id",
                [':reference_no' => $reference, ':id' => $this->id]
            )->execute();
            Yii::$app->user->identity->LogActivity('booking', $this->id, 'create');
        } else {
            Yii::$app->user->identity->LogActivity('booking', $this->id, 'update');
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Mark record as deleted and hides fron list.
     * @return boolean
     */
    public function softDelete()
    {
        $connection = \Yii::$app->db;
        $connection->createCommand("update " . self::tableName() . " set trashed='1',trashed_at='" . date("Y-m-d H:i:s") . "',trashed_by='" . Yii::$app->user->identity->id . "' where id='" . $this->id . "'")->execute();
        Yii::$app->user->identity->LogActivity('booking', $this->id, 'delete');
        return true;
    }

    /**
     * Send booking email.
     * @return boolean
     */
    public function sendBookingEmail()
    {
        //Send Emails to member
        \Yii::$app->mailer->compose(['html' => 'newBookingMember-html', 'text' => 'newBookingMember-text'], ['model' => $this])
            ->setFrom([\Yii::$app->params['supportEmail'] => Yii::$app->params['siteName'] . ''])
            ->setTo(Yii::$app->user->identity->userEmails)
            ->setSubject('New Booking')
            ->send();

        //Send Email to Admin
        \Yii::$app->mailer->compose(['html' => 'newBookingAdmin-html', 'text' => 'newBookingAdmin-text'], ['model' => $this])
            ->setFrom([\Yii::$app->params['supportEmail'] => Yii::$app->params['siteName'] . ''])
            ->setTo(Yii::$app->params['bookingAdminEmail'])
            ->setSubject('New Booking By ' . Yii::$app->user->identity->username)
            ->send();
        return true;
    }
}