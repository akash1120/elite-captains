<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%boat_to_addon}}".
 *
 * @property int $id
 * @property int $boat_id
 * @property int $addon_id
 */
class BoatToAddon extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%boat_to_addon}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['boat_id', 'addon_id'], 'required'],
            [['boat_id', 'addon_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
			'boat_id' => Yii::t('app', 'Boat ID'),
			'addon_id' => Yii::t('app', 'Addon ID'),
		];
    }

    /**
     * Get boat row
     * @return \yii\db\ActiveQuery
     */
    public function getBoat()
    {
        return $this->hasOne(Boat::className(), ['id' => 'boat_id']);
    }

    /**
     * Get addon row
     * @return \yii\db\ActiveQuery
     */
    public function getAddon()
    {
        return $this->hasOne(Addon::className(), ['id' => 'addon_id']);
    }
}
