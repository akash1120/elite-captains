<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property string $payment_ref
 * @property int $invoice_id
 * @property string $trax_ref
 * @property string $status
 * @property int $payment_method_id
 * @property int $user_id
 * @property int $received_by
 * @property string $amount
 * @property string $note
 * @property string $date
 * @property string $response_code
 */
class Payments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    public $cheques = [];
    public $saved_cheques = [];

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payment_ref', 'invoice_id', 'status', 'payment_method_id', 'frequency', 'currency', 'user_id', 'amount'], 'required'],
            [['invoice_id', 'payment_method_id', 'user_id', 'received_by'], 'integer'],
            [['status', 'note', 'transaction_id', 'frequency', 'number_of_cheques'], 'string'],
            [['date', 'trax_ref', 'received_by', 'amount', 'currency', 'user_id_dropdown', 'number_of_cheques', 'cheques','discount'], 'safe'],
            [['payment_ref', 'trax_ref'], 'string', 'max' => 64],
            [['response_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_ref' => 'Payment Ref',
            'invoice_id' => 'Invoice ID',
            'trax_ref' => 'Trax Ref',
            'status' => 'Status',
            'payment_method_id' => 'Payment Method ID',
            'user_id' => 'User ID',
            'received_by' => 'Received By',
            'amount' => 'Amount',
            'note' => 'Note',
            'date' => 'Date',
            'response_code' => 'Response Code',
        ];
    }


    public function beforeSave($insert)
    {
        //update invoice status
        return true;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool
     */
    public function afterSave($insert, $changedAttributes)
    {

        if ($this->cheques <> null) {

            // PaymentCheques::deleteAll(['payment_id' => $this->id, 'customer_id' => $this->user_id]);

            foreach ($this->cheques as $cheque) {

                $paymentCheques = new PaymentCheques();
                $paymentCheques->payment_id = $this->id;
                $paymentCheques->customer_id = $this->user_id;
                $paymentCheques->invoice_id = $this->invoice_id;
                $paymentCheques->cheque_number = $cheque['cheque_number'];
                $paymentCheques->date = $cheque['date'];
                $paymentCheques->amount = $cheque['amount'];

                if (!$paymentCheques->save()) {
                    return false;
                }
            }
        }
        $previous_payments= Payments::find()->where(['invoice_id' => $this->invoice_id])->asArray()->all();
        $paid_amount = 0;
        if(isset($previous_payments) && $previous_payments <> null){
            foreach ($previous_payments as $key => $payment){
                $paid_amount = $paid_amount + $payment['amount'] + $payment['discount'];
            }
        }


        //update invoice status
        $invoice = Invoices::findOne($this->invoice_id);

        if ($invoice->total > $paid_amount) {
            $invoice->status = 'partial';
        } else {
            $invoice->status = 'paid';
        }

        //$invoice->status = 'paid';
        if (!$invoice->save()) {
            return false;
        }


        //update contract status
        $current_date = date('y-m-d');
        $contract = Contracts::findOne($invoice->contract_id);;
        $contract_end_date = date('Y-m-d', strtotime($contract->end_date));
        if ($contract->current_expiry <> null) {
            $contract_end_date = date('Y-m-d', strtotime($contract->current_expiry));
        }
        if ((strtotime($current_date) >= strtotime($contract->start_date)) && (strtotime($current_date) <= strtotime($contract_end_date))) {
            $contract->status = 'active';
            if (!$contract->save()) {
                return false;
            }
        }

        if($this->payment_method_id == 1) {
            $model_reciept = new Reciepts();
            $model_reciept->payment_ref = $this->payment_ref;
            $model_reciept->invoice_id = $this->invoice_id;
            $model_reciept->payment_method_id = 1;
            $model_reciept->frequency = $invoice->billing_cycle;
            $model_reciept->status = 'paid';
            $model_reciept->user_id = $invoice->user_id;
            $model_reciept->payment_id = $this->id;
            $model_reciept->amount = $this->amount;
            $model_reciept->discount = $this->discount;
            $model_reciept->date = date('Y-m-d H:i:s');
            if (!$model_reciept->save()) {
                // echo "<pre>";
                // print_r($this);
                //  var_dump($model_reciept->errors);
                return false;
            }
        }else if($this->payment_method_id == 1){

        }


    }

    /**
     * return html status
     */
    public function getTxtStatus()
    {
        if ($this->status == 'paid') {
            return '<span class="label label-success">Paid</span>';

        } else if ($this->status == 'cancelled') {
            return '<span class="label label-warning">Cancelled</span>';
        } else if ($this->status == 'partial') {
            return '<span class="label label-warning">Partial</span>';
        } else {
            return '<span class="label label-danger">declined</span>';
        }
    }

    /**
     * return User
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * return Method
     */
    public function getPaymentMethod()
    {
        return $this->hasOne(PaymentMethods::className(), ['id' => 'payment_method_id']);
    }

    /**
     * return Method
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoices::className(), ['id' => 'invoice_id']);
    }

    /**
     * Send booking email.
     * @return boolean
     */
    public function sendReceiptEmail($model, $terms)
    {
        //Send Emails to member
       /* $resp = \Yii::$app->mailer->compose(['html' => 'receipt-html', 'text' => 'receipt-html'], ['payment' => $model, 'terms' => $terms])
            ->setFrom([\Yii::$app->params['supportEmail'] => Yii::$app->params['siteName'] . ''])
            ->setTo(Yii::$app->user->identity->userEmails)
            //->setTo('akashahmed4all@gmail.com')
            ->setSubject('Receipt REC-' . $model->payment_ref)
            ->send();*/
        return true;
    }
}
