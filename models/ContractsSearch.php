<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contracts;

/**
 * ContractsSearch represents the model behind the search form of `app\models\Contracts`.
 */
class ContractsSearch extends Contracts
{
    public $pageSize;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'package_id', 'payment_method_id', 'payment_term_id', 'free_days', 'created_by', 'remaining_freeze_days', 'current_expiry', 'renewal_in'], 'integer'],
            [['start_date', 'end_date', 'check_number', 'check_date', 'card_number', 'card_expiry_date', 'cvv', 'status', 'update_at', 'create_at', 'renewal_in', 'renewal_duration'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = Contracts::find();

        $current_user = Yii::$app->user->identity->id;

        if (Yii::$app->user->identity->parent != 0) {
            $current_user = Yii::$app->user->identity->parent;
        }

        if (Yii::$app->user->identity->user_type == 0) {
            $query = Contracts::find()->
            where(['user_id' => $current_user]);
        } else {
            $query = Contracts::find();
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize != null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
            ],
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'package_id' => $this->package_id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'payment_method_id' => $this->payment_method_id,
            'payment_term_id' => $this->payment_term_id,
            'check_date' => $this->check_date,
            'card_expiry_date' => $this->card_expiry_date,
            'free_days' => $this->free_days,
            'created_by' => $this->created_by,
            'update_at' => $this->update_at,
            'create_at' => $this->create_at,
            'current_expiry' => $this->current_expiry,
            //  'renewal_in' => $this->renewal_in,
        ]);

        if ((isset($this->renewal_in) && $this->renewal_in <> null) && (isset($this->renewal_duration) && $this->renewal_duration <> null)) {
            $today = date("Y-m-d");
            $time = '+' . $this->renewal_in . ' ' . $this->renewal_duration;
            $query->andWhere('DATE(contracts.current_expiry) <= "' . date("Y-m-d", strtotime("$today $time")) . '"');
           // $query->andWhere('DATE(contracts.current_expiry) >= "' . $today. '"');
        }
        $query->andFilterWhere(['like', 'check_number', $this->check_number])
            ->andFilterWhere(['like', 'card_number', $this->card_number])
            ->andFilterWhere(['like', 'cvv', $this->cvv])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
