<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Booking;

/**
 * BookingSearch represents the model behind the search form of `app\models\Booking`.
 */
class BookingSearch extends Booking
{
	public $listType,$userName,$pageSize;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'city_id', 'marina_id', 'boat_id', 'time_slot_id', 'booking_type', 'is_archive', 'created_by', 'updated_by', 'trashed', 'trashed_by', 'pageSize'], 'integer'],
            [['reference_no','userName','listType','booking_date', 'booking_comments', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchActive($params)
    {
        $this->load($params);
        
        $query = Booking::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
			],
        ]);

        // grid filtering conditions
        if (!Yii::$app->user->identity->user_type) {
            $query->andFilterWhere(['and',
                ['>=', 'DATE(booking_date)', date("Y-m-d")],
                ['in', 'user_id', Yii::$app->user->identity->userIdz],
                ['trashed' => 0,]
            ]);
        }else{
            $query->andFilterWhere(['and',
                ['>=', 'DATE(booking_date)', date("Y-m-d")],
                ['trashed' => 0,]
            ]);
        }

        $query->andFilterWhere(['like', 'reference_no', $this->reference_no])
					->andFilterWhere(['like', 'booking_comments', $this->booking_comments]);
		
		$dataProvider->query->orderBy("`booking_date` desc");

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchAll($params)
    {
        $this->load($params);
        
        $query = Booking::find()->joinWith(['user']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
			],
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            Booking::tableName().'.city_id' => $this->city_id,
            Booking::tableName().'.marina_id' => $this->marina_id,
            'boat_id' => $this->boat_id,
            'booking_date' => $this->booking_date,
            'time_slot_id' => $this->time_slot_id,
            'booking_type' => $this->booking_type,
            'is_archive' => $this->is_archive,
        ]);

        $query->andFilterWhere(['and',
			['like', 'username', $this->userName],
			['like', 'booking_comments', $this->booking_comments],
			['like', 'reference_no', $this->reference_no],
		]);
		
		if($this->listType=='active'){
			$query->andFilterWhere(['and',['>=','DATE(booking_date)',date("Y-m-d")],[Booking::tableName().'.trashed' => 0,]]);
		}
		if($this->listType=='old'){
			$query->andFilterWhere(['and',['<','DATE(booking_date)',date("Y-m-d")],[Booking::tableName().'.trashed' => 0,]]);
		}
		if($this->listType=='all'){
			$query->andFilterWhere([Booking::tableName().'.trashed' => 0,]);
		}
		if($this->listType=='deleted'){
			$query->andFilterWhere([Booking::tableName().'.trashed' => 1,]);
		}
		
		$dataProvider->query->orderBy("`booking_date` desc");

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchHistory($params)
    {
        $this->load($params);
        
        $query = Booking::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
			],
        ]);

        // grid filtering conditions
        $query->andFilterWhere(['and',
			['<','DATE(booking_date)',date("Y-m-d")],
			['in','user_id', Yii::$app->user->identity->userIdz],
			['trashed' => 0,]
        ]);

        $query->andFilterWhere(['like', 'reference_no', $this->reference_no])
					->andFilterWhere(['like', 'booking_comments', $this->booking_comments]);
		
		$dataProvider->query->orderBy("`booking_date` desc");

        return $dataProvider;
    }
}