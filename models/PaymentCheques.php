<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment_cheques".
 *
 * @property int $id
 * @property int $payment_id
 * @property int $customer_id
 * @property int $invoice_id
 * @property string $cheque_number
 * @property string $date
 * @property string $amount
 */
class PaymentCheques extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_cheques';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payment_id', 'customer_id', 'invoice_id'], 'required'],
            [['payment_id', 'customer_id', 'invoice_id'], 'integer'],
            [['date'], 'safe'],
            [['cheque_number', 'amount'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'payment_id' => Yii::t('app', 'Payment ID'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'invoice_id' => Yii::t('app', 'Invoice ID'),
            'cheque_number' => Yii::t('app', 'Cheque Number'),
            'date' => Yii::t('app', 'Date'),
            'amount' => Yii::t('app', 'Amount'),
        ];
    }
    public function getUser(){
        return $this->hasOne(User::className(),['id' => 'customer_id']);
    }
    public function getPayment(){
        return $this->hasOne(Payments::className(),['id' => 'payment_id']);
    }
}
