<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reciepts".
 *
 * @property int $id
 * @property string $payment_ref
 * @property int $invoice_id
 * @property string $trax_ref
 * @property string $status
 * @property int $payment_method_id
 * @property int $user_id
 * @property int $received_by
 * @property string $amount
 * @property string $note
 * @property string $date
 * @property string $response_code
 * @property string $pt_invoice_id
 * @property string $transaction_id
 * @property string $frequency
 * @property string $currency
 * @property string $number_of_cheques
 * @property int $payment_id
 */
class Reciepts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reciepts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {

        return[
            [['payment_ref', 'invoice_id', 'status', 'payment_method_id', 'frequency','payment_id', 'user_id', 'amount'], 'required'],
            [['invoice_id', 'payment_method_id', 'user_id', 'received_by','payment_id'], 'integer'],
            [['status', 'note', 'transaction_id', 'frequency', 'number_of_cheques'], 'string'],
            [['date', 'trax_ref', 'received_by', 'amount', 'currency', 'user_id_dropdown', 'number_of_cheques', 'cheques','discount'], 'safe'],
            [['payment_ref', 'trax_ref'], 'string', 'max' => 64],
            [['response_code'], 'string', 'max' => 255],];

       /* return [
            [['payment_ref', 'invoice_id', 'trax_ref', 'status', 'payment_method_id', 'user_id', 'received_by', 'amount'], 'required'],
            [['invoice_id', 'payment_method_id', 'user_id', 'received_by', 'payment_id'], 'integer'],
            [['status', 'note'], 'string'],
            [['date'], 'safe'],
            [['payment_ref', 'trax_ref', 'amount'], 'string', 'max' => 64],
            [['response_code', 'pt_invoice_id', 'transaction_id', 'frequency', 'currency', 'number_of_cheques'], 'string', 'max' => 255],
        ];*/
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_ref' => 'Payment Ref',
            'invoice_id' => 'Invoice ID',
            'trax_ref' => 'Trax Ref',
            'status' => 'Status',
            'payment_method_id' => 'Payment Method ID',
            'user_id' => 'User ID',
            'received_by' => 'Received By',
            'amount' => 'Amount',
            'note' => 'Note',
            'date' => 'Date',
            'response_code' => 'Response Code',
            'pt_invoice_id' => 'Pt Invoice ID',
            'transaction_id' => 'Transaction ID',
            'frequency' => 'Frequency',
            'currency' => 'Currency',
            'number_of_cheques' => 'Number Of Cheques',
            'payment_id' => 'Payment ID',
        ];
    }
}
