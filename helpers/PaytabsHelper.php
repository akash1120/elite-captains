<?php
/**
 * Created by PhpStorm.
 * User: TOSHIBA
 * Date: 1/9/2019
 * Time: 12:05 AM
 */

namespace app\helpers;
use app\models\Payments;
use Yii;

@session_start();
define("TESTING", "https://localhost:8888/paytabs/apiv2/index");
define("AUTHENTICATION", "https://www.paytabs.com/apiv2/validate_secret_key");
define("PAYPAGE_URL", "https://www.paytabs.com/apiv2/create_pay_page");
define("VERIFY_URL", "https://www.paytabs.com/apiv2/verify_payment");

class PaytabsHelper
{
    public $api_key;
    private $merchant_id = "10039566";
    private $merchant_email = "info@elitecaptains.ae";
    private $merchant_secretKey = "zGoZtK0xHypxwqN0D8wQMldO1iPDHOVhvkgY5gwMPVjml7mZfTP1f2QglGT2vPj0azYfQv2Fqm1MuoMJiBLxEBNZiH2NxlNIUMYL";
    private $siteUrl = "http://booking.elitecaptains.ae";
    private $ipnUrl = "http://booking.elitecaptains.ae/site/verify-payment";
    private $returnUrl = "http://booking.elitecaptains.ae/site/verify-payment/";

    /**
     * paytabs constructor.
     * @param $merchant_email
     * @param $merchant_secretKey
     */
    function paytabs()
    {
        $this->merchant_email = "";
        $this->merchant_secretKey = "";
        $this->api_key = "";
    }

    /**
     * @return string
     */
    function authentication()
    {
        $obj = json_decode($this->runPost(AUTHENTICATION, array("merchant_email" => $this->merchant_email, "merchant_secretKey" => $this->merchant_secretKey)));
        if ($obj->access == "granted")
            $this->api_key = $obj->api_key;
        else
            $this->api_key = "";
        return $this->api_key;
    }

    /**
     * @param $url
     * @param $fields
     * @return mixed
     */
    function runPost($url, $fields)
    {

        $fields_string = "";

        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }

        rtrim($fields_string, '&');

        $ch = curl_init();
        $ip = $_SERVER['REMOTE_ADDR'];

        $ipaddress = array(
            "REMOTE_ADDR" => $ip,
            "HTTP_X_FORWARDED_FOR" => $ip
        );
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $ipaddress);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_REFERER, 1);

        $result = curl_exec($ch);
        // print_r($result);
        curl_close($ch);

        return $result;
    }

    /**
     * @param $values
     * @return mixed
     */
    function create_pay_page($invoice)
    {
        $user = null;
        if ($invoice <> null) {

            $user = ($invoice->user <> null) ? $invoice->user : null;
            $contract = ($invoice->contract <> null) ? $invoice->contract : null;
            $name = ($user->username <> null) ? $user->username : "";
        }
        $cc_name = explode(" ", $name);

        $values = [

            // Customer's Name on the invoice
            'title' => $name,
            // This will be prefilled as Credit Card First Name & Last Name
            'cc_first_name' => (isset($user->first_name)) ? $user->first_name : $user->username,
            'cc_last_name' => (isset($user->last_name)) ? $user->last_name : $user->username,
            'email' => ($user <> null) ? $user->email : "-",
            'cc_phone_number' => "+971",
            'phone_number' => ($user <> null) ? $user->mobile : "-",

            //Customer's Billing Address (All fields are mandatory)
            // When the country is selected as USA or CANADA, the state field should contain a String of 2 characters containing the ISO state code otherwise the payments may be rejected.
            // For other countries, the state can be a string of up to 32 characters.
            'billing_address' => ($user <> null) ? $user->address : "-",
            'city' => ($user <> null) ? ($user->city <> null) ? $user->city->title : "-" : "-",
            'state' => ($user <> null) ? ($user->city <> null) ? $user->city->title : "-" : "-",
            'postal_code' => ($user <> null) ? $user->postal_code : "-",
            'country' => "ARE",

            //Customer's Shipping Address (All fields are mandatory)
            'address_shipping' => ($user <> null) ? $user->address : "-",
            'city_shipping' => ($user <> null) ? ($user->city <> null) ? $user->city->title : "-" : "-",
            'state_shipping' => ($user <> null) ? ($user->city <> null) ? $user->city->title : "-" : "-",
            'postal_code_shipping' => ($user <> null) ? $user->postal_code : "-",
            'country_shipping' => "ARE",


            //Product title of the product. If multiple products then add “||” separator
            "products_per_title" => "Invoice",

            //Currency of the amount stated. 3 character ISO currency code
            'currency' => "AED",
            /* "unit_price" => ($invoice <> null) ? $invoice->total : "",  */                  // Unit price of the product. If multiple products then add “||” separator.
            'quantity' => "1",                    //Quantity of products. If multiple products then add “||” separator
            'other_charges' => "0",            //Additional charges. e.g.: shipping charges, taxes, VAT, etc.
            /*'amount' => ($invoice <> null) ? $invoice->total : "", */               //Amount of the products and other charges, it should be equal to: amount = (sum of all products’ (unit_price * quantity)) + other_charges
            //This field will be displayed in the invoice as the sub total field

            'discount' => "0",                    //Discount of the transaction. The Total amount of the invoice will be= amount - discount
            "msg_lang" => "english",            //Language of the PayPage to be created. Invalid or blank entries will default to English.(Englsh/Arabic)


            "reference_no" => ($invoice <> null) ? $invoice->invoice_id : "",        // Invoice reference number in your system
            "site_url" => $this->siteUrl, //The requesting website be exactly the same as the website/URL associated with your PayTabs Merchant Account
            'return_url' => $this->returnUrl,
            "cms_with_version" => "Website API 3.1"
        ];


        $values['merchant_email'] = $this->merchant_email;
        $values['merchant_secretKey'] = $this->merchant_secretKey;
        $values['secret_key'] = $this->merchant_secretKey;
        $values['ip_customer'] = $_SERVER['REMOTE_ADDR'];
        $values['ip_merchant'] = $_SERVER['SERVER_ADDR'];




        $previous_payments= Payments::find()->where(['invoice_id' => $invoice->id])->asArray()->all();
        $paid_amount = 0;
        if(isset($previous_payments) && $previous_payments <> null){
            foreach ($previous_payments as $key => $payment){
                $paid_amount = $paid_amount + $payment['amount'] + $payment['discount'];

            }
        }
        /*  echo "<pre>";
          print_r($invoice);
          die;*/

        //for recurring page
        if (($contract->paymentTerm <> null) && ($contract->paymentTerm->short_code != 'pia') && $paid_amount == 0 && $invoice->type=='contract') {

            $values['is_recurrence_payments'] = 'TRUE';

            if(Yii::$app->user->identity->id == 154){
                $values['recurrence_start_date']  =  date('d/m/Y', strtotime(' +1 day'));
            }else{
                $values['recurrence_start_date'] = ($invoice->contract <> null) ? date('d/m/Y', strtotime($invoice->contract->start_date)) : null;
            }

            // $values['recurrence_start_date'] = ($invoice->contract <> null) ? date('d/m/Y', strtotime($invoice->contract->start_date)) : null;;
            $values['recurrence_frequency'] = ($contract->frequency <> null) ? $contract->frequency : '3';
            $values['recurrence_billing_cycle'] = $contract->paymentTerm->short_code;
        }
        if($contract->frequency <> null && $contract->frequency > 1 && $invoice->type=='contract'){

            $values['amount'] = ($contract->recurring_amount <> null) ? $contract->recurring_amount :  $invoice->total;
            $values['unit_price'] = ($contract->recurring_amount <> null) ? $contract->recurring_amount :  $invoice->total;
        }else{
            $values['amount'] = ($invoice <> null) ? ($invoice->total - $paid_amount) : "";
            $values['unit_price'] = ($invoice <> null) ? ($invoice->total - $paid_amount) : "";
        }
        $values['is_tokenization'] = 'TRUE';
        //   if(($user <> null) && ($user->pay_page_url != '')) {
        $values['is_existing_customer'] = 'FALSE';

        //   }
        return json_decode($this->runPost(PAYPAGE_URL, $values));
    }

    /**
     * @return mixed
     */
    function send_request()
    {
        $values['ip_customer'] = $_SERVER['REMOTE_ADDR'];
        $values['ip_merchant'] = $_SERVER['SERVER_ADDR'];
        return json_decode($this->runPost(TESTING, $values));
    }

    /***
     * @param $payment_reference
     * @return mixed
     */
    function verify_payment($payment_reference)
    {
        $values['merchant_email'] = $this->merchant_email;
        $values['merchant_secretKey'] = $this->merchant_secretKey;
        $values['secret_key'] = $this->merchant_secretKey;
        $values['payment_reference'] = $payment_reference;
        return json_decode($this->runPost(VERIFY_URL, $values));
    }

    /**
     * @param $url
     * @param $fields
     * @return mixed
     */
    function responseCodes($code)
    {
        $response_array = array(
            "100" => "Payment is completed.",
            "481" => "This transaction may be suspicious. If this transaction is genuine, please contact PayTabs customer service to enquire about the feasibility of processing this transaction",
            "482" => "This transaction may be suspicious. If this transaction is genuine, please contact PayTabs customer service to enquire about the feasibility of processing this transaction",
            "810" => "You already requested Refund for this Transaction ID",
            "811" => "Amount is above or below the invoice and also the minimum balance",
            "812" => "Refund request is sent to Operation for Approval. You can track the Status",
            "813" => "You are not authorized to view this transaction",
            "0404" => "You don’t have permissions",
            "4001" => "Variable not found",
            "4002" => "Invalid Credentials.",
            "4006" => "Your time interval should be less than 60 days",
            "4007" => "'currency' code used is invalid. Only 3 character ISO currency codes are valid.",
            "4008" => "Your SITE URL is not matching with your profile URL",
            "4012" => "PayPage created successfully",
            "4013" => "Your 'amount' post variable should be between 0.27 and 5000.00 USD",
            "4014" => "Products titles, Prices, quantity are not matching",
            "4090" => "Data Found",
            "4091" => "There are no transactions available",
            "4094" => "Your total amount is not matching with the sum of unit price amounts per quantity ",
            "4404" => "You don't have permissions to create an Invoice",
            "5000" => "Payment has been rejected",
            "5001" => "Payment has been accepted successfully",
            "5002" => "Payment has been forcefully accepted",
            "5003" => "Payment has been refunded",
        );

        if (array_key_exists($code, $response_array)) {
            return $response_array[$code];
        } else {
            echo " the transaction has been rejected!";
        }

    }


}