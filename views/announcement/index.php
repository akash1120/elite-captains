<?php

use yii\helpers\Html;
use yii\web\JqueryAsset;
use app\widgets\CustomGridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AnnouncementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCssFile('@web/'.'plugins/datepicker/datepicker3.css', ['depends' => [JqueryAsset::className()]]);
$this->registerJsFile('@web/'.'plugins/datepicker/bootstrap-datepicker.js', ['depends' => [JqueryAsset::className()]]);

$this->title = Yii::t('app', 'Announcements');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('
	$(document).on("pjax:success", function() {
		initDtpicker();
	});
	initDtpicker();
');
?>
<div class="addon-index">

    <?= CustomGridView::widget([
		'create'=>true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:10px;'],],
            'heading',
            'date:date',
            'start_date:date',
            'end_date:date',
            ['format'=>'html','attribute'=>'color_class','value'=>function($model){return Yii::$app->params['noticeClass'][$model->color_class];},'filter'=>Yii::$app->params['noticeClass']],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>Yii::t('app','Actions'),
                'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],
                'contentOptions'=>['class'=>'noprint text-right'],
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fa fa-folder"></i>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'data-toggle'=>'tooltip',
                            'class'=>'btn btn-info btn-xs',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-edit"></i>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'data-toggle'=>'tooltip',
                            'class'=>'btn btn-success btn-xs',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-remove"></i>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'data-toggle'=>'tooltip',
                            'data-method'=>'post',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this?'),
                            'class'=>'btn btn-danger btn-xs',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
<script>
function initDtpicker(){
	$("input[name=\"AnnouncementSearch[date]\"]").datepicker({
		format: "yyyy-mm",
	}).on("changeDate", function(e){
		$(this).datepicker("hide");
	});
	$("input[name=\"AnnouncementSearch[start_date]\"]").datepicker({
		format: "yyyy-mm",
	}).on("changeDate", function(e){
		$(this).datepicker("hide");
	});
	$("input[name=\"AnnouncementSearch[end_date]\"]").datepicker({
		format: "yyyy-mm",
	}).on("changeDate", function(e){
		$(this).datepicker("hide");
	});
}
</script>
