<?php

use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Announcement */
/* @var $form yii\widgets\ActiveForm */

$this->registerCssFile('@web/'.'plugins/datepicker/datepicker3.css', ['depends' => [JqueryAsset::className()]]);
$this->registerJsFile('@web/'.'plugins/datepicker/bootstrap-datepicker.js', ['depends' => [JqueryAsset::className()]]);

$this->registerJs('
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
	}).on("changeDate", function(e) {
		$(this).datepicker("hide");
	});
');
?>
<div class="addon-form box">
	<?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'heading')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'date')->textInput(['class'=>'form-control dtpicker']) ?>
            </div>
        </div>
        <?= $form->field($model, 'descp')->textarea(['rows' => 6]) ?>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'start_date')->textInput(['class'=>'form-control dtpicker']) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'end_date')->textInput(['class'=>'form-control dtpicker']) ?>
            </div>
        </div>
        <?= $form->field($model, 'color_class')->dropDownList(Yii::$app->params['noticeClass']) ?>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app','Cancel'),['index'],['class'=>'btn btn-default'])?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
