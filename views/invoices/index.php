<?php

use app\widgets\CustomGridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InvoicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Invoices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoices-index">

    <?= CustomGridView::widget([
        'create' => false,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:10px;'],],
            [
                'attribute' => 'user_id',
                'label' => 'User',
                'value' => function ($model) {
                    return ($model->user <> null) ? $model->user->username : "-";
                }
            ],
            'invoice_id',
            /* 'contract_id',*/
            'invoice_date',
            'subtotal',
            'tax',
            'total',
            ['format' => 'html', 'attribute' => 'status', 'value' => function ($model) {
                return $model->txtStatus;
            }, 'filter' => Yii::$app->params['statusArr']],
            /*'billing_cycle',*/
            [
                'attribute' => 'billing_cycle',
                'label' => 'Billing Cycle',
                'value' => function ($model) {
                    return ($model->paymentTerms <> null) ? $model->paymentTerms->name : "-";
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['class' => 'noprint', 'style' => 'width:100px;'],
                'contentOptions' => ['class' => 'noprint'],
                'template' => '{invoice} {send-invoice} {view} {pay-online} {pay-manual} {update} {delete} {payment-detail}',
                'visibleButtons' => [
                    'view' => function ($model, $key, $index) {
                        return false;
                    },
                    'pay-online' => function ($model, $key, $index) {
                        if ($model->status != 'paid') {
                            return true;
                        } else {
                            return false;
                        }
                    },
                    'pay-manual' => function ($model, $key, $index) {
                        if ($model->status != 'paid') {
                            return true;
                        } else {
                            return false;
                        }
                    },
                    'update' => function ($model, $key, $index) {
                        return false;
                    },
                    'delete' => function ($model, $key, $index) {
                        return false;
                    },
                    'payment-detail' => function ($model, $key, $index) {
                        return true;
                    },
                ],
                'buttons' => [

                    'invoice' => function ($url, $model) {
                        if (Yii::$app->user->identity->user_type == 1) {
                            $url = \yii\helpers\Url::to(['/invoices/send-invoice-email/', 'id' => $model->id, 'mode' => 'invoice']);
                            return Html::a('<i class="fa fa-envelope"></i>', $url, [
                                'title' => Yii::t('app', 'Email'),
                                'data-toggle' => 'tooltip',
                                'class' => 'btn btn-info btn-xs',
                            ]);
                        } else {
                            return false;
                        }

                    },
                    'pay-online' => function ($url, $model) {
                        $url = \yii\helpers\Url::to(['/site/invoice-payment/', 'invoice_id' => $model->id]);
                        return Html::a('<i class="fa fa-credit-card"></i>', $url, [
                            'title' => Yii::t('app', 'Pay Online'),
                            'data-toggle' => 'tooltip',
                            'class' => 'btn btn-info btn-xs',
                        ]);
                    },
                    'payment-detail' => function ($url, $model) {
                        $url = \yii\helpers\Url::to(['/invoices/viewpaymentdetail/', 'id' => $model->id]);
                        return Html::a('<i class="fa fa-list"></i>', $url, [
                            'title' => Yii::t('app', 'Payment Detail'),
                            'data-toggle' => 'tooltip',
                            'class' => 'btn btn-info btn-xs',
                        ]);
                    },
                    'pay-manual' => function ($url, $model) {
                        if (Yii::$app->user->identity->user_type == 1) {
                            $url = \yii\helpers\Url::to(['/payments/create/', 'invoice_id' => $model->id]);
                            return Html::a('<i class="fa fa-money"></i>', $url, [
                                'title' => Yii::t('app', 'Pay Manual'),
                                'data-toggle' => 'tooltip',
                                'class' => 'btn btn-info btn-xs',
                                'style' => 'margin-top:3px',
                            ]);
                        }
                    },
                    'send-invoice' => function ($url, $model) {
                        $url = \yii\helpers\Url::to(['/invoices/view-invoice/', 'id' => $model->id, 'mode' => 'download']);
                        return Html::a('<i class="fa fa-folder"></i>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'data-toggle' => 'tooltip',
                            'class' => 'btn btn-info btn-xs',
                        ]);

                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-edit"></i>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'data-toggle' => 'tooltip',
                            'class' => 'btn btn-success btn-xs',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-remove"></i>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'data-toggle' => 'tooltip',
                            'data-method' => 'post',
                            'data-confirm' => Yii::t('app', 'Are you sure you want to delete this?'),
                            'class' => 'btn btn-danger btn-xs',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
