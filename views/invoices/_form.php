<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
/*use yii\helpers\Html;
use yii\widgets\ActiveForm;*/
use app\models\User;
use app\models\Contracts;
use app\models\Addon;
use app\models\ContarctAddons;
use app\models\PaymentMethods;
use app\models\PaymentTerms;
use app\models\TermsAndConditions;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Invoices */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .padding20 {
        padding: 20px;
    }

    .hide {
        display: none;
    }
</style>
<div class="user-form">
    <div class="box box-success">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="box-body">
            <div class="panel panel-default ">
                <div class="panel-heading"><strong>Required Information</strong></div>
                <div class="row padding20">
                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(user::find()->where(['user_type' => 1, 'status' => 1])->orderBy('username')->asArray()->all(), 'id', 'username'))->label('Users') ?>
                    </div>
                    <div class="col-xs-12 col-sm-6">


                        <?= $form->field($model, 'invoice_date')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'Enter birth date ...'],
                            'value' => date('Y-m-d'),
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="col-xs-12 col-sm-6" style="clear: both;">
                        <?= $form->field($model, 'subtotal')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'tax')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'total')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
            </div>

            <div class="panel panel-default ">
                <div class="panel-heading"><strong>Addons</strong></div>
                <div class="row padding20">
                    <div class="col-xs-4 col-sm-4">
                        <label class="control-label" for="Contracts-addons_quantity">Addons </label>
                    </div>
                    <div class="col-xs-4 col-sm-4">
                        <label class="control-label" for="Contracts-addons_quantity"> Free Allowed</label>
                    </div>
                    <div class="col-xs-4 col-sm-4">
                        <label class="control-label" for="Contracts-addons_quantity"> Price of Additional usage</label>
                    </div>
                    <div id="addons">
                        <?php
                        if (!empty($model->saved_addons)) {
                            foreach ($model->saved_addons as $key => $addon) {
                                ?>
                                <div class="col-xs-4 col-sm-4">
                                    <div class="form-group field-contracts-addons-0-addon_name">
                                        <input type="text" id="contracts-addons-<?= $key ?>-addon_name"
                                               class="form-control" value="<?= $addon['addon_title']; ?>"
                                               name="Contracts[addons][<?= $key ?>][addon_name]">
                                        <div class="help-block"></div>
                                    </div>
                                    <div class="form-group field-contracts-addons-0-addon_id">
                                        <input type="text" id="contracts-addons-<?= $key ?>-addon_id" class="hide"
                                               value="<?= $addon['addon_id']; ?>"
                                               name="Contracts[addons][<?= $key ?>][addon_id]">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <div class="form-group field-contracts-addons-0-addons_quantity">
                                        <input type="text" id="contracts-addons-<?= $key ?>-addons_quantity"
                                               class="form-control" value="<?= $addon['qunatity']; ?>"
                                               name="Contracts[addons][<?= $key ?>][addons_quantity]">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <div class="form-group field-contracts-addons-0-addons_price">
                                        <input type="text" id="contracts-addons-<?= $key ?>-addons_price"
                                               class="form-control" value="<?= $addon['price']; ?>"
                                               name="Contracts[addons][<?= $key ?>][addons_price]">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>

                    </div>
                </div>
            </div>

                <div class="panel panel-default ">
                    <div class="panel-heading"><strong>General Information</strong></div>
                    <div class="row padding20">
                        <div class="col-xs-12 col-sm-6">
                            <?= $form->field($model, 'billing_cycle')->dropDownList(ArrayHelper::map(PaymentTerms::find()->where(['status' => 1])->orderBy('name')->asArray()->all(), 'id', 'name'))->label('Billing Cycle') ?>
                        </div>

                        <div class="col-xs-12 col-sm-6">
                            <?= $form->field($model, 'status')->dropDownList(['pending' => 'Pending', 'paid' => 'Paid', 'cancelled' => 'Cancelled']) ?>
                        </div>
                       <!-- <div class="col-xs-12 col-sm-6">
                            <?/*= $form->field($model, 'last_payment_date')->textInput() */?>
                        </div>-->
                        <div class="col-xs-12">
                            <?= $form->field($model, 'invoice_description')->textarea(['rows' => 2]) ?>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default ">
                    <div class="panel-heading"><strong>Terms And Conditions</strong></div>
                    <div class="row padding20">

                        <div class="col-xs-12">
                            <?= $form->field($model, 'invoice_terms')->textarea(['rows' => 2]) ?>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <?= $form->field($model, 'client_notes')->textarea(['rows' => 2]) ?>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <?= $form->field($model, 'admin_notes')->textarea(['rows' => 2]) ?>
                        </div>

                    </div>
                </div>


                <div class="box">
                    <div class="box-body">
                        <div class="form-group">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                            <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end();

                ?>
            </div>



