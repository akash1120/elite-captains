<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Invoices;
use app\models\User;
use app\models\Package;
use app\models\Contracts;
use app\models\TermsAndConditions;

/* @var $this yii\web\View */
/* @var $model app\models\Invoices */

$this->title = $invoice->id;
$this->params['breadcrumbs'][] = ['label' => 'Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->title = 'Search Results';
$this->params['breadcrumbs'][] = $this->title;
$this->params['invoice_title'] = $this->title;

$member = ($invoice->user <> null) ? $invoice->user : null;
$contract = ($invoice->contract <> null) ? $invoice->contract : null;
?>

<section class="MainArea" style="background:#fff;font-family: 'Century Gothic'">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                <div class="col-3 col-sm-3 col-md-3 col-lg-8 col-xl-8" style="width: 60%; float: left;">
                    <a href="/"><img src="<?= Yii::$app->params['siteUrl']?>/images/logo.jpg" alt="BBG-Dubai"></a>
                </div>

                <div class="col-3 col-sm-3 col-md-4 col-lg-4 col-xl-4" style="width: 30%; float: right;">
                    <h1 style="color: #000;"><b><?= 'Invoice' ?></b></h1>
                    <strong><span>Invoice date: <?php echo date("d-M-Y", strtotime($invoice->invoice_date)); ?></span></strong><br/>
                    <strong><span>Invoice number: <?= str_pad($invoice->invoice_id, 7, "0", STR_PAD_LEFT); ?></span></strong>
                </div>

                <div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-6" style="width: 60%; float: left;">
                    <br/><br/>
                    <strong><br/><?= "Elite Captain"; ?></strong>
                    <strong><br/>Telephone: <?= "+971 234 56765"; ?><br/></strong>
                    <strong>Email: <?=  "admin@gmail.com"; ?><br/></strong>
                    <strong>VAT Registration no:
                        <u><?= "100009222233388877555"; ?></u><br/></strong>
                </div>

                <div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-6" style="width: 30%; float:right;">
                    <br/>
                    <strong>Bill To:</strong><br/>
                    <strong>Name: <?= ($member <> null) ? $member->username :""; ?>
                 <!--   <br/>
                    <strong>Company: <?/*= ($member <> null && $member->accountCompany <> null) ? $member->accountCompany->name : ""; */?></strong>-->
                    <br/>
                    <!--<strong>Address: <?/*= ($member <> null) ? $member->address : ""; */?><br/></strong>-->
<!--                    <strong>P.O
                        Box: <?/*= ($member <> null && $member->accountCompany <> null) ? $member->accountCompany->postal_code : ""; */?><br/></strong>-->
                    <strong>Telephone: <?= ($member <> null) ? $member->mobile : ""; ?></strong>
                    <br/>
                 <!--   <strong>VAT Registration no:
                        <u><?/*= ($member <> null) ? $member->vat_number : ""; */?></u><br/><br/>
                    </strong>-->
                </div>

                <div style="width: 100%;" class="col-3 col-sm-3 col-md-3 col-lg-12 col-xl-12">
                    <div style="width: 100%;">
                        <?php if($invoice->type == 'addon' && $invoice->addon_id > 0){ ?>
                            <table class="table" style="width: 100%; float: left;" cellpadding="5">
                                <thead>
                                <tr>
                                    <th class="col-md-8"
                                        style="width:70%; background: #1d355f; color: #ffffff; text-align: center;">Addon
                                    </th>
                                    <th class="col-md-4"
                                        style="width:30%; background: #1d355f; color: #ffffff; text-align: center">Amount
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $captain =  \app\models\Addon::find()->where(['id' => $invoice->addon_id])->one();
                                if ($captain <> null) {
                                    ?>
                                }
                                <tr>
                                                        <td>
                                                            <h3> <?= $captain->title ?> </h3>
                                                        </td>
                                                        <td style="text-align: center;">AED <?= $captain->price; ?></td>
                                </tr>

                               <?php }
                                ?>
                                </tbody>
                            </table>
                        <?php }else{ ?>
                        <table class="table" style="width: 100%; float: left;" cellpadding="5">
                            <thead>
                            <tr>
                                <th class="col-md-8"
                                    style="width:70%; background: #1d355f; color: #ffffff; text-align: center;">Package
                                </th>
                                <th class="col-md-4"
                                    style="width:30%; background: #1d355f; color: #ffffff; text-align: center">Amount
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if ($contract <> null) {
                                if ($contract<> null) {

                                        $package = Package::find()->where(['id' => $contract['package_id']])->one();
                                        if ($package <> null) {
                                            $_package = $package->title;
                                        }

                                        ?>
                                        <tr>
                                            <td>
                                                <?= ($_package <> null) ? $_package : ""; ?>
                                                <br/> <?=  '( From:  '. $contract['start_date'] . ' to '. $contract['end_date']; ?>
                                            </td>
                                            <td style="text-align: center;">AED <?= $contract['price']; ?></td>
                                        </tr>
                                        <?php
                                    }

                            }
                            ?>
                            </tbody>
                        </table>
                        <?php } ?>
                    </div>
                    <?php if($invoice->contract->first_contract_check == 1){ ?>
                    <div style="clear:both;width: 100%;border-top: #000 solid 1px;margin-top: 30px;">
                        <table class="table" style="width: 100%; float: left;" cellpadding="5">
                            <tbody>

                            <tr>
                                <td style="width:70%;text-align: left;">
                                    <strong>Joining Fee</strong>
                                </td>
                                <td style="width:30%;text-align: center;">
                                    AED <?= ($invoice <> null) ? $invoice->joining_fee : 0; ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                   <?php } ?>
                    <div style="clear:both;width: 100%;border-top: #000 solid 1px;margin-top: 30px;">
                        <table class="table" style="width: 100%; float: left;" cellpadding="5">
                            <tbody>

                            <tr>
                                <td style="width:70%;text-align: left;">
                                    <strong>Subtotal</strong>
                                    <br/><strong>VAT(5%)</strong>
                                </td>
                                <td style="width:30%;text-align: center;">
                                    <strong><?= ($invoice <> null) ? $invoice->subtotal : 0; ?></strong>
                                    <br/><strong><?= ($invoice <> null) ? $invoice->tax : 0; ?></strong>
                                </td>
                            </tr>

                            <tr>
                                <td style="width:70%;text-align: left;">
                                    <br/><strong>Total</strong>
                                </td>
                                <td style="width:30%;text-align: center;">
                                    <br/>
                                    <strong>
                                        AED <?= ($invoice <> null) ? $invoice->subtotal + $invoice->tax : 0; ?>
                                        <input type="hidden"
                                               value="<?= ($invoice <> null) ? $invoice->subtotal + $invoice->tax : 0; ?>"
                                               id="actual_total"/>
                                    </strong>
                                </td>
                            </tr>

                            <?php
                           /* if ($invoice <> null) {
                                if ($invoice->adjustment <> null) {
                                    $adjustment = $invoice->adjustment;
                                    */?><!--
                                    <tr>
                                        <td style="width:70%;text-align: left;">
                                            <strong>Adjustment</strong>
                                            <br/>
                                            <?/*= $adjustment->reason; */?>
                                        </td>
                                        <td style="width:30%;text-align: right;">
                                            <strong>
                                                <?/*= $adjustment->type . " " . $adjustment->adjustment; */?>
                                            </strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:70%;text-align: left;"><strong>Total Payable</strong>
                                        </td>
                                        <td style="width:30%;text-align: right;">
                                            <strong>AED <?/*= ($invoice <> null) ? $invoice->total : 0; */?></strong>
                                        </td>
                                    </tr>
                                    --><?php
/*                                }
                            }*/
                            ?>
                            <hr>
                            </tbody>
                        </table>
                    </div>
                </div>



                <div class="col-3 col-sm-3 col-md-3 col-lg-12 col-xl-12">
                    <?php
                    if ($invoice <> null) {
                        if ($invoice->status == 'unpaid') {
                            echo \yii\helpers\Html::a('Pay Online', [
                                \Yii::$app->params['appUrl'] . "/site/invoice-payment", 'invoice_id' => $invoice->invoice_id
                            ]);
                            echo '<br/>';
                        }
                    }
                    ?>

                </div>
                <div class="col-3 col-sm-3 col-md-3 col-lg-12 col-xl-12">

                <strong>Terms & Conditions</strong>
                <p><?= $terms['terms']; ?></p>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    .searchResult .view a {
        font-weight: bold;
    }

    #events, #news {
        background: #ffffff;
    }

    .searchResult .view {
        padding: 5px 5px 10px;
        margin-bottom: 10px;
        border-bottom: dashed 1px #ccc;
        -webkit-transition: background 0.2s;
        transition: background 0.2s;
    }
</style>
