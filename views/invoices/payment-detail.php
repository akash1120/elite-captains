<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\widgets\CustomGridView;

/* @var $this yii\web\View */
/* @var $model app\models\Invoices */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoices-view box">

    <div class="box-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'User',
                'value' => function ($model) {
                    return ($model->user <> null) ? $model->user->username : "-";
                }
            ],
            'invoice_id',
            'contract_id',
            'invoice_date',
            'subtotal',
            'tax',
            'total',
            [
                'attribute' => 'total',
                'label' => 'Paid Amount',
                'value' => $paid_amount
            ],
            [
                'attribute' => 'total',
                'label' => 'Remaining Balance',
                'value' => $model->total - $paid_amount - $discount
            ],
            [
                'attribute' => 'total',
                'label' => 'Discount',
                'value' => $discount
            ],
            'txtStatus:html',
            'admin_notes',
            'client_notes',
            'invoice_terms',
            ],
        ]) ?>
    </div>
</div>

<div class="payments-index">
    <section class="content-header" style="padding-top:0px;">
        <h1>Payment Records</h1>
    </section>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'payment_ref',
            [
                'attribute' => 'invoice_id',
                'label' => 'Invoice Number',
                'value' => function ($model) {
                    return ($model->invoice <> null) ? $model->invoice->invoice_id : "-";
                }
            ],
            [
                'attribute' => 'user_id',
                'label' => 'User',
                'value' => function ($model) {
                    return ($model->user <> null) ? $model->user->username : "-";
                }
            ],
            [
                'attribute' => 'payment_method_id',
                'label' => 'Method',
                'value' => function ($model) {
                    return ($model->paymentMethod <> null) ? $model->paymentMethod->name : "-";
                }
            ],
            'amount',
            'discount',
            'date',
            'transaction_id',
            'frequency',
            ['format' => 'html', 'attribute' => 'status', 'value' => function ($model) {
                return $model->txtStatus;
            }, 'filter' => Yii::$app->params['statusArr']],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['class' => 'noprint', 'style' => 'width:100px;'],
                'contentOptions' => ['class' => 'noprint text-center'],
                'template' => ' {cheques}',
                'visibleButtons' => [
                    'cheques' => function ($model, $key, $index) {
                        if ($model->payment_method_id == 3) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                ],
                'buttons' => [
                    'cheques' => function ($url, $model) {
                        $url = \yii\helpers\Url::to(['/payments/view-cheques/', 'id' => $model->id]);
                        return Html::a('<i class="fa fa-bank"></i>', $url, [
                            'title' => Yii::t('app', 'Cheques Detail'),
                            'data-toggle' => 'tooltip',
                            'class' => 'btn btn-info btn-xs',
                        ]);

                    },
                ],
            ],
        ],

    ]); ?>
</div>