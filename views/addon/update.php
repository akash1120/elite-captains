<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Addon */

$this->title = Yii::t('app', 'Update Addon: {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Addons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="addon-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
