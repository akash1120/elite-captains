<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Addon */

$this->title = Yii::t('app', 'Create Addon');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Addons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="addon-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
