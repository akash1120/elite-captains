<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FreezeContract */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Freeze Contracts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="freeze-contract-view card-box">

   <!-- <h1><?/*= Html::encode($this->title) */?></h1>-->

    <p class="pull-right">

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'User',
                'value' => function ($model) {
                    return ($model->user <> null) ? $model->user->username : "-";
                }
            ],
            'contract_id',
            'start_date',
            'end_date',
            'status',
            'date',
        ],
    ]) ?>

</div>
