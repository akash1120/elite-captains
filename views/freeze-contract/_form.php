<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FreezeContract */
/* @var $form yii\widgets\ActiveForm */
?>
<?php

?>
<p style="text-align: right;">
    <span class="label label-warning">
        <strong>
            <?= ($contract <> null) ? "Remaining Freeze Days: " . $contract[0]->remaining_freeze_days : ""; ?>
        </strong>
    </span>
</p>
<div class="freeze-contract">
    <div class="box box-success">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="box-body">
            <div class="panel panel-default ">
                <div class="panel-heading"><strong>Required Information</strong></div>
                <div class="row padding20">
                    <div class="col-md-12">
                        <?php $form = ActiveForm::begin(); ?>

                        <div class="col-xs-12 col-sm-12">
                            <?= $form->field($model, 'contract_id')->dropDownList(\yii\helpers\ArrayHelper::map($contract, 'id', function ($contract) {
                                return ($contract->user <> null) ? $contract->id . " -> " . $contract->user->username : "-";
                            }))->label('Contract') ?>
                        </div>

                        <?php if($edit == 'create'){ ?>
                        <!--<div class="col-xs-12 col-sm-6">
                            <?/*= $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                                'options' => ['placeholder' => 'Enter Start date ...'],
                                'type' => DatePicker::TYPE_INPUT,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd'
                                ]
                            ]);
                            */?>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <?/*= $form->field($model, 'end_date')->widget(DatePicker::classname(), [
                                'options' => ['placeholder' => 'Enter End date ...'],
                                'type' => DatePicker::TYPE_INPUT,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd'
                                ]
                            ]);
                            */?>
                        </div>-->
                            <div class="col-xs-12 col-sm-6">
                                <?php
                                $start_date = date('Y-m-d');
                                $end_date = date('Y-m-d');

                                if ($model->start_date) {
                                    $start_date = date('Y-m-d', strtotime($model->start_date));
                                }
                                if ($model->end_date) {
                                    $end_date = date('Y-m-d', strtotime($model->end_date));
                                }


                                echo '<label class="control-label">Valid Dates</label>';
                                echo DatePicker::widget([
                                    'name' => 'FreezeContract[start_date]',
                                    'value' => $start_date,
                                    'type' => DatePicker::TYPE_RANGE,
                                    'name2' => 'FreezeContract[end_date]',
                                    'value2' => $end_date,
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm-dd'
                                    ],
                                ]);
                                ?>
                            </div>


                        <?php }else{ ?>

                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group ">
                                    <input type="text" id="" class="form-control" name="" value="<?= date('d-m-Y',strtotime($model['start_date'])); ?>" readonly>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <input type="text" id="" class="form-control" name="" value="<?= date('d-m-Y',strtotime($model['end_date'])); ?>" readonly>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-xs-12 col-sm-6">
                            <?= $form->field($model, 'status')->dropDownList(['active' => 'Active']) ?>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>