<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FreezeContractSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Freeze Contracts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="freeze-contract-index card-box">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="pull-right">
        <?=  Html::a('Create', ['create'], ['class' => 'btn btn-success'])?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'User',
                'value' => function ($model) {
                    return ($model->user <> null) ? $model->user->username : "-";
                }
            ],
            'contract_id',
            'start_date',
            'end_date',
            ['format' => 'html', 'attribute' => 'status', 'value' => function ($model) {
                return $model->status;
            }, 'filter' => Yii::$app->params['statusFreeze']],
            'date',
            'pause_date',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['class' => 'noprint', 'style' => 'width:100px;'],
                'contentOptions' => ['class' => 'noprint'],
                'template' => '{view} {update} {pause} {daysremove} {delete}',
                'visibleButtons' => [
                    'view' => function ($model, $key, $index) {
                        return false;
                    },

                    'update' => function ($model, $key, $index) {
                        return false;
                    },
                    'pause' => function ($model, $key, $index) {

                        $current_date = date('y-m-d');
                        if ((strtotime($model->start_date) <= strtotime($current_date)) && (strtotime($model->end_date) > strtotime($current_date)) && ($model->status == 'active')) {
                            return true;
                        }else{
                            return false;
                        }
                    },
                    'daysremove' => function ($model, $key, $index) {
                        $current_date = date('y-m-d');
                        if ((strtotime($model->start_date) > strtotime($current_date))   && ($model->status == 'active')) {
                            return true;
                        }else{
                            return false;
                        }

                       // return true;
                    },
                    'delete' => function ($model, $key, $index) {
                        return false;

                        // return true;
                    },
                ],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-edit"></i>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'data-toggle' => 'tooltip',
                            'class' => 'btn btn-success btn-xs',
                        ]);
                    },
                    'pause' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pause"> Unfreeze</i>', $url, [
                            'title' => Yii::t('app', 'UnFreeze Contract'),
                            'data-toggle' => 'tooltip',
                            'class' => 'btn btn-success btn-xs',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash"></i>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'data-toggle' => 'tooltip',
                            'class' => 'btn btn-danger btn-xs',
                        ]);
                    },
                    'daysremove' => function ($url, $model) {

                            $url = \yii\helpers\Url::to(['/freeze-contract/remove-days/', 'id' => $model->id]);
                            return Html::a('<i class="fa fa-trash"></i>', $url, [
                                'title' => Yii::t('app', 'Delete'),
                                'data-toggle' => 'tooltip',
                                'class' => 'btn btn-danger btn-xs',
                            ]);

                    },
                ],
            ],
        ],
    ]); ?>
</div>
