<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FreezeContract */

$this->title = 'Freeze Contract';
$this->params['breadcrumbs'][] = ['label' => 'Freeze Contracts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="freeze-contract-create card-box">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'contract' => $contract,
        'edit' => 'create'
    ]) ?>

</div>
