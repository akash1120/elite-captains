<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FreezeContract */
//
$this->title = 'Update Freeze Contract: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Freeze Contracts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="freeze-contract-update card-box">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'contract' => $contract,
        'edit' => 'update'
    ]) ?>

</div>
