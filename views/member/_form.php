<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\City;
use app\models\Package;
use app\assetfiles\DatePickerAsset;
DatePickerAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs('
	$(document).delegate("#user-package_id", "change", function() {
		if($(this).val()=='.Yii::$app->params['sharingPackageId'].'){
			//$("#subUser").show();
		}else{
			$("#subUser").hide();
		}
	});
	$("#subUser").hide();
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
		endDate: new Date(),
	}).on("changeDate", function(e) {
		$(this).datepicker("hide");
	});
');
?>
<div class="user-form">
    <div class="box box-success">
    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
    <div class="box-body">
        <div class="row">
            <div class="col-xs-12 col-sm-12"><?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map(City::find()->where(['status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title')) ?></div>
           <!-- <div class="col-xs-12 col-sm-6">
                <?/*= $form->field($model, 'package_id')->dropDownList(ArrayHelper::map(Package::find()->where(['status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title'),['prompt'=>Yii::t('app','Select')]) */?>
            </div>-->
        </div>
     <!--   <div class="row">
            <div class="col-xs-12 col-sm-6"><?/*= $form->field($model, 'package_start')->textInput(['class'=>'form-control dtpicker','autocomplete'=>'off','maxlength' => true]) */?></div>
            <div class="col-xs-12 col-sm-6"><?/*= $form->field($model, 'package_end')->textInput(['class'=>'form-control dtpicker','autocomplete'=>'off','maxlength' => true]) */?></div>
        </div>-->
        <div class="row">
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?></div>
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?></div>
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'date_of_birth')->textInput(['class'=>'form-control dtpicker']) ?></div>
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'registration_no')->textInput(['maxlength' => true]) ?></div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?></div>
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'new_password')->textInput() ?></div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?></div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'is_licensed')->dropDownList(Yii::$app->params['isLicensedArr']) ?></div>
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'status')->dropDownList(Yii::$app->params['statusArr']) ?></div>
        </div>
        <?php if($model->imageSrc!=null && $model->imageSrc!=Yii::$app->params['default_avatar']){?>
        <div class="form-group">
            <label class="control-label"><?= Yii::t('app','Old Image')?></label>
            <div><img src="<?= $model->imageSrc?>" width="128" /></div>
        </div>
        <?php }?>
        <?= $form->field($model, 'image')->fileInput() ?>
        <div class="row">
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'notice')->textInput(['maxlength' => true]) ?></div>
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'notice_class')->dropDownList(Yii::$app->params['noticeClass']) ?></div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?></div>
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'vat_number')->textInput(['maxlength' => true]) ?></div>
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'address')->textarea(['rows' => 2]); ?></div>
        </div>
    </div>
    <div id="subUser"<?= $model->id!=null && $model->parent==0 && $model->package_id==Yii::$app->params['sharingPackageId'] ? ' style="display:block;"' : ' style="display:none;"'?>>
        <div class="box box-success">
            <div class="box-header"><h3 class="box-title">Second User Detail</h3></div>
            <div class="hidden"><?= $form->field($model, 'sub_user_id')->textInput() ?></div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'sub_city_id')->dropDownList(ArrayHelper::map(City::find()->where(['status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title')) ?></div>
                    <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'sub_username')->textInput(['maxlength' => true]) ?></div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'sub_new_password')->textInput() ?></div>
                    <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'sub_email')->textInput(['maxlength' => true]) ?></div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'sub_mobile')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'sub_is_licensed')->dropDownList(Yii::$app->params['isLicensedArr']) ?></div>
                </div>
				<?php if($model->sub_imageSrc!=null && $model->sub_imageSrc!=Yii::$app->params['default_avatar']){?>
                <div class="form-group">
                    <label class="control-label"><?= Yii::t('app','Old Image')?></label>
                    <div><img src="<?= $model->sub_imageSrc?>" width="128" /></div>
                </div>
                <?php }?>
                <?= $form->field($model, 'sub_image')->fileInput() ?>
                <div class="row">
                    <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'sub_notice')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'sub_notice_class')->dropDownList(Yii::$app->params['noticeClass']) ?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-body">
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                <?= Html::a(Yii::t('app','Cancel'),['index'],['class'=>'btn btn-default'])?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>