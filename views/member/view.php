<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->username.$model->subUserNames;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$subUsers=$model->subUsers;
?>
<div class="user-view box">
    <div class="box-header with-border">
        <h3></h3>
        <div class="box-tools pull-right">
            <?= Html::a('<i class="fa fa-edit"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-box-tool', 'data-toggle'=>'tooltip', 'title'=>'Update']) ?>
            <?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn btn-box-tool', 'data-toggle'=>'tooltip', 'title'=>'Delete', 'data'=>['method' => 'post', 'confirm' => Yii::t('app', 'Are you sure you want to delete this?'),]]) ?>
            <?= Html::a('<i class="fa fa-times"></i>', ['index'], ['class' => 'btn btn-box-tool', 'data-toggle'=>'tooltip', 'title'=>'Back']) ?>
        </div>
    </div>
    <div class="box-body">
    	<div class="row">
        	<div class="col-xs-12<?= $subUsers!=null ? ' col-sm-6' : ''?>">
				<?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
	        			['attribute'=>'imageSrc','format'=>['image',['width'=>'100','height'=>'100']]],
                        'username',
                        ['attribute'=>'city_id','value'=>$model->city->title],
                        ['attribute'=>'package_id','value'=> (isset($model->package->title)) ? $model->package->title :'-'],
                        'email:email',
                        'mobile',
                        'txtLicensed:html',
                        'txtStatus:html',
            			'noticeText:html',
                        ],
                    ]) ?>
            </div>
        	<?php if($subUsers!=null){foreach($subUsers as $subUser){?>
            <div class="col-xs-12 col-sm-6">
				<?= DetailView::widget([
                    'model' => $subUser,
                    'attributes' => [
	        			['attribute'=>'imageSrc','format'=>['image',['width'=>'100','height'=>'100']]],
                        'username',
                        ['attribute'=>'city_id','value'=>$subUser->city->title],
                        ['attribute'=>'package_id','value'=>(isset($model->package->title)) ? $model->package->title :'-'],
                        'email:email',
                        'mobile',
                        'txtLicensed:html',
                        'txtStatus:html',
            			'noticeText:html',
                        ],
                    ]) ?>
            </div>
            <?php }}?>
        </div>
    </div>
</div>
<div class="box box-warning">
    <div class="box-header">
        <div class="note-form">
            <?php $form = ActiveForm::begin(); ?>
			<?= $form->field($modelNote, 'comment',['template'=>'
            <div class="input-group">
                <span class="input-group-addon">Note</span>
                {input}
                <span class="input-group-btn">
                    '.Html::submitButton(Yii::t('app', 'Post'), ['class' => 'btn btn-warning']).'
                </span>
            </div>
            {error}
            '])->textInput(['maxlength' => true]) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
	<?php
    echo ListView::widget( [
        'dataProvider' => $dataProvider,
        'id' => 'my-listview-id',
        'itemOptions' => ['class' => 'item'],
        'itemView' => '_note_item',
		'emptyText'=>'',
        'layout'=>"
            <div class=\"box-body chat\">{items}</div>
            <div class=\"box-footer\"><center>{pager}</center></div>
        ",
    ] );
    ?>
</div>
