<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserRequestsDiscussion */
?>
<img src="<?= $model->createdBy->imageSrc?>" alt="user image" class="online">
<p class="message">
    <a href="#" class="name">
        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> <?= Yii::$app->helperFunction->convertTime($model->updated_at)?></small>
        <?= $model->createdBy->username?>
    </a>
    <?= nl2br($model->comment)?>
</p>