<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\widgets\CustomGridView;
use app\models\City;
use app\models\Package;
use app\models\Contracts;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Members');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?= CustomGridView::widget([
		'create'=>true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:10px;'],],
            ['attribute'=>'registration_no'],
            ['format'=>'html','attribute'=>'id','label' => 'Package','value'=>function($model){

            /*  $html = $current_contract->package->title;
              $html.= $current_contract->current_expiry;
              $html.= $model->id;*/
                $html= '';
            if(isset($model->package->title)) {
                $html = $model->package->title;
            }
                if(isset($model->expiryStatus)) {
                    $html .= $model->expiryStatus;
                }
              // $html .= $model->expiryStatus;
              //$html.= $model->renewedStatus;
              return $html;
            },'filter'=>ArrayHelper::map(Package::find()->where(['status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title')],
            ['attribute'=>'username','value'=>function($model){return ($model->parent>0 && $model->parentUser!=null ? $model->parentUser->username.' -> ' : '').$model->username;}],
            'email:email',
            'mobile',
            ['attribute'=>'city_id','value'=>function($model){return $model->city->title;},'filter'=>ArrayHelper::map(City::find()->where(['status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title')],
            ['format'=>'html','attribute'=>'is_licensed','value'=>function($model){return $model->txtLicensed;},'filter'=>Yii::$app->params['isLicensedArr']],
            ['format'=>'html','attribute'=>'status','value'=>function($model){return $model->txtStatus;},'filter'=>Yii::$app->params['statusArr']],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>Yii::t('app','Actions'),
                'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],
                'contentOptions'=>['class'=>'noprint text-right'],
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fa fa-folder"></i>', ($model->parent==0 ? $url : ['view','id'=>$model->parent]), [
                            'title' => Yii::t('app', 'View'),
                            'data-toggle'=>'tooltip',
                            'class'=>'btn btn-info btn-xs',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-edit"></i>', ($model->parent==0 ? $url : ['update','id'=>$model->parent]), [
                            'title' => Yii::t('app', 'Update'),
                            'data-toggle'=>'tooltip',
                            'class'=>'btn btn-success btn-xs',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        /*return Html::a('<i class="fa fa-remove"></i>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'data-toggle'=>'tooltip',
                            'data-method'=>'post',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this?'),
                            'class'=>'btn btn-danger btn-xs',
                        ]);*/
                    },
                ],
            ],
        ],
    ]); ?>
</div>
