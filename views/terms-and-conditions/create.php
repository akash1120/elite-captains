<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TermsAndConditions */

$this->title = 'Create Terms And Conditions';
$this->params['breadcrumbs'][] = ['label' => 'Terms And Conditions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="terms-and-conditions-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
