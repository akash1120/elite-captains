<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentTerms */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="payment-terms-form box">
	<?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
	    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'no_of_days')->textInput() ?>
    <?= $form->field($model, 'short_code')->textInput() ?>
    <?= $form->field($model, 'status')->dropDownList(Yii::$app->params['statusArr']) ?>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app','Cancel'),['index'],['class'=>'btn btn-default'])?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
