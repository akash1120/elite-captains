<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentTerms */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Payment Terms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-terms-view box">
    <div class="box-header with-border">
        <h3></h3>
        <div class="box-tools pull-right">
            <?= Html::a('<i class="fa fa-edit"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-box-tool', 'data-toggle'=>'tooltip', 'title'=>'Update']) ?>
            <?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn btn-box-tool', 'data-toggle'=>'tooltip', 'title'=>'Delete', 'data'=>['method' => 'post', 'confirm' => 'Are you sure you want to delete this?',]]) ?>
            <?= Html::a('<i class="fa fa-times"></i>', ['index'], ['class' => 'btn btn-box-tool', 'data-toggle'=>'tooltip', 'title'=>'Back']) ?>
        </div>
    </div>
    <div class="box-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
	            'name',
            'no_of_days',
            'txtStatus:html',
            ],
        ]) ?>
    </div>
</div>
