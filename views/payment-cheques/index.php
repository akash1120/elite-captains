<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentChequesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Cheques';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-cheques-index">

    <!--<h1><? /*= Html::encode($this->title) */ ?></h1>-->

    <?php


    // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'payment_id',
            [
                'attribute' => 'customer_id',
                'label' => 'Customer',
                'value' => function ($model) {
                    return ($model->user <> null) ? ucfirst($model->user->username) : "-";
                }
            ],
            'cheque_number',
           // 'date',
            [
                'attribute' => 'date',
                'label' => 'Date',
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->date));
                }
            ],
            'amount',
           // 'status',
            [
                'attribute' => 'status',
                'label' => 'Status',
                'value' => function ($model) {
                    return ucfirst($model->status);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['class' => 'noprint', 'style' => 'width:100px;'],
                'contentOptions' => ['class' => 'noprint'],
                'template' => '{view} {update} {delete}',
                'visibleButtons' => [
                    'view' => function ($model, $key, $index) {
                        return true;
                    },

                    'update' => function ($model, $key, $index) {
                        if (Yii::$app->user->identity->user_type == 1) {
                            return true;
                        } else {
                            return false;
                        }
                        return false;
                    },
                    'delete' => function ($model, $key, $index) {

                        return false;
                    },
                ],
                'buttons' => [

                    'view' => function ($url, $model) {
                        $url = \yii\helpers\Url::to(['/payments/view-cheques/', 'id' => $model->payment_id]);
                        return Html::a('<i class="fa fa-eye"></i>', $url, [
                            'title' => Yii::t('app', 'View Cheques'),
                            'data-toggle' => 'tooltip',
                            'class' => 'btn btn-info btn-xs',
                        ]);
                    },


                    'update' => function ($url, $model) {
                        $url = \yii\helpers\Url::to(['/payments/cheque/', 'id' => $model->payment_id]);
                        return Html::a('<i class="fa fa-edit"></i>', $url, [
                            'title' => Yii::t('app', 'Update Cheques'),
                            'data-toggle' => 'tooltip',
                            'class' => 'btn btn-info btn-xs',
                        ]);
                    },


                ],
            ],

        ],
    ]); ?>


</div>
