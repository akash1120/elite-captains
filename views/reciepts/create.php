<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Reciepts */

$this->title = 'Create Reciepts';
$this->params['breadcrumbs'][] = ['label' => 'Reciepts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reciepts-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
