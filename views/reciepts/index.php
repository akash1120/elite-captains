<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RecieptsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reciepts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reciepts-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Reciepts', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'payment_ref',
            'invoice_id',
            'trax_ref',
            'status',
            //'payment_method_id',
            //'user_id',
            //'received_by',
            //'amount',
            //'note:ntext',
            //'date',
            //'response_code',
            //'pt_invoice_id',
            //'transaction_id',
            //'frequency',
            //'currency',
            //'number_of_cheques',
            //'payment_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
