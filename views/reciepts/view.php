<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Reciepts */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Reciepts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="reciepts-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'payment_ref',
            'invoice_id',
            'trax_ref',
            'status',
            'payment_method_id',
            'user_id',
            'received_by',
            'amount',
            'note:ntext',
            'date',
            'response_code',
            'pt_invoice_id',
            'transaction_id',
            'frequency',
            'currency',
            'number_of_cheques',
            'payment_id',
        ],
    ]) ?>

</div>
