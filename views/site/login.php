<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
    <p class="login-box-msg">Please fill out the following fields to login:</p>

    <?php $form = ActiveForm::begin(['id' => 'login-form',]); ?>
      <?= $form->field($model, 'username',['template'=>'
	  <div class="form-group has-feedback">
	  	{input}
		<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		{error}{hint}
	  </div>
	  '])->textInput(['autofocus' => true, 'placeholder' => 'Username']) ?>
      <?= $form->field($model, 'password',['template'=>'
	  <div class="form-group has-feedback">
	  	{input}
		<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		{error}{hint}
	  </div>
	  '])->passwordInput(['placeholder' => 'Password']) ?>
      <div class="row">
        <div class="col-xs-8">
          <?= $form->field($model, 'rememberMe')->checkbox()?>
        </div>
        <div class="col-xs-4">
          <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
        </div>
      </div>
    </form>
    <?= '';//Html::a('I forgot my password',['site/forget-password'])?>
    <?php ActiveForm::end(); ?>
</div>
