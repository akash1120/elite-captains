<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */

if (Yii::$app->user->identity->user_type != 0) {
    $this->title = 'Dashboard';
} else {
    $this->title = 'Dashboard - Upcoming Booking';
}
?>
<?php
if (Yii::$app->user->identity->user_type != 0){
$stats = Yii::$app->controller->adminStats;
?>
<div class="row">
    <a href="<?= Url::to(['booking/all']) ?>">
        <div class="col-xs-12 col-sm-3">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-star-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">All Bookings</span>
                    <span class="info-box-number"><?= $stats['allBookings'] ?></span>
                </div>
            </div>
        </div>
    </a>
    <a href="<?= Url::to(['booking/index']) ?>">
        <div class="col-xs-12 col-sm-3">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-calendar"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Future Bookings</span>
                    <span class="info-box-number"><?= $stats['futureBookings'] ?></span>
                </div>
            </div>
        </div>
    </a>
    <a href="<?= Url::to(['booking/history']) ?>">
        <div class="col-xs-12 col-sm-3">
            <div class="info-box">
                <span class="info-box-icon bg-navy"><i class="fa fa-bookmark-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Past Bookings</span>
                    <span class="info-box-number"><?= $stats['pastBookings'] ?></span>
                </div>
            </div>
        </div>
    </a>
    <a href="<?= Url::to(['package/index']) ?>">
        <div class="col-xs-12 col-sm-3">
            <div class="info-box">
                <span class="info-box-icon bg-maroon"><i class="fa fa-pie-chart"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Packages</span>
                    <span class="info-box-number"><?= $stats['allPackages'] ?></span>
                </div>
            </div>
        </div>
    </a>
    <a href="<?= Url::to(['city/index']) ?>">
        <div class="col-xs-12 col-sm-3">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-building"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Cities</span>
                    <span class="info-box-number"><?= $stats['allCities'] ?></span>
                </div>
            </div>
        </div>
    </a>
    <a href="<?= Url::to(['marina/index']) ?>">
        <div class="col-xs-12 col-sm-3">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-anchor"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Marina</span>
                    <span class="info-box-number"><?= $stats['allMarinas'] ?></span>
                </div>
            </div>
        </div>
    </a>
    <a href="<?= Url::to(['boat/index']) ?>">
        <div class="col-xs-12 col-sm-3">
            <div class="info-box">
                <span class="info-box-icon bg-purple"><i class="fa fa-ship"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Boats</span>
                    <span class="info-box-number"><?= $stats['allBoats'] ?></span>
                </div>
            </div>
        </div>
    </a>
    <a href="<?= Url::to(['member/index']) ?>">
        <div class="col-xs-12 col-sm-3">
            <div class="info-box">
                <span class="info-box-icon bg-blue"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Members</span>
                    <span class="info-box-number"><?= $stats['allUsers'] ?></span>
                </div>
            </div>
        </div>
    </a>
    <a href="<?= Url::to(['contracts/comming-renewals']) ?>">
        <div class="col-xs-12 col-sm-3">
            <div class="info-box">
                <span class="info-box-icon bg-blue"><i class="fa fa-refresh"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Renewals( 30 days)</span>
                    <span class="info-box-number"><?= $stats['allCommingRenewals'] ?></span>
                </div>
            </div>
        </div>
    </a>
    <a href="<?= Url::to(['payment-cheques/comming-cheques']) ?>">
        <div class="col-xs-12 col-sm-3">
            <div class="info-box">
                <span class="info-box-icon bg-blue"><i class="fa fa-bank"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Cheques( 30 days)</span>
                    <span class="info-box-number"><?= $stats['allUpcomingCheques'] ?></span>
                </div>
            </div>
        </div>
    </a>
    <a href="<?= Url::to(['invoices/comming-invoices']) ?>">
        <div class="col-xs-12 col-sm-3">
            <div class="info-box">
                <span class="info-box-icon bg-blue"><i class="fa fa-money"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Invoices( 30 days)</span>
                    <span class="info-box-number"><?= $stats['allUpcomingInvoices'] ?></span>
                </div>
            </div>
        </div>
    </a>

</div>
</section>
<section class="content-header" style="padding-top:0px;">
    <h1>Upcoming Booking</h1>
</section>
<section class="content">
    <?php } ?>
    <div class="box box-success">
        <div class="box-body">
            <?php Pjax::begin(['id' => 'pjax-booking']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:10px;'],],
                    ['attribute' => 'city_id', 'value' => function ($model) {
                        return $model->city->title;
                    }],
                    ['attribute' => 'marina_id', 'value' => function ($model) {
                        return $model->marina->title;
                    }],
                    ['attribute' => 'boat_id', 'value' => function ($model) {
                        return $model->boat->title;
                    }],
                    'booking_date:date',
                    ['attribute' => 'time_slot_id', 'value' => function ($model) {
                        return $model->timeSlot->title;
                    }],
                    'txtAddons:raw',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['class' => 'noprint', 'style' => 'width:30px;'],
                        'contentOptions' => ['class' => 'noprint text-right'],
                        'template' => '{delete}',
                        'buttons' => [
                            'delete' => function ($url, $model) {
                                if (Yii::$app->user->identity->id == $model->user_id) {
                                    return Html::a('<i class="fa fa-remove"></i>', ['booking/delete', 'id' => $model->id], [
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-toggle' => 'tooltip',
                                        'data-method' => 'post',
                                        'data-confirm' => Yii::t('app', 'Are you sure you want to delete this?'),
                                        'class' => 'btn btn-danger btn-xs',
                                    ]);
                                }
                            },
                        ],
                    ]
                ],
                'layout' => "{items}",
                'emptyText' => 'No future booking found!',
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>