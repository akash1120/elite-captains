<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\City;
use app\models\Marina;
use yii\web\JqueryAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Booking */
/* @var $form yii\widgets\ActiveForm */

$this->registerCssFile('@web/'.'plugins/datepicker/datepicker3.css', ['depends' => [JqueryAsset::className()]]);
$this->registerJsFile('@web/'.'plugins/datepicker/bootstrap-datepicker.js', ['depends' => [JqueryAsset::className()]]);

$this->title = Yii::t('app', 'Book Boat');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bookings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$defMarina=[];
$jScript='var cities = []';
$cities=City::find()->where(['status'=>1,'trashed'=>0])->asArray()->all();
if($cities!=null){
	foreach($cities as $city){
		$jScript.='
			var city_'.$city['id'].' = [
		';
		$marinas=Marina::find()->where(['city_id'=>$city['id'],'status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all();
		if($marinas!=null){
			foreach($marinas as $marina){
				$jScript.='{display: "'.$marina['title'].'", value: "'.$marina['id'].'"},';
			}
		}
		$jScript.=']';
	}
}
if($modelForm->city_id!=null){
	$defMarina=ArrayHelper::map(Marina::find()->where(['city_id'=>$modelForm->city_id,'status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title');
}
$this->registerJs($jScript.'
	$("#bookingform-city_id").change(function() {
		cval=$(this).val();
		$("#bookingform-marina_id").html("");
		array_list=eval("city_"+cval)
		$(array_list).each(function (i) {
			$("#bookingform-marina_id").append("<option value=\""+array_list[i].value+"\""+(array_list[i].sel!="" ? array_list[i].sel : "")+">"+array_list[i].display+"</option>");
		});
	});
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
		startDate: "today",
		
	}).on("changeDate", function(e) {
		$(this).datepicker("hide");
	});
');
?>
<div class="bookingform-create">
	<?php $form = ActiveForm::begin(); ?>
	<div class="bookingform-form box">
        <div class="box-body">
			<div class="row">
            	<div class="col-xs-12 col-sm-3"><?= $form->field($modelForm, 'city_id')->dropDownList(ArrayHelper::map($cities,'id','title'),['prompt'=>Yii::t('app','Select')]) ?></div>
            	<div class="col-xs-12 col-sm-3"><?= $form->field($modelForm, 'marina_id')->dropDownList($defMarina,['prompt'=>Yii::t('app','Select')]) ?></div>
                <div class="col-xs-12 col-sm-3"><?= $form->field($modelForm, 'date',['template'=>'
				  {label}
				  <div class="form-group has-feedback">
					{input}
					<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
					{error}{hint}
				  </div>
				  '])->textInput(['class'=>'form-control dtpicker', 'readonly'=>'readonly']) ?></div>
                <div class="col-xs-12 col-sm-3"><?= Html::submitButton('Search', ['class' => 'btn btn-success btn-block', 'style'=>'margin-top:25px;']) ?></div>
            </div>
        </div>
    </div>
    <div id="boats-area" style="margin-top:20px;"></div>
    <?php ActiveForm::end(); ?>
</div>
<script type="text/javascript">
function loadBoats()
{
	if($("#booking-city_id").val()==""){
		swal('Information missing','Select City','info');
		return false;
	}
	if($("#booking-marina_id").val()==""){
		swal('Information missing','Select Marina','info');
		return false;
	}
	if($("#booking-booking_date").val()==""){
		swal('Information missing','Select Date','info');
		return false;
	}
	city_id=$("#booking-city_id").val();
	marina_id=$("#booking-marina_id").val();
	booking_date=$("#booking-booking_date").val();
	if(city_id!='' && marina_id!='' && booking_date!=''){
		$.ajax({
			url: "<?= Url::to(['booking/load-boats','city_id'=>'']);?>"+city_id+"&marina_id="+marina_id+"&booking_date="+booking_date,
			dataType: "html",
			success: function(html) {
				$("#boats-area").html(html);
			},
			error: bbAlert
		});
	}
}
</script>
