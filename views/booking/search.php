<?php
use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BoatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
echo ListView::widget( [
	'dataProvider' => $dataProvider,
	'viewParams' => ['city_id'=>$searchModel->city_id,'marina_id'=>$searchModel->marina_id,'booking_date'=>$booking_date],
	'itemOptions' => ['class' => 'col-xs-12 col-sm-4'],
	'itemView' => '/boat/_item',
	'layout'=>"<div class=\"row\">{items}</div>",
] );
?>