<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\widgets\CustomGridView;
use app\models\City;
use app\models\Marina;
use app\models\Boat;
use app\models\TimeSlot;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(Yii::$app->controller->action->id=='all'){
	if($searchModel->listType=='active'){
		$this->title = Yii::t('app', 'Future Bookings');
	}
	if($searchModel->listType=='old'){
		$this->title = Yii::t('app', 'Old Bookings');
	}
	if($searchModel->listType=='all'){
		$this->title = Yii::t('app', 'All Bookings');
	}
	if($searchModel->listType=='deleted'){
		$this->title = Yii::t('app', 'Deleted Bookings');
	}
}elseif(Yii::$app->controller->action->id=='history'){
	$this->title = Yii::t('app', 'Booking History');
}else{
	$this->title = Yii::t('app', 'Active Bookings');
}

$this->params['breadcrumbs'][] = $this->title;

$defMarina=Marina::find()->where(['status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all();
if($searchModel->city_id!=null){
	$defMarina=Marina::find()->where(['city_id'=>$searchModel->city_id,'status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all();
}
$defBoats=Boat::find()->where(['status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all();
if($searchModel->marina_id!=null){
	$defMarina=Boat::find()->where(['marina_id'=>$searchModel->marina_id,'status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all();
}

$columns[]=['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:10px;'],];
if((Yii::$app->user->identity->user_type==1 || Yii::$app->user->identity->user_type==2)){
$columns[]=['class' => 'yii\grid\CheckboxColumn','checkboxOptions'=>function ($model, $key, $index, $column){return ['value' => $model->id, 'class'=>'cb'];},'contentOptions'=>['style'=>'width: 10px;']];
}
$columns[]='reference_no';
if((Yii::$app->user->identity->user_type==1 || Yii::$app->user->identity->user_type==2) && Yii::$app->controller->action->id=='all'){
$columns[] = 'userName';
}
if(Yii::$app->user->identity->user_type==0 && Yii::$app->user->identity->package_id==Yii::$app->params['goldenSharePackageId']){
$columns[] = 'userName';
}
$columns[]=['attribute'=>'city_id','value'=>function($model){return $model->city->title;},'filter'=>ArrayHelper::map(City::find()->where(['status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title')];
$columns[]=['attribute'=>'marina_id','value'=>function($model){return $model->marina->title;},'filter'=>ArrayHelper::map($defMarina,'id','title')];
$columns[]=['attribute'=>'boat_id','value'=>function($model){return $model->boat->title;},'filter'=>ArrayHelper::map($defBoats,'id','title')];
$columns[]='booking_date:date';
$columns[]=['attribute'=>'time_slot_id','value'=>function($model){return $model->timeSlot->title;},'filter'=>ArrayHelper::map(TimeSlot::find()->where(['status'=>1,'trashed'=>0])->asArray()->all(),'id','title')];
$columns[]=['attribute'=>'captain','label' => 'Addons','value'=>function($model){
    $addons_text='';
    if(isset($model->captaindata->title) && isset($model->fooddata->title)){
        $addons_text .= $model->captaindata->title.', '.$model->fooddata->title;
    }else if(isset($model->captaindata->title)){
        $addons_text .= $model->captaindata->title;
    }else if(isset($model->fooddata->title)){
        $addons_text .= $model->fooddata->title;
    }

    return $addons_text;

}];
/*$columns[]='txtAddons:html';*/
if(Yii::$app->controller->action->id=='index' || Yii::$app->controller->action->id=='all'){
	$columns[]=[
		'class' => 'yii\grid\ActionColumn',
		'header'=>Yii::t('app','Actions'),
		'headerOptions'=>['class'=>'noprint','style'=>'width:30px;'],
		'contentOptions'=>['class'=>'noprint text-right'],
		'template' => '{delete}',
		'buttons' => [
			'delete' => function ($url, $model) {
				if(Yii::$app->user->identity->user_type==0 && Yii::$app->user->identity->id==$model->user_id){
    				return Html::a('<i class="fa fa-remove"></i>', $url, [
    					'title' => Yii::t('app', 'Delete'),
    					'data-toggle'=>'tooltip',
    					'data-method'=>'post',
    					'data-confirm'=>Yii::t('app','Are you sure you want to delete this?'),
    					'class'=>'btn btn-danger btn-xs',
    				]);
				}elseif(Yii::$app->user->identity->id==2){
					return Html::a('<i class="fa fa-remove"></i>', $url, [
						'title' => Yii::t('app', 'Delete'),
						'data-toggle'=>'tooltip',
						'data-method'=>'post',
						'data-confirm'=>Yii::t('app','Are you sure you want to delete this?'),
						'class'=>'btn btn-danger btn-xs',
					]);
				}
			},
		],
	];
}
?>
<?php if(Yii::$app->controller->action->id=='all'){?>
<?= Html::a('Future Bookings',['booking/all','list'=>'active'],['class'=>'btn btn-'.($searchModel->listType=='active' ? 'primary' : 'default').' btn-sm'])?> 
<?= Html::a('Old Bookings',['booking/all','list'=>'old'],['class'=>'btn btn-'.($searchModel->listType=='old' ? 'primary' : 'default').' btn-sm'])?> 
<?= Html::a('All Bookings',['booking/all','list'=>'all'],['class'=>'btn btn-'.($searchModel->listType=='all' ? 'primary' : 'default').' btn-sm'])?> 
<?= Html::a('Deleted Bookings',['booking/all','list'=>'deleted'],['class'=>'btn btn-'.($searchModel->listType=='deleted' ? 'primary' : 'default').' btn-sm'])?>
<?php }?>
<div class="booking-index">
    <?php
    if(Yii::$app->controller->action->id=='all'){
		echo CustomGridView::widget([
			'create'=>true,
			'bulkdelete'=>true,
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => $columns,
		]);
	}else{
		if(Yii::$app->controller->action->id=='index' && (Yii::$app->user->identity->user_type==1 || Yii::$app->user->identity->user_type==2)){
			echo CustomGridView::widget([
				'create'=>true,
				'bulkdelete'=>true,
				'dataProvider' => $dataProvider,
				'columns' => $columns,
			]);
		}else{
			echo CustomGridView::widget([
				'create'=>true,
				'dataProvider' => $dataProvider,
				'columns' => $columns,
			]);
		}
	}
	?>
</div>
