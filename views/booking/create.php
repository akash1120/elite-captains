<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\City;
use app\models\Marina;
use yii\web\JqueryAsset;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Booking */
/* @var $form yii\widgets\ActiveForm */

$this->registerCssFile('@web/'.'plugins/datepicker/datepicker3.css', ['depends' => [JqueryAsset::className()]]);
$this->registerJsFile('@web/'.'plugins/datepicker/bootstrap-datepicker.js', ['depends' => [JqueryAsset::className()]]);

$this->title = Yii::t('app', 'Book Boat');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bookings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$defMarina=[];
$jScript='var cities = []';

$current_user = Yii::$app->user->identity->id ;
$current_contract = \app\models\Contracts::find()->where([
    'user_id' => $current_user,
    'status' => 'active'
])->orderBy(['id' => SORT_DESC])->one();


$city_ids = array();
if(!empty($current_contract)) {
    if ($current_contract->contractCities <> null && !empty($current_contract->contractCities)) {
        foreach ($current_contract->contractCities as $city) {
            $city_ids[] = $city->city_id;
        }
    }
}
if(!empty($city_ids)) {
    $cities = City::find()->where(['status' => 1, 'trashed' => 0, 'id' => $city_ids])->asArray()->all();
}else{
    $cities = City::find()->where(['status' => 1, 'trashed' => 0, 'id' => [1]])->asArray()->all();
}

//$cities=City::find()->where(['status'=>1,'trashed'=>0])->asArray()->all();
if($cities!=null){
    foreach($cities as $city){
        $jScript.='
			var city_'.$city['id'].' = [
		';
        $marinas=Marina::find()->where(['city_id'=>$city['id'],'status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all();
        if($marinas!=null){
            foreach($marinas as $marina){
                $jScript.='{display: "'.$marina['title'].'", value: "'.$marina['id'].'"},';
            }
        }
        $jScript.=']';
    }
}
if($modelForm->city_id!=null){
    $defMarina=ArrayHelper::map(Marina::find()->where(['city_id'=>$modelForm->city_id,'status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title');
}
$this->registerJs($jScript.'
	$("#bookingform-city_id").change(function() {
		cval=$(this).val();
		$("#bookingform-marina_id").html("");
		array_list=eval("city_"+cval)
		$(array_list).each(function (i) {
			$("#bookingform-marina_id").append("<option value=\""+array_list[i].value+"\""+(array_list[i].sel!="" ? array_list[i].sel : "")+">"+array_list[i].display+"</option>");
		});
	});
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
		startDate: "'.(Yii::$app->user->identity->user_type==0 ? date("Y-m-d",mktime(0,0,0,date("m"),(date("d")+1),date("Y"))) : date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y")))).'",
		
	}).on("changeDate", function(e) {
		$(this).datepicker("hide");
	});
	
	$(".box-solid").find(".timings").css("height",Math.max.apply(Math, $(".box-solid .timings").map(function() { return $(this).height(); }))+"px");
');
?>
<div class="bookingform-create">
    <div class="bookingform-form box">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>
            <div class="row">

                <div class="col-xs-12 col-sm-3"><?= $form->field($modelForm, 'city_id')->dropDownList(ArrayHelper::map($cities,'id','title'),['prompt'=>Yii::t('app','Select')]) ?></div>
                <div class="col-xs-12 col-sm-3"><?= $form->field($modelForm, 'marina_id')->dropDownList($defMarina,['prompt'=>Yii::t('app','Select')]) ?></div>
                <div class="col-xs-12 col-sm-3"><?= $form->field($modelForm, 'date',['template'=>'
				  {label}
				  <div class="form-group has-feedback">
					{input}
					<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
					{error}{hint}
				  </div>
				  '])->textInput(['class'=>'form-control dtpicker', 'readonly'=>'readonly']) ?></div>
                <div class="col-xs-12 col-sm-3"><?= Html::submitButton('Search', ['class' => 'btn btn-success btn-block', 'style'=>'margin-top:25px;']) ?></div>
            </div>
            <?php ActiveForm::end(); ?>

            <?php

            /*  echo "<pre>";
              print_r($saved_addons);
              die;*/
            $captain_disable = '';
            $food_disable = '';
            if(!empty($saved_addons)){
                $captain_availability =  $saved_addons[0]['qunatity'] - $saved_addons[0]['addon_used'];
                $food_availability =  $saved_addons[1]['qunatity'] - $saved_addons[1]['addon_used'];
                if($captain_availability > 0){
                    $captain_disable = '';
                }else{
                    $captain_disable =' disabled="disabled"';
                }
                if($food_availability > 0){
                    $food_disable = '';
                }else{
                    $food_disable =' disabled="disabled"';
                }
                ?>
                <div class="col-xs-12 col-sm-6">
                    <h3>Addons</h3>
                    <table style="width:100%">
                        <tr>
                            <th></th>
                            <th>Total</th>
                            <th>Availability</th>

                        </tr>
                        <!--  <tr>
                        <th>Captains</th>
                        <td><?/*= $saved_addons[0]['qunatity']; */?> </td>
                        <td><?/*= $captain_availability */?> </td>

                    </tr>-->
                        <tr>

                            <th>Food(Lunch/Dinner)</th>
                            <td><?= $saved_addons[1]['qunatity']; ?> </td>
                            <td><?= $food_availability ?> </td>

                        </tr>

                    </table>

                </div>
            <?php } ?>
        </div>
    </div>
    <div id="boats-area" style="margin-top:20px;">
        <?php
        if($searchModel!=null){
            echo ListView::widget( [
                'dataProvider' => $dataProvider,
                'viewParams' => ['city_id'=>$searchModel->city_id,'marina_id'=>$searchModel->marina_id,'booking_date'=>$modelForm->date,'captain_disable'=>$captain_disable, 'food_disable'=> $food_disable, 'member'=> $member,'contract'=> $contract],
                'itemOptions' => ['class' => 'col-xs-12 col-sm-4'],
                'itemView' => '/boat/_item',
                'layout'=>"<div class=\"row\">{items}</div>",
                'emptyText' => '<div class="alert alert-danger">No results found!</div>'
            ] );
        }
        ?>
    </div>
</div>