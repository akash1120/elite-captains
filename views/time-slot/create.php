<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TimeSlot */

$this->title = Yii::t('app', 'Create Time Slot');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Time Slots'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-slot-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
