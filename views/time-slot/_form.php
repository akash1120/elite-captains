<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Marina;

/* @var $this yii\web\View */
/* @var $model app\models\TimeSlot */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="time-slot-form box">
	<?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
        <div class="row">
        	<div class="col-xs-12 col-sm-4"><?= $form->field($model, 'marina_id')->dropDownList(ArrayHelper::map(Marina::find()->select(['id','title'])->where(['status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title')) ?></div>
        	<div class="col-xs-12 col-sm-4"><?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?></div>
        	<div class="col-xs-12 col-sm-4"><?= $form->field($model, 'status')->dropDownList(Yii::$app->params['statusArr']) ?></div>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app','Cancel'),['index'],['class'=>'btn btn-default'])?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
