<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Package;
use app\models\PaymentMethods;
use app\models\PaymentTerms;
use app\assetfiles\DatePickerAsset;

DatePickerAsset::register($this);
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs('
 $("#card").hide();
  $("#cash").hide();
	$(document).delegate("#user-package_id", "change", function() {
		if($(this).val()==' . Yii::$app->params['sharingPackageId'] . '){
			$("#subUser").show();
		}else{
			$("#subUser").hide();
		}
	});
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
	}).on("changeDate", function(e) {
		$(this).datepicker("hide");
	});
	
	$(document).delegate("#paymentmethods-short_code", "change", function() {
	if($(this).val()=="cash"){
	        $("#card").hide();
			$("#cash").show(500);
			
		}else{
		  $("#cash").hide();
          $("#card").show(500);
		}
	});
');
?>
<style>
    .padding20 {
        padding: 20px;
    }

    .hide {
        display: none;
    }
</style>
<div class="user-form">
    <div class="box box-success">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="box-body">
            <div class="panel panel-default ">
                <div class="panel-heading"><strong>Required Information</strong></div>
                <div class="row padding20">
                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'id')->dropDownList(ArrayHelper::map(user::find()->where(['user_type' => 1, 'status' => 1])->orderBy('username')->asArray()->all(), 'id', 'username'))->label('Users') ?>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'id')->dropDownList(ArrayHelper::map(Package::find()->where(['status' => 1, 'trashed' => 0])->orderBy('title')->asArray()->all(), 'id', 'title'), ['prompt' => Yii::t('app', 'Select')])->label('Packages') ?>
                    </div>

                    <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'id')->textInput(['class' => 'form-control dtpicker', 'autocomplete' => 'off', 'maxlength' => true])->label('Start date') ?></div>
                    <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'id')->textInput(['class' => 'form-control dtpicker', 'autocomplete' => 'off', 'maxlength' => true])->label('End date') ?></div>
                </div>
            </div>

            <div class="panel panel-default ">
                <div class="panel-heading"><strong>Addons</strong></div>
                <div class="row padding20">
                    <div class="col-xs-12 col-sm-12">
                        <?= $form->field($model, 'addons')->widget(MultipleInput::className(), [
                            'max' => 99,
                            'columns' => [
                                [
                                    'name' => 'id',
                                    'type' => 'dropDownList',
                                    'title' => 'Addons',
                                    'defaultValue' => 1,
                                    'items' => [
                                        1 => 'User 1',
                                        2 => 'User 2'
                                    ],
                                    'headerOptions' => [
                                        'style' => 'width: 47%;',
                                    ]
                                ],
                                [
                                    'name' => 'quantity',
                                    'enableError' => true,
                                    'title' => 'Quantity',
                                    'options' => [
                                        'class' => 'input-priority',
                                        'type' => 'number',
                                    ],
                                    'headerOptions' => [
                                        'style' => 'width: 47%;',
                                    ]
                                ],
                            ]
                        ])->label(false);
                        ?>
                    </div>


                </div>
            </div>

            <div class="panel panel-default ">
                <div class="panel-heading"><strong>Payment Information</strong></div>
                <div class="row padding20">
                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'short_code')->dropDownList(ArrayHelper::map(PaymentMethods::find()->where(['status' => 1])->orderBy('name')->asArray()->all(), 'short_code', 'name'))->label('Payment Method') ?>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'id')->dropDownList(ArrayHelper::map(PaymentTerms::find()->where(['status' => 1])->orderBy('name')->asArray()->all(), 'id', 'name'))->label('Payment Terms') ?>
                    </div>
                    <div id="cash" class="">
                        <div class="col-xs-12 col-sm-6">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'type' => 'number'])->label('Check Number') ?>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <?= $form->field($model, 'id')->textInput(['class' => 'form-control dtpicker', 'autocomplete' => 'off', 'maxlength' => true])->label('Check date') ?>
                        </div>
                    </div>
                    <div id="card">
                        <div class="col-xs-12 col-sm-5">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'type' => 'number'])->label('Card Number') ?>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <?= $form->field($model, 'id')->textInput(['class' => 'form-control dtpicker', 'autocomplete' => 'off', 'maxlength' => true])->label('Expiry date') ?>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'type' => 'number'])->label('CVV') ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'type' => 'number'])->label('Free Days') ?>
                    </div>
                </div>
            </div>


            <div class="box">
                <div class="box-body">
                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>