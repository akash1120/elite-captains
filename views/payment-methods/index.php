<?php

use yii\helpers\Html;
use app\widgets\CustomGridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentMethodsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Methods';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-methods-index">

    <?= CustomGridView::widget([
		'create'=>true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:10px;'],],

            'name',
            'short_code',
            ['format'=>'html','attribute'=>'status','value'=>function($model){return $model->txtStatus;},'filter'=>Yii::$app->params['statusArr']],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>Yii::t('app','Actions'),
                'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],
                'contentOptions'=>['class'=>'noprint text-right'],
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fa fa-folder"></i>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'data-toggle'=>'tooltip',
                            'class'=>'btn btn-info btn-xs',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-edit"></i>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'data-toggle'=>'tooltip',
                            'class'=>'btn btn-success btn-xs',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-remove"></i>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'data-toggle'=>'tooltip',
                            'data-method'=>'post',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this?'),
                            'class'=>'btn btn-danger btn-xs',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
