<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Addon;
use app\models\Service;
use app\models\PackageToAddon;

/* @var $this yii\web\View */
/* @var $model app\models\Package */
/* @var $form yii\widgets\ActiveForm */

$addons=Addon::find()->where(['status'=>1,'trashed'=>0])->orderBy(['title'=>SORT_ASC])->asArray()->all();
?>
<div class="package-form box">
	<?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
	    <div class="row">
        	<div class="col-xs-12 col-sm-4"><?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?></div>
            <!--<div class="col-xs-12 col-sm-4"><?/*= $form->field($model, 'duration')->dropDownList(Yii::$app->params['packageDurationArr']) */?></div>-->
            <?= $form->field($model, 'duration')->hiddenInput(['value'=> 365])->label(false) ?>
        	<div class="col-xs-12 col-sm-4"><?= $form->field($model, 'price')->textInput()->label('Annual Price'); ?></div>
            <?= $form->field($model, 'half_year_duration')->hiddenInput(['value'=> 182])->label(false) ?>
            <div class="col-xs-12 col-sm-4"><?= $form->field($model, 'half_price')->textInput()->label('Half Yearly Price') ?></div>

        </div>
	    <div class="row">
        	<div class="col-xs-12 col-sm-4"><?= $form->field($model, 'authorized_captains')->textInput() ?></div>
        	<div class="col-xs-12 col-sm-4"><?= $form->field($model, 'no_of_booking')->textInput() ?></div>
            <div class="col-xs-12 col-sm-4"><?= $form->field($model, 'status')->dropDownList(Yii::$app->params['statusArr']) ?></div>
        </div>
        <?php if($addons!=null){?>
        <h4><strong><?= Yii::t('app','Addons')?></strong></h4>
        <table class="table table-striped table-bordered">
        	<thead>
            	<tr>
                	<?php foreach($addons as $addon){?>
                    <th>Free <?= $addon['title']?> available</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<?php
					foreach($addons as $addon){
					$value=0;
					$result=PackageToAddon::find()->where(['package_id'=>$model->id,'addon_id'=>$addon['id']])->asArray()->one();
					if($result!=null){
						$value=$result['free_qty'];
					}
					?>
                    <td><?= Html::textInput('Package[addon_idz]['.$addon['id'].']', $value); ?></td>
                    <?php }?>
                </tr>
            </tbody>
        </table>
		<?php }?>
        
		<?= $form->field($model, 'service_idz',['template'=>'<h4>{label}</h4><div class="row">{input}</div>{error}{hint}'])->checkboxList(ArrayHelper::map(Service::find()->orderBy(['title'=>SORT_ASC])->asArray()->all(),'id','title'), [
			'item' => function($index, $label, $name, $checked, $value) {
				$checked = $checked ? 'checked' : '';
				return "<div class=\"col-xs-6 col-sm-4\" style=\"padding-bottom:10px;\"><label style=\"font-weight:normal\"><input type=\"checkbox\" {$checked} name=\"{$name}\" value=\"{$value}\">&nbsp;&nbsp;{$label}</label></div>";
        	}
		]) ?>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app','Cancel'),['index'],['class'=>'btn btn-default'])?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
