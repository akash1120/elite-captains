<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\City;

/* @var $this yii\web\View */
/* @var $model app\models\Marina */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="marina-form box">
	<?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
        <?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map(City::find()->where(['status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title')) ?>
		<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'status')->dropDownList(Yii::$app->params['statusArr']) ?>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app','Cancel'),['index'],['class'=>'btn btn-default'])?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
