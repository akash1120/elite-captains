<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Marina */

$this->title = Yii::t('app', 'Update Marina: {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Marinas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="marina-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
