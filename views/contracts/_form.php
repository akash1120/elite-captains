<?php
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Package;
use app\models\Addon;
use app\models\ContarctAddons;
use app\models\PaymentMethods;
use app\models\PaymentTerms;
use app\models\TermsAndConditions;
use yii\helpers\Url;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('
$(document).delegate("#contracts-package_id", "change", function() {
		if($(this).val()=='.Yii::$app->params['sharingPackageId'].'){
			$("#subUser").show();
		}else{
			$("#subUser").hide();
		}
	});
	$(document).delegate("#contracts-payment_method_id", "change", function() {
	if($(this).val()==2){
	        $("#card").removeClass("hide");
	        $("#contracts-frequency").attr("required",true);
	        $("#contracts-contracts-recurring_amount").attr("required",true);
			
		}else{
		$("#contracts-frequency").attr("required",false);
	        $("#contracts-contracts-recurring_amount").attr("required",false);
		  $("#card").addClass("hide");
		   
		}
	});
');

$terms_and_conditions = TermsAndConditions::find()->where(['id' => 1])->asArray()->one();
$terms = ($model['terms'] ? $model['terms'] : $terms_and_conditions['terms']);
$client_notes = ($model['client_notes'] ? $model['client_notes'] : $terms_and_conditions['client_notes']);
$admin_notes = ($model['admin_notes'] ? $model['admin_notes'] : $terms_and_conditions['admin_notes']);
$joining_fee = ($model['joining_fee'] ? $model['joining_fee'] : $terms_and_conditions['joining_fee']);
$joining_display = 'display:none';
if(isset($model->first_contract_check) && ($model->first_contract_check == 1)){
    $joining_display = 'display: block';
}
?>
<style>
    .padding20 {
        padding: 20px;
    }

    .hide {
        display: none;
    }
</style>
<div class="user-form">
    <div class="box box-success">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="box-body">
            <div class="panel panel-default ">
                <div class="panel-heading"><strong>Required Information</strong></div>
                <div class="row padding20">


                    <div class="col-sm-6" id="select_package" style="padding-left:0px: ">
                        <label>Packages</label>
                        <?= \kartik\select2\Select2::widget([
                            'model' => $model,
                            'attribute' => 'package_id',
                            'data' => \yii\helpers\ArrayHelper::map(\app\models\Package::find()->all(), 'id', function ($model) {
                                return $model->title;
                            }),
                            'options' => ['class' => 'form-control','placeholder' => 'Search Package ...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]);
                        ?>
                        <div class="help-block hidden" id="help_block_package">Package cannot be blank.</div>
                    </div>
                    <div class="col-sm-6" id="select_user" style="padding-left:0px: ">
                        <label>User</label>
                        <?= \kartik\select2\Select2::widget([
                            'model' => $model,
                            'attribute' => 'user_id',
                            'data' => \yii\helpers\ArrayHelper::map(User::find()->all(), 'id', function ($model) {
                                //return $model->first_name . " " . $model->last_name;
                                return $model->username;
                            }),
                            'options' => ['placeholder' => 'Search User ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                        <div class="help-block hidden" id="help_block_user">User cannot be blank.</div>
                    </div>

                    <!-- <div class="col-xs-12 col-sm-6">
                        <?/*= $form->field($model, 'package_id')->dropDownList(ArrayHelper::map(Package::find()->where(['status' => 1, 'trashed' => 0])->orderBy('title')->asArray()->all(), 'id', 'title'), ['prompt' => Yii::t('app', 'Select')])->label('Packages') */?>
                    </div>-->

                    <!--<div class="col-xs-12 col-sm-6">
                        <?/*= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(user::find()->where(['user_type' => 0, 'status' => 1])->orderBy('username')->asArray()->all(), 'id', 'username'),['prompt' => Yii::t('app', 'Select')])->label('User') */?>
                    </div>-->
                    <div class="clearfix" style="padding: 5px;"></div>
                    <div class="col-xs-12 col-sm-6">
                        <?php
                        $start_date = date('Y-m-d');
                        $end_date = date('Y-m-d');

                        if ($model->start_date) {
                            $start_date = date('Y-m-d', strtotime($model->start_date));
                        }
                        if ($model->end_date) {
                            $end_date = date('Y-m-d', strtotime($model->end_date));
                        }


                        echo '<label class="control-label">Valid Dates</label>';
                        echo DatePicker::widget([
                            'name' => 'Contracts[start_date]',
                            'value' => $start_date,
                            'type' => DatePicker::TYPE_RANGE,
                            'name2' => 'Contracts[end_date]',
                            'value2' => $end_date,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ],
                        ]);
                        ?>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <?php
                        if(isset($model->id) && $model->id <> null){
                            ?>
                            <?= $form->field($model, 'free_days')->textInput(['maxlength' => true, 'type' => 'number','readonly'=> true])->label('Freez Days') ?>
                        <?php  }else{
                            ?>
                            <?= $form->field($model, 'free_days')->textInput(['maxlength' => true, 'type' => 'number'])->label('Freez Days') ?>
                        <?php } ?>
                    </div>
                    <div class="col-xs-12 col-sm-6" id="duration_drop_box">
                        <?php if(!isset($model->id)){ ?>
                        <label class="control-label" for="contracts-duration">Package Duration</label>
                        <select id="contracts-duration" class="form-control" name="Contracts[duration]">
                        </select>
                        <?php }else{ ?>

                            <label class="control-label" for="contracts-duration">Package Duration</label>
                            <select id="contracts-duration" class="form-control" name="Contracts[duration]">
                                <option value="365" <?php if($model->duration == '365'){ echo 'selected' ;} ?> data_price="<?= $model->package->price ?>">One Year</option>
                                <option value="182" <?php if($model->duration == '182'){ echo 'selected' ;} ?> data_price="<?= $model->package->half_price ?>">Half Year</option>
                            </select>

                        <?php } ?>

                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'price')->textInput(['maxlength' => true, 'class' => 'validate_float_number form-control']) ?>
                    </div>

                    <div class="col-xs-12 col-sm-6" id="subUser" <?= $model->sub_user!=null && $model->package_id==Yii::$app->params['sharingPackageId'] ? ' style="display:block;"' : ' style="display:none;"'?>>
                        <?= $form->field($model, 'sub_user')->dropDownList(ArrayHelper::map(user::find()->where(['user_type' => 0, 'status' => 1])->orderBy('username')->asArray()->all(), 'id', 'username'))->label('Second User') ?>
                    </div>
                    <div class="col-xs-12 col-sm-6" id="joining_fee" style="<?= $joining_display; ?>">
                        <?= $form->field($model, 'joining_fee')->textInput(['maxlength' => true, 'class' => 'validate_float_number form-control','value' => $joining_fee]) ?>

                    </div>
                    <div class="col-sm-6" id="select_city" style="padding-left:0px: ">
                        <label>City</label>

                        <?php  ?>

                        <?= \kartik\select2\Select2::widget([
                            'model' => $model,
                            'attribute' => 'cities',
                            'data' => \yii\helpers\ArrayHelper::map(\app\models\City::find()->all(), 'id', function ($model) {
                                return $model->title;
                            }),
                            'options' => ['class' => 'form-control','placeholder' => 'Select Cities'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'multiple'=> true,
                            ],
                        ]);
                        ?>
                        <div class="help-block hidden" id="help_block_city">City cannot be blank.</div>
                        <input id="saved_cities" value="<?= $model->saved_cities ?>" type="hidden">
                    </div>


                </div>
            </div>

            <div class="panel panel-default ">
                <div class="panel-heading"><strong>Addons</strong></div>
                <div class="row padding20">
                    <div class="col-xs-4 col-sm-4">
                        <label class="control-label" for="Contracts-addons_quantity">Addons </label>
                    </div>
                    <div class="col-xs-4 col-sm-4">
                        <label class="control-label" for="Contracts-addons_quantity"> Free Allowed</label>
                    </div>
                    <div class="col-xs-4 col-sm-4">
                        <label class="control-label" for="Contracts-addons_quantity"> Price of Additional usage</label>
                    </div>
                    <div id="addons">
                        <?php
                        if (!empty($model->saved_addons)) {
                            foreach ($model->saved_addons as $key => $addon) {
                                ?>
                                <div class="col-xs-4 col-sm-4">
                                    <div class="form-group field-contracts-addons-0-addon_name">
                                        <input type="text" id="contracts-addons-<?= $key ?>-addon_name"
                                               class="form-control" value="<?= $addon['addon_title']; ?>"
                                               name="Contracts[addons][<?= $key ?>][addon_name]" readonly>
                                        <div class="help-block"></div>
                                    </div>
                                    <div class="form-group field-contracts-addons-0-addon_id">
                                        <input type="text" id="contracts-addons-<?= $key ?>-addon_id" class="hide"
                                               value="<?= $addon['addon_id']; ?>"
                                               name="Contracts[addons][<?= $key ?>][addon_id]">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <div class="form-group field-contracts-addons-0-addons_quantity">
                                        <input type="text" id="contracts-addons-<?= $key ?>-addons_quantity"
                                               class="form-control" value="<?= $addon['qunatity']; ?>"
                                               name="Contracts[addons][<?= $key ?>][addons_quantity]">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <div class="form-group field-contracts-addons-0-addons_price">
                                        <input type="text" id="contracts-addons-<?= $key ?>-addons_price"
                                               class="form-control" value="<?= $addon['price']; ?>"
                                               name="Contracts[addons][<?= $key ?>][addons_price]" readonly>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>

                        <!--  <div class="col-xs-4 col-sm-4">

                        <? /*= $form->field($model, 'addons[0][addon_name]')->textInput()->label(false);
                        */ ?>
                        <? /*= $form->field($model, 'addons[0][addon_id]')->textInput(['class'=>'hide'])->label(false);
                        */ ?>
                    </div>
                    <div class="col-xs-4 col-sm-4">

                        <? /*= $form->field($model, 'addons[0][addons_quantity]')->textInput()->label(false);
                        */ ?>
                    </div>
                    <div class="col-xs-4 col-sm-4">

                        <? /*= $form->field($model, 'addons[0][addons_price]')->textInput()->label(false);
                        */ ?>
                    </div>-->


                    </div>
                </div>
            </div>

            <div class="panel panel-default ">
                <div class="panel-heading"><strong>Payment Information</strong></div>
                <div class="row padding20">
                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'payment_method_id')->dropDownList(ArrayHelper::map(PaymentMethods::find()->where(['status' => 1])->orderBy('name')->asArray()->all(), 'id', 'name'))->label('Payment Method') ?>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'payment_term_id')->dropDownList(ArrayHelper::map(PaymentTerms::find()->where(['status' => 1])->orderBy('name')->asArray()->all(), 'id', 'name'))->label('Payment Terms') ?>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'status')->dropDownList(['pending' => 'Pending', 'active' => 'Active', 'hold' => 'Hold', 'cancel' => 'Cancel', 'closed' => 'Closed', 'declined' => 'Declined']) ?>
                    </div>

                    <div class="clearfix"></div>
                    <div id="card" class="<?= ($model->payment_method_id == 2)? '':'hide'; ?>">
                        <div class="col-xs-12 col-sm-6">
                            <?= $form->field($model, 'frequency')->textInput(['maxlength' => true, 'type' => 'number', 'class' => 'form-control']) ?>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <?= $form->field($model, 'recurring_amount')->textInput(['maxlength' => true, 'class' => 'validate_float_number form-control'])->label('Recurring Amount') ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default ">
                <div class="panel-heading"><strong>Terms And Conditions</strong></div>
                <div class="row padding20">
                    <div class="col-xs-12">
                        <?= $form->field($model, 'terms')->textarea(['rows' => 2, 'value' => $terms]) ?>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'client_notes')->textarea(['rows' => 2, 'value' => $client_notes]) ?>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'admin_notes')->textarea(['rows' => 2, 'value' => $admin_notes]) ?>
                    </div>
                </div>
            </div>


            <div class="box">
                <div class="box-body">
                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end();

            ?>
        </div>


        <?= $this->registerJs('
        
 var saved_cities = jQuery("#saved_cities").val();
 if(saved_cities != ""){
  var res = saved_cities.split(",");
 jQuery(\'#contracts-cities\').val(res).trigger(\'change\');
 }
           
    $("body").on("change", "#contracts-package_id", function(e) {
        var package = $(this).val();
        $.ajax({
            url: "' . Url::base(true) . '/contracts/getpackage?id="+package,
            type: "GET",
            success : function(res){
                var obj = JSON.parse(res);
                $("#addons").html(obj.addons);
                $("#contracts-price").val(obj.price);
                $("#duration_drop_box").html(obj.duration);
                
                $(\'#contracts-duration\').on(\'change\',function(){
     
       var duration_price = $(\'option:selected\', this).attr(\'data_price\');
         $("#contracts-price").val(duration_price); 
    });
           
            },
            error : function(data){
                console.log(data);
            }
        });    
    });
    $("body").on("change", "#contracts-user_id", function(e) {
        var user_id = $(this).val();
        $.ajax({
            url: "' . Url::base(true) . '/contracts/checkjoiningfee?id="+user_id,
            type: "GET",
            success : function(res){
             console.log(res);
             if(res == 1){
             $("#joining_fee").hide();
             }else{
             $("#joining_fee").show();
             }
               /* var obj = JSON.parse(res);
                $("#addons").html(obj.addons);
                $("#duration_drop_box").html(obj.duration);
                $("#contracts-price").val(obj.price);*/
           
            },
            error : function(data){
                console.log(data);
            }
        });    
    });
    
  $(\'#w0\').on(\'submit\',function(event) {
        var validate = 1;
        var package = $(\'#contracts-package_id\').val();
        var user = $(\'#contracts-user_id\').val();

        if(package == \'\') {
            $(\'#help_block_package\').removeClass(\'hidden\');
            $(\'#select_package\').addClass(\'has-error\');
            validate = 0;
        }
        if(user == \'\') {
            $(\'#help_block_user\').removeClass(\'hidden\');
            $(\'#select_user\').addClass(\'has-error\');
            validate = 0;
        }
          if(user == \'\') {
            $(\'#help_block_city\').removeClass(\'hidden\');
            $(\'#select_city\').addClass(\'has-error\');
            validate = 0;
        }
        if(validate == 1){
            return true;
        }else{
            return false;
        }
        event.preventDefault();
    });
    $(\'#contracts-package_id\').on(\'change\',function(){
        var value_package = $(this).val();
        if(value_package != \'\'){
            $(\'#help_block_package\').addClass(\'hidden\');
            $(\'#select_package\').removeClass(\'has-error\');
        }else{
            $(\'#help_block_package\').removeClass(\'hidden\');
            $(\'#select_package\').addClass(\'has-error\');
        }
    });
    $(\'#contracts-user_id\').on(\'change\',function(){
        var value_user = $(this).val();
        if(value_user != \'\'){
            $(\'#help_block_user\').addClass(\'hidden\');
            $(\'#select_user\').removeClass(\'has-error\');
        }else{
            $(\'#help_block_user\').removeClass(\'hidden\');
            $(\'#select_user\').addClass(\'has-error\');
        }
    });
    
     $(\'#contracts-cities\').on(\'change\',function(){
        var value_city = $(this).val();
        if(value_city != \'\'){
            $(\'#help_block_city\').addClass(\'hidden\');
            $(\'#select_city\').removeClass(\'has-error\');
        }else{
            $(\'#help_block_city\').removeClass(\'hidden\');
            $(\'#select_city\').addClass(\'has-error\');
        }
    });
    
    
    $(\'#contracts-duration\').on(\'change\',function(){
       var duration_price = $(\'option:selected\', this).attr(\'data_price\');
         $("#contracts-price").val(duration_price); 
    });
    
'); ?>