<?php

use app\models\Package;
use app\models\PaymentMethods;
use app\models\User;
use app\widgets\CustomGridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContractsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contracts';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .btn-xs {
        margin: 1px;
    }
</style>
<div class="Contracts-index">
    <?php
    if (Yii::$app->user->identity->user_type == 1) {
        echo $this->render('_search', ['model' => $searchModel]);
    }

    ?>

    <?= CustomGridView::widget([
        'create' => $createLink,
        'dataProvider' => $dataProvider,
        /*'filterModel' => $searchModel,*/
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['class' => 'noprint', 'style' => 'width:10px;'],],
            [
                'attribute' => 'user_id',
                'label' => 'User',
                'value' => function ($model) {
                    return ($model->user <> null) ? $model->user->username : "-";
                }
            ],
            [
                'attribute' => 'package_id',
                'label' => 'Package',
                'value' => function ($model) {
                    return ($model->package <> null) ? $model->package->title : "-";
                },
            ],
            'start_date',
            'end_date',
            'current_expiry',
            [
                'attribute' => 'payment_method_id',
                'label' => 'Payment Method',
                'value' => function ($model) {
                    return ($model->paymentMethod <> null) ? $model->paymentMethod->name : "-";
                },
            ],
            'free_days',
            ['format' => 'html', 'attribute' => 'status', 'value' => function ($model) {
                return $model->txtStatus;
            }, 'filter' => Yii::$app->params['statusArr']],
            /* 'update_at',*/
            /*  'create_at',*/
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['class' => 'noprint', 'style' => 'width:100px;'],
                'contentOptions' => ['class' => 'noprint text-right'],
                'template' => '{invoice} {renewal}  {view} {update} {delete} {unfreez}',
                'visibleButtons' => [
                    'view' => function ($model, $key, $index) {

                        return true;
                    },
                    'renewal' => function ($model, $key, $index) {
                        if (Yii::$app->user->identity->user_type == 1) {


                            $end_date = strtotime($model->current_expiry); // or your date as well
                            $ren_date = strtotime("+30 days");
                            if ($end_date <= $ren_date) {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    },
                    'unfreez' => function ($model, $key, $index) {
                        if ($model->is_freeze == 1 && $model->status == 'active') {
                            return true;
                        } else {
                            return false;
                        }
                    },
                    'update' => function ($model, $key, $index) {
                        return false;
                    },
                    'delete' => function ($model, $key, $index) {
                        return false;
                    },
                ],
                'buttons' => [

                    'invoice' => function ($url, $model) {
                        if (Yii::$app->user->identity->user_type == 1) {
                            $url = \yii\helpers\Url::to(['/invoices/send-invoice-email/', 'id' => $model->id, 'mode' => 'contract']);
                            return Html::a('<i class="fa fa-money"></i>', $url, [
                                'title' => Yii::t('app', 'Email'),
                                'data-toggle' => 'tooltip',
                                'class' => 'btn btn-info btn-xs',
                            ]);
                        } else {
                            return false;
                        }


                    },
                    'renewal' => function ($url, $model) {
                        $url = Url::to(['/contracts/create/']);
                        return Html::a('<i class="fa fa-refresh"></i>', $url, [
                            'title' => Yii::t('app', 'Renewal'),
                            'data-toggle' => 'tooltip',
                            'class' => 'btn btn-info btn-xs',
                        ]);

                    },
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fa fa-folder"></i>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'data-toggle' => 'tooltip',
                            'class' => 'btn btn-info btn-xs',
                        ]);
                    },
                    'unfreez' => function ($url, $model) {
                        $url = Url::to(['/freeze-contract/']);
                        return Html::a('Un Freeze', $url, [
                            'title' => Yii::t('app', 'Un freeze'),
                            'data-toggle' => 'tooltip',
                            'class' => 'btn btn-warning btn-xs',
                        ]);

                    },
                    /* 'update' => function ($url, $model) {
                         return Html::a('<i class="fa fa-edit"></i>', $url, [
                             'title' => Yii::t('app', 'Update'),
                             'data-toggle' => 'tooltip',
                             'class' => 'btn btn-success btn-xs',
                         ]);
                     },
                     'delete' => function ($url, $model) {
                         return Html::a('<i class="fa fa-remove"></i>', $url, [
                             'title' => Yii::t('app', 'Delete'),
                             'data-toggle' => 'tooltip',
                             'data-method' => 'post',
                             'data-confirm' => Yii::t('app', 'Are you sure you want to delete this?'),
                             'class' => 'btn btn-danger btn-xs',
                         ]);
                     },*/
                ],
            ],
        ],
    ]); ?>
</div>
