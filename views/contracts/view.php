<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Contracts */

$this->title ="View Contract";
$this->params['breadcrumbs'][] = ['label' => 'Contracts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$is_freeze = $model->is_freeze;
?>
<div class="Contracts-view box">
    <div class="box-header with-border">
        <h3></h3>
        <?php if($is_freeze!=0){?>
            <div class="col-xs-12 col-sm-6">
                <?= Html::a('<i class="fa fa-edit"> Un Freeze</i>', 'freeze-contract', ['class' => 'btn btn-success', 'data-toggle'=>'tooltip', 'title'=>'Un Freez']) ?>
            </div>
        <?php }?>
        <?php if(Yii::$app->user->identity->user_type == 1){?>
        <div class="box-tools pull-right">
            <?= Html::a('<i class="fa fa-edit"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-box-tool', 'data-toggle'=>'tooltip', 'title'=>'Update']) ?>
            <?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn btn-box-tool', 'data-toggle'=>'tooltip', 'title'=>'Delete', 'data'=>['method' => 'post', 'confirm' => 'Are you sure you want to delete this?',]]) ?>
            <?= Html::a('<i class="fa fa-times"></i>', ['index'], ['class' => 'btn btn-box-tool', 'data-toggle'=>'tooltip', 'title'=>'Back']) ?>
        </div>
        <?php }?>
    </div>
    <div class="box-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
	          /*  'user_id',*/
            ['attribute'=>'user_id','value'=>$model->user->username],
            ['attribute'=>'package_id','value'=>$model->package->title],
          /*  'package_id',*/
            'start_date',
            'end_date',
            'current_expiry',
            ['attribute'=>'user_id','value'=>$model->paymentMethod->name],
            ['attribute'=>'package_id','value'=>$model->paymentTerm->name],
            /*'payment_method_id',
            'payment_term_id',*/
           /* 'check_number',*/
            /*'check_date',
            'card_number',
            'card_expiry_date',
            'cvv',*/
            'free_days',
            'txtStatus:html',
           /* 'update_at',
            'create_at',*/
            ],
        ]) ?>
    </div>
</div>
