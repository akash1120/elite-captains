<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Package;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ContractsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="Contracts-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-sm-3" style="padding-left:0px: ">
        <label>Member</label>
        <?= \kartik\select2\Select2::widget([
            'model' => $model,
            'attribute' => 'user_id',
            'data' => \yii\helpers\ArrayHelper::map(User::find()->all(), 'id', function ($model) {
                //return $model->first_name . " " . $model->last_name;
                return $model->username;
            }),
            'options' => ['placeholder' => 'Search Member ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>
    <div class="col-sm-3" style="padding-left:0px: ">
        <label>Package</label>
        <?= \kartik\select2\Select2::widget([
            'model' => $model,
            'attribute' => 'package_id',
            'data' => \yii\helpers\ArrayHelper::map(\app\models\Package::find()->all(), 'id', function ($model) {
                return $model->title;
            }),
            'options' => ['placeholder' => 'Search Package ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>

    <div class="col-sm-3" style="padding-left:0px: ">
        <label>Start Date</label>
        <?php
        $current_date = date('Y-m-d');
        ?>
        <?=
        DatePicker::widget([
            'name' => 'ContractsSearch[start_date]',
            'type' => DatePicker::TYPE_INPUT,
            /*'value' => $current_date,*/
            'options' => ['placeholder' => 'Start date'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);
        ?>
    </div>
    <div class="col-sm-3" style="padding-left:0px: ">
        <label>End Date</label>
        <?php
        $current_date = date('Y-m-d');
        ?>
        <?=
        DatePicker::widget([
            'name' => 'ContractsSearch[end_date]',
            'type' => DatePicker::TYPE_INPUT,
            /*'value' => $current_date,*/
            'options' => ['placeholder' => 'End date'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);
        ?>
    </div>
    <div class="col-sm-3" style="padding-left:0px: ">
        <label>Current Expiry</label>
        <?php
        $current_date = date('Y-m-d');
        ?>
        <?=
        DatePicker::widget([
            'name' => 'ContractsSearch[current_expiry]',
            'type' => DatePicker::TYPE_INPUT,
            /*'value' => $current_date,*/
            'options' => ['placeholder' => 'Current Expiry'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);
        ?>
    </div>

    <div class="col-sm-3" style="padding-left:0px: ">
        <label>Status</label>
        <?= \kartik\select2\Select2::widget([
            'model' => $model,
            'attribute' => 'status',
            'data' => [
                'pending' => 'Pending',
                'active' => 'Active',
                'closed' => 'Closed',
                'declined' => 'Declined',
            ],
            'options' => ['placeholder' => 'Search Status ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>

    <div class="col-sm-4">
        <label>Renewal In</label>
    <div class="input-group" style="display: block; margin-top: -15px;">
        <?=  $form->field($model, 'renewal_in')->textInput(['maxlength' => 10, 'type'=> 'number', 'min'=> 1, 'class' => 'form-control','style' =>  'width: 30%; margin-bottom:0px;'])->label(false) ?>
        <?=  $form->field($model, 'renewal_duration')->dropDownList(['days' => 'Days', 'months' => 'Months', 'year' => 'Year'],['prompt'=>'Select Duration Period','style' =>  'width: 69%','class' => 'form-control'])->label(false) ?>

    </div>
    </div>



    <?php // $form->field($model, 'start_date') ?>

    <?php // $form->field($model, 'end_date') ?>

    <?php // echo $form->field($model, 'payment_method_id') ?>

    <?php // echo $form->field($model, 'payment_term_id') ?>

    <?php // echo $form->field($model, 'check_number') ?>

    <?php // echo $form->field($model, 'check_date') ?>

    <?php // echo $form->field($model, 'card_number') ?>

    <?php // echo $form->field($model, 'card_expiry_date') ?>

    <?php // echo $form->field($model, 'cvv') ?>

    <?php // echo $form->field($model, 'free_days') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'update_at') ?>

    <?php // echo $form->field($model, 'create_at') ?>

<div class="clearfix"></div>
    <div class="form-group pull-right">

        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>


    <?php ActiveForm::end(); ?>
</div>
<div class="clearfix"></div>
