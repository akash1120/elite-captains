<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ContractsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="Contracts-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'package_id') ?>

    <?= $form->field($model, 'start_date') ?>

    <?= $form->field($model, 'end_date') ?>

    <?php // echo $form->field($model, 'payment_method_id') ?>

    <?php // echo $form->field($model, 'payment_term_id') ?>

    <?php // echo $form->field($model, 'check_number') ?>

    <?php // echo $form->field($model, 'check_date') ?>

    <?php // echo $form->field($model, 'card_number') ?>

    <?php // echo $form->field($model, 'card_expiry_date') ?>

    <?php // echo $form->field($model, 'cvv') ?>

    <?php // echo $form->field($model, 'free_days') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'update_at') ?>

    <?php // echo $form->field($model, 'create_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    $("body").on("change", "#contracts-package_id", function(e) {

        var package = $(this).val();
        $.ajax({
            url: "' . Url::base(true) . '/contracts/getpackage?id="+package,
            type: "GET",
            success : function(res){
                var obj = JSON.parse(res);
                $("#addons").html(obj.addons);
                $("#contracts-price").val(obj.price);

            },
            error : function(data){
                console.log(data);
            }
        });
    });
    $('#w0').on('submit',function(event) {
        var validate = 1;
        var package = $('#contracts-package_id').val();
        var user = $('#contracts-user_id').val();

        if(package == '') {
            $('#help_block_package').removeClass('hidden');
            $('#select_package').addClass('has-error');
            validate = 0;
        }
        if(user == '') {
            $('#help_block_user').removeClass('hidden');
            $('#select_user').addClass('has-error');
            validate = 0;
        }
        if(validate == 1){
            return true;
        }else{
            return false;
        }
        event.preventDefault();
    });
    $('#contracts-package_id').on('change',function(){
        var value_package = $(this).val();
        if(value_package != ''){
            $('#help_block_package').addClass('hidden');
            $('#select_package').removeClass('has-error');
        }else{
            $('#help_block_package').removeClass('hidden');
            $('#select_package').addClass('has-error');
        }

    });
    $('#contracts-user_id').on('change',function(){
        var value_user = $(this).val();
        if(value_user != ''){
            $('#help_block_user').addClass('hidden');
            $('#select_user').removeClass('has-error');
        }else{
            $('#help_block_user').removeClass('hidden');
            $('#select_user').addClass('has-error');
        }
    });

</script>
