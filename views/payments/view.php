<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */


$this->params['breadcrumbs'][] = ['label' => 'Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="payments-view card-box">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'payment_ref',
            'invoice_id',
            'trax_ref',
            'status',
            /*'payment_method_id',*/
            ['attribute'=>'payment_method_id','value'=>$model->paymentMethod->name],
            ['attribute'=>'user_id','value'=>$model->user->username],
            /*'user_id',*/
           /* 'received_by',*/
            'amount',
            'discount',
            'note:ntext',
            'date',
            'response_code',
            'pt_invoice_id',
            'transaction_id',
            'frequency',
            'currency',
        ],
    ]) ?>

    <section class="content-header" style="padding-top:0px;">
        <h1>Payment Records</h1>
    </section>
    <section class="content">
        <div class="box box-success">
            <div class="box-body">
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'payment_ref',
            'invoice_id',
            'trax_ref',
            'status',
            'cheque_number',
            'cheque_status',
        ]
]); ?>
            </div>
        </div>
    </section>

</div>
