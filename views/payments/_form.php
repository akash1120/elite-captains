<?php
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Package;
use app\models\Addon;
use app\models\ContarctAddons;
use app\models\PaymentMethods;
use app\models\PaymentTerms;
use app\models\TermsAndConditions;
use yii\helpers\Url;
use unclead\multipleinput\MultipleInput;
use app\assetfiles\DatePickerAsset;

DatePickerAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs('

	/*$(document).delegate("#Contracts-payment_method_id", "change", function() {
	if($(this).val()==1){
	        $("#card").hide();
			$("#cash").show(500);
			
		}else{
		  $("#cash").hide();
          $("#card").show(500);
		}
	});*/
');

$terms_and_conditions = TermsAndConditions::find()->where(['id' => 1])->asArray()->one();
/*echo "<pre>";
print_r($invoice);
die;*/
?>
<style>
    .padding20 {
        padding: 20px;
    }

    .hide {
        display: none;
    }
</style>
<div class="user-form">
    <div class="box box-success">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="box-body">
            <div class="panel panel-default ">
                <div class="panel-heading"><strong>Required Information</strong></div>
                <div class="row padding20">
                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'invoice_id')->textInput(['readonly' => true, 'value' => $invoice->invoice_id])->label('Invoice Number') ?>
                        <?= $form->field($model, 'invoice_id')->hiddenInput(['readonly' => true, 'value' => $invoice->id])->label(false) ?>
                    </div>

                    <div class="col-xs-12 col-sm-6">

                        <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(user::find()->where(['user_type' => 1, 'status' => 1])->orderBy('username')->asArray()->all(), 'id', 'username'), array("disabled" => "disabled"), ['prompt' => Yii::t('app', 'Select')])->label('Users') ?>

                        <?= $form->field($model, 'user_id')->hiddenInput(['readonly' => true, 'value' => $invoice->user_id])->label(false) ?>
                    </div>

                </div>
            </div>
            <?php if(!isset($model->id)){ ?>
            <div class="panel panel-default ">
                <div class="panel-heading"><strong>payment Modes</strong></div>
                <div class="row padding20">
                    <div class="col-xs-12 col-sm-6 ">
                        <?= $form->field($model, 'payment_method_id')->dropDownList(ArrayHelper::map(PaymentMethods::find()->where(['status' => 1, 'id' => [1, 3]])->orderBy('name')->asArray()->all(), 'id', 'name'))->label('Payment Method') ?>
                    </div>
                    <div class="col-xs-12 col-sm-6" id="number_of_cheques_div">
                        <?= $form->field($model, 'number_of_cheques')->dropDownList([1 => '1', 2 => '2', 3 => '3', 4 => '4', 5 => '5', 6 => '6', 7 => '7', 8 => '8', 9 => '9', 10 => '10', 11 => '11', 12 => '12'], ['prompt' => Yii::t('app', 'Select')]) ?>
                    </div>
                    <div class="clearfix"></div>
                    <div id="checque_data">
                        <div class="col-xs-4 col-sm-4">
                            <label class="control-label" for="Contracts-addons_quantity">Cheque Number </label>
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <label class="control-label" for="Contracts-addons_quantity"> Date</label>
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <label class="control-label" for="Contracts-addons_quantity"> Amount</label>
                        </div>



                        <div id="addons">

                        </div>
                        <div class="col-xs-12 col-sm-12">
                            <label class="control-label pull-right" for="Contracts-addons_quantity"> Total : <span id="cheque_total"></span></label>
                        </div>

                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="panel panel-default ">
                <div class="panel-heading"><strong>Payment Information</strong></div>
                <div class="row padding20">
                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'payment_ref')->textInput(['maxlength' => true])->label('Ref#') ?>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'date')->textInput(['maxlength' => true, 'value' => date('Y-m-d H:i:s'), 'readonly' => true]) ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'amount')->textInput(['maxlength' => true, 'value' => ($invoice->total - $paid_amount - $discount)])->label('Amount (Remaining)') ?>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'discount')->textInput(['maxlength' => true, 'value' => 0]) ?>
                    </div>

                    <?php if($paid_amount <> null){ ?>
                        <div class="clearfix"></div>
                    <div class="col-xs-12 col-sm-6">
                        <label class="control-label" for="payments-amount">Alraedy Payed Amount</label>
                        <input type="text"  class="form-control"  value="<?= $paid_amount; ?>" aria-required="true" aria-invalid="false" readonly>
                    </div>
                        <div class="col-xs-12 col-sm-6">
                            <label class="control-label" for="payments-amount">Alraedy Given Discount</label>
                            <input type="text"  class="form-control"  value="<?= $discount; ?>" aria-required="true" aria-invalid="false" readonly>
                        </div>

                        <div class="col-xs-12 col-sm-6">
                            <label class="control-label" for="payments-amount">Total Payable Amount</label>
                            <input type="text"  class="form-control"  value="<?= $invoice->total; ?>" aria-required="true" aria-invalid="false" readonly>
                        </div>
                    <?php } ?>

                   <!-- <div class="col-xs-12 col-sm-6">
                        <?/*= $form->field($model, 'status')->dropDownList([ 'paid' => 'Paid', 'cancelled' => 'Cancelled','declined' => 'Declined',]) */?>
                    </div>-->
                    <div class="col-xs-12">
                        <?= $form->field($model, 'note')->textarea(['rows' => 2]) ?>
                    </div>
                </div>
            </div>



            <div class="box">
                <div class="box-body">
                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end();

            ?>
        </div>

        <?= $this->registerJs(' 
           
        
        $("#w0").submit(function() {
         var inputs = $(".price_input");
         var dropdown = $("#payments-payment_method_id").val();
         var payments_discount = $("#payments-discount").val();
         
        
       if(dropdown == "3"){
       var dropdown_term = $("#payments-number_of_cheques").val();
       
       if(dropdown_term == ""){
         swal("value of number of cheques should not b empty","Please select number of cheques.","error");
                return false;
         }
       
         var total_amount = parseInt($("#payments-amount").val());
         var cheque_sum = 0;
            for(var i = 0; i < inputs.length; i++){
                cheque_sum = parseInt(cheque_sum) + parseInt($(inputs[i]).val());
            }
             cheque_sum = parseInt(cheque_sum) + parseInt(payments_discount);
            
            $("#cheque_total").text(cheque_sum);
            
            if(total_amount != cheque_sum){
              swal("value of amount in check is not equal to total amount","Please enter correct Amount.","error");
                return false;
            }else{
                return true;
            }   
         }
         return true
          
          });
              
     $("#number_of_cheques_div").hide();
      $("#checque_data").hide();
     $("body").on("change", "#payments-payment_method_id", function(e) {
  
        var method= $(this).val();
        if(method == 3){
         $("#number_of_cheques_div").show();
         $("#checque_data").show(); 
          $("body").on("change", "#payments-number_of_cheques", function(e) {
            var no_of_cheques= $(this).val();
            var total_amount= $("#payments-amount").val();
              var i;
                $.ajax({
                    url: "' . Url::base(true) . '/payments/getcheques?id="+no_of_cheques+"&total="+total_amount,
                    type: "GET",
                    success : function(res){
                     var obj = JSON.parse(res);
                     console.log(obj);
                        $("#addons").html(obj);
                       
                        $(".dtpicker").datepicker({
		                format: "yyyy-mm-dd",
		                todayHighlight: true,
	                    }).on("changeDate", function(e) {
		                $(this).datepicker("hide");
	});                        
            },
            error : function(data){
                console.log(data);
            }
        });         
           });
        }else{
          $("#number_of_cheques_div").hide();
           $("#checque_data").hide();
        } 
          
    });
'); ?>
