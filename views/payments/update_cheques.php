<?php
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Package;
use app\models\Addon;
use app\models\ContarctAddons;
use app\models\PaymentMethods;
use app\models\PaymentTerms;
use app\models\TermsAndConditions;
use yii\helpers\Url;
use unclead\multipleinput\MultipleInput;
use app\assetfiles\DatePickerAsset;

DatePickerAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs('

	/*$(document).delegate("#Contracts-payment_method_id", "change", function() {
	if($(this).val()==1){
	        $("#card").hide();
			$("#cash").show(500);
			
		}else{
		  $("#cash").hide();
          $("#card").show(500);
		}
	});*/
');

$terms_and_conditions = TermsAndConditions::find()->where(['id' => 1])->asArray()->one();

?>
<style>
    .padding20 {
        padding: 20px;
    }

    .hide {
        display: none;
    }
</style>
<div class="user-form">
    <div class="box box-success">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="box-body">


            <div class="panel panel-default ">
                <div class="panel-heading"><strong>Update Status</strong></div>
                <div class="row padding20">
                    <div id="checque_data">
                        <div class="col-xs-4 col-sm-4">
                            <label class="control-label" for="Contracts-addons_quantity">Cheque Number </label>
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <label class="control-label" for="Contracts-addons_quantity"> Date</label>
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <label class="control-label" for="Contracts-addons_quantity"> Amount</label>
                        </div>
                        <div id="addons">
                            <?php
                            if (!empty($model->saved_cheques)) {
                                foreach ($model->saved_cheques as $key => $cheque) {
                                    $status = $cheque['status'];
                                    ?>

                                    <div class="col-xs-12 col-sm-4">
                                        <div class="form-group field-payments-cheques-<?= $key ?>-cheque_number">
                                            <input type="text" id="payments-cheques-<?= $key ?>-cheque_number" class="form-control " name="" value="<?= $cheque['cheque_number']; ?>" readonly>
                                            <input type="text" id="payments-cheques-<?= $key ?>-cheque_number" class="form-control hide" name="cheques[<?= $key ?>][id]" value="<?= $cheque['id']; ?>" readonly>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="form-group field-payments-cheques-<?= $key ?>-date">
                                            <input type="text" id="payments-cheques-<?= $key ?>-date" class="form-control" name="" value="<?= date('d-m-Y',strtotime($cheque['date'])); ?>" readonly>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="form-group field-payments-cheques-<?= $key ?>-amount">
                                            <input type="text" id="payments-cheques-<?= $key ?>-amount" class="form-control" name="" value="<?= $cheque['amount']; ?>" readonly>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-2">
                                        <div class="form-group field-payments-cheques-<?= $key ?>-status">

                                            <select class="form-control" name="cheques[<?= $key ?>][status]">
                                                <option <?php if($status == 'pending'){ echo 'selected';} ?> value="pending">Pending</option>
                                                <option <?php if($status == 'paid'){ echo 'selected';} ?> value="paid">Paid</option>
                                                <option <?php if($status == 'cash'){ echo 'selected';} ?> value="cash">By cash</option>
                                                <option <?php if($status == 'online'){ echo 'selected';} ?> value="online">Credit Card</option>
                                                <option <?php if($status == 'declined'){ echo 'selected';} ?> value="declined">Declined</option>
                                            </select>


                                          <!--  <input type="text" id="payments-cheques-<?/*= $key */?>-status" class="form-control" name="cheques[<?/*= $key */?>][status]" value="<?/*= $cheque['status']; */?>" readonly>-->
                                            <div class="help-block"></div>
                                        </div>
                                    </div>

                                    <?php
                                }
                            }
                            ?>


                        </div>
                        <div class="col-xs-12 col-sm-12">
                            <label class="control-label pull-right" for="Contracts-addons_quantity"> Total : <span id="cheque_total"></span><?= $model->amount; ?></label>
                        </div>

                    </div>
                </div>
            </div>

            <div class="box">
                <div class="box-body">
                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end();

            ?>
        </div>

        <?= $this->registerJs(' 
           
        
        $("#w0").submit(function() {
         var inputs = $(".price_input");
         var dropdown = $("#payments-payment_method_id").val();
         
        
       if(dropdown == "3"){
       var dropdown_term = $("#payments-number_of_cheques").val();
       
       if(dropdown_term == ""){
         swal("value of number of cheques should not b empty","Please select number of cheques.","error");
                return false;
         }
       
         var total_amount = parseInt($("#payments-amount").val());
         var cheque_sum = 0;
            for(var i = 0; i < inputs.length; i++){
                cheque_sum = parseInt(cheque_sum) + parseInt($(inputs[i]).val());
            }
            $("#cheque_total").text(cheque_sum);
            
            if(total_amount != cheque_sum){
              swal("value of amount in check is not equal to total amount","Please enter correct Amount.","error");
                return false;
            }else{
                return true;
            }   
         }
         return true
          
          });
              
     $("#number_of_cheques_div").hide();
      //$("#checque_data").hide();
     $("body").on("change", "#payments-payment_method_id", function(e) {
  
        var method= $(this).val();
        if(method == 3){
         $("#number_of_cheques_div").show();
         $("#checque_data").show(); 
          $("body").on("change", "#payments-number_of_cheques", function(e) {
            var no_of_cheques= $(this).val();
            var total_amount= $("#payments-amount").val();
              var i;
                $.ajax({
                    url: "' . Url::base(true) . '/payments/getcheques?id="+no_of_cheques+"&total="+total_amount,
                    type: "GET",
                    success : function(res){
                     var obj = JSON.parse(res);
                     console.log(obj);
                        $("#addons").html(obj);
                       
                        $(".dtpicker").datepicker({
		                format: "yyyy-mm-dd",
		                todayHighlight: true,
	                    }).on("changeDate", function(e) {
		                $(this).datepicker("hide");
	});                        
            },
            error : function(data){
                console.log(data);
            }
        });         
           });
        }else{
          $("#number_of_cheques_div").hide();
           $("#checque_data").hide();
        } 
          
    });
'); ?>
