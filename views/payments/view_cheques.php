<?php
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php   if (Yii::$app->user->identity->user_type == 1) { ?>
<p>
    <?= Html::a('Update', ['cheque', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
</p>
<?php } ?>

<div class="user-form">
    <div class="box box-success">
        <div class="box-body">
                    <div class="clearfix"></div>
                    <div id="checque_data">
                        <div class="col-xs-4 col-sm-4">
                            <label class="control-label" for="Contracts-addons_quantity">Cheque Number </label>
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <label class="control-label" for="Contracts-addons_quantity"> Date</label>
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <label class="control-label" for="Contracts-addons_quantity"> Amount</label>
                        </div>


                        <div id="addons">
                            <?php
                            if (!empty($model->saved_cheques)) {
                            foreach ($model->saved_cheques as $key => $cheque) {
                            ?>

                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group field-payments-cheques-<?= $key ?>-cheque_number">
                                    <input type="text" id="payments-cheques-<?= $key ?>-cheque_number" class="form-control " name="Payments[cheques][<?= $key ?>][cheque_number]" value="<?= $cheque['cheque_number']; ?>" readonly>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3">
                                <div class="form-group field-payments-cheques-<?= $key ?>-date">
                                    <input type="text" id="payments-cheques-<?= $key ?>-date" class="form-control dtpicker" name="Payments[cheques][<?= $key ?>][date]" value="<?= date('d-m-Y',strtotime($cheque['date'])); ?>" readonly>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3">
                                <div class="form-group field-payments-cheques-<?= $key ?>-amount">
                                    <input type="text" id="payments-cheques-<?= $key ?>-amount" class="form-control price_input" name="Payments[cheques][<?= $key ?>][amount]" value="<?= $cheque['amount']; ?>" readonly>
                                    <div class="help-block"></div>
                                </div>
                            </div>

                                <div class="col-xs-12 col-sm-2">
                                    <div class="form-group field-payments-cheques-<?= $key ?>-amount">
                                        <input type="text" id="payments-cheques-<?= $key ?>-amount" class="form-control price_input" name="Payments[cheques][<?= $key ?>][status]" value="<?= $cheque['status']; ?>" readonly>
                                        <div class="help-block"></div>
                                    </div>
                                </div>

                                <?php
                            }
                            }
                            ?>


                        </div>
                        <div class="col-xs-12 col-sm-12">
                            <label class="control-label pull-right" for="Contracts-addons_quantity"> Total : <span id="cheque_total"><?= $model->amount; ?></span></label>
                        </div>

                    </div>

        </div>
    </div>
</div>
