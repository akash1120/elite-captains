<?php

/*use yii\helpers\Html;
use yii\grid\GridView;*/
use app\widgets\CustomGridView;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-index">
    <!--
    <h1><? /*= Html::encode($this->title) */ ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            /*  'id',*/
            'payment_ref',
            /* 'invoice_id',*/
            [
                'attribute' => 'invoice_id',
                'label' => 'Invoice Number',
                'value' => function ($model) {
                    return ($model->invoice <> null) ? $model->invoice->invoice_id : "-";
                }
            ],
            /*'trax_ref',*/
            [
                'attribute' => 'user_id',
                'label' => 'User',
                'value' => function ($model) {
                    return ($model->user <> null) ? $model->user->username : "-";
                }
            ],
            [
                'attribute' => 'payment_method_id',
                'label' => 'Method',
                'value' => function ($model) {
                    return ($model->paymentMethod <> null) ? $model->paymentMethod->name : "-";
                }
            ],
            /*'payment_method_id',*/
            //'received_by',
            'amount',
            'discount',
            //'note:ntext',
            'date',
            //'response_code',
            //'pt_invoice_id',
            'transaction_id',
            'frequency',
            //'currency',
            ['format' => 'html', 'attribute' => 'status', 'value' => function ($model) {
                return $model->txtStatus;
            }, 'filter' => Yii::$app->params['statusArr']],
            /* ['class' => 'yii\grid\ActionColumn'],*/
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['class' => 'noprint', 'style' => 'width:100px;'],
                'contentOptions' => ['class' => 'noprint text-center'],
                'template' => '{send-receipt} {view} {update} {cheques}',
                'visibleButtons' => [
                    'view' => function ($model, $key, $index) {

                        return true;
                    },
                    'update' => function ($model, $key, $index) {

                        return true;
                    },

                    'send-receipt' => function ($model, $key, $index) {
                        return true;
                    },
                    'cheques' => function ($model, $key, $index) {
                        if ($model->payment_method_id == 3) {
                            return true;
                        } else {
                            return false;
                        }


                    }
                ],
                'buttons' => [
                    'send-receipt' => function ($url, $model) {
                        $url = \yii\helpers\Url::to(['/payments/send-receipt-email/', 'id' => $model->id, 'mode' => 'receipt']);
                        return Html::a('<i class="fa fa-envelope"></i>', $url, [
                            'title' => Yii::t('app', 'Send Email'),
                            'data-toggle' => 'tooltip',
                            'class' => 'btn btn-info btn-xs',
                        ]);

                    },
                    'cheques' => function ($url, $model) {
                        $url = \yii\helpers\Url::to(['/payments/view-cheques/', 'id' => $model->id]);
                        return Html::a('<i class="fa fa-bank"></i>', $url, [
                            'title' => Yii::t('app', 'Cheques Detail'),
                            'data-toggle' => 'tooltip',
                            'class' => 'btn btn-info btn-xs',
                        ]);

                    },
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fa fa-folder"></i>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'data-toggle' => 'tooltip',
                            'class' => 'btn btn-info btn-xs',
                        ]);
                    },
                    'update' => function ($url, $index) {
                        return Html::a('<i class="fa fa-edit"></i>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'data-toggle'=>'tooltip',
                            'class'=>'btn btn-success btn-xs',
                        ]);
                    },
                ],
            ],
        ],

    ]); ?>
</div>
