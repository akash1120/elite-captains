<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\assetfiles\AppAsset;
use app\models\Announcement;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
$this->registerJs('
	$(".sidebar-menu").tree();
');
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode(Yii::$app->params['siteName'] . ' - ' . $this->title) ?></title>
        <base href="<?= Yii::$app->params['siteUrl'] ?>"/>
        <?php $this->head() ?>
    </head>
    <body class="sidebar-mini skin-black-light">
    <?php $this->beginBody() ?>
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="<?= Yii::$app->params['siteUrl'] ?>" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>E</b>B</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><img src="<?= Yii::$app->params['siteUrl'] ?>/images/logo.jpg"
                                           class="img-responsive"/></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?= Yii::$app->params['siteUrl'] . Yii::$app->user->identity->imageSrc ?>"
                                     class="user-image" alt="User Image">
                                <span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?= Yii::$app->params['siteUrl'] . Yii::$app->user->identity->imageSrc ?>"
                                         class="img-circle" alt="User Image">
                                    <p>
                                        <?= Yii::$app->user->identity->username ?>
                                        <small>Member
                                            since <?= Yii::$app->formatter->asDate(Yii::$app->user->identity->created_at, "php:M. Y") ?></small>
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <!--a href="#" class="btn btn-default btn-flat">Profile</a-->
                                    </div>
                                    <div class="pull-right">
                                        <?= Html::a('Sign out', ['site/logout'], ['class' => 'btn btn-default btn-flat', 'data-method' => 'post']) ?>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar">
            <section class="sidebar">
                <ul class="sidebar-menu tree" data-widget="tree">
                    <li class="<?php if (Yii::$app->controller->id == 'site' && Yii::$app->controller->id == 'index') echo 'active'; ?>"><?= Html::a('<i class="fa fa-dashboard"></i> <span>Dashboard</span>', ['site/index']) ?></li>
                    <li class="treeview<?php if (Yii::$app->controller->id == 'booking') echo ' active'; ?>"><a
                                href="javascript::"><i class="fa fa-files-o"></i> <span>Bookings</span></a>
                        <ul class="treeview-menu">
                            <li class="<?php if (Yii::$app->controller->id == 'booking' && Yii::$app->controller->id == 'create') echo 'active'; ?>"><?= Html::a('<i class="fa fa-circle-o"></i> Book a Boat', ['booking/create']) ?></li>
                            <li class="<?php if (Yii::$app->controller->id == 'booking' && Yii::$app->controller->id == 'index') echo 'active'; ?>"><?= Html::a('<i class="fa fa-circle-o"></i> My bookings', ['booking/index']) ?></li>
                            <?php if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) { ?>
                                <li class="<?php if (Yii::$app->controller->id == 'booking' && Yii::$app->controller->id == 'all') echo 'active'; ?>"><?= Html::a('<i class="fa fa-circle-o"></i> All Bookings', ['booking/all', 'list' => 'all']) ?></li>
                            <?php } ?>
                            <li class="<?php if (Yii::$app->controller->id == 'booking' && Yii::$app->controller->id == 'history') echo 'active'; ?>"><?= Html::a('<i class="fa fa-circle-o"></i> Booking History', ['booking/history']) ?></li>
                        </ul>
                    </li>
                    <?php if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) { ?>
                        <li class="treeview<?php if (Yii::$app->controller->id == 'city' || Yii::$app->controller->id == 'marina' || Yii::$app->controller->id == 'boat' || Yii::$app->controller->id == 'time-slot' || Yii::$app->controller->id == 'addon') echo ' active'; ?>">
                            <a href="javascript::"><i class="fa fa-pie-chart"></i> <span>Master</span></a>
                            <ul class="treeview-menu">
                                <li class="<?php if (Yii::$app->controller->id == 'city') echo 'active'; ?>"><?= Html::a('<i class="fa fa-circle-o"></i> Manage Cities', ['city/index']) ?></li>
                                <li class="<?php if (Yii::$app->controller->id == 'marina') echo 'active'; ?>"><?= Html::a('<i class="fa fa-circle-o"></i> Manage Marina', ['marina/index']) ?></li>
                                <li class="<?php if (Yii::$app->controller->id == 'boat') echo 'active'; ?>"><?= Html::a('<i class="fa fa-circle-o"></i> Manage Boats', ['boat/index']) ?></li>
                                <li class="<?php if (Yii::$app->controller->id == 'time-slot') echo 'active'; ?>"><?= Html::a('<i class="fa fa-circle-o"></i> Manage Time Slots', ['time-slot/index']) ?></li>
                                <li class="<?php if (Yii::$app->controller->id == 'terms-and-conditions' && Yii::$app->controller->id == 'index') echo 'active'; ?>"><?= Html::a('<i class="fa fa-circle-o"></i> <span>Terms and Conditions</span>', ['terms-and-conditions/index']) ?></li>
                                <?php if (Yii::$app->user->identity->user_type == 2) { ?>
                                    <li class="<?php if (Yii::$app->controller->id == 'addon') echo 'active'; ?>"><?= Html::a('<i class="fa fa-circle-o"></i> Manage Addons', ['addon/index']) ?></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="<?php if (Yii::$app->controller->id == 'member') echo 'active'; ?>"><?= Html::a('<i class="fa fa-laptop"></i> <span>Members</span>', ['member/index']) ?></li>
                        <li class="treeview<?php if (Yii::$app->controller->id == 'package' || Yii::$app->controller->id == 'staff') echo ' active'; ?>">
                            <a href="javascript::"><i class="fa fa-edit"></i> <span>System</span></a>
                            <ul class="treeview-menu">
                                <li class="<?php if (Yii::$app->controller->id == 'announcement') echo 'active'; ?>"><?= Html::a('<i class="fa fa-circle-o"></i> Manage Announcements', ['announcement/index']) ?></li>
                                <li class="<?php if (Yii::$app->controller->id == 'package') echo 'active'; ?>"><?= Html::a('<i class="fa fa-circle-o"></i> Manage Packages', ['package/index']) ?></li>
                                <li class="<?php if (Yii::$app->controller->id == 'staff') echo 'active'; ?>"><?= Html::a('<i class="fa fa-circle-o"></i> Manage Staff', ['staff/index']) ?></li>
                            </ul>
                        </li>
                        <li class="<?php if (Yii::$app->controller->id == 'payment-terms' && Yii::$app->controller->id == 'index') echo 'active'; ?>"><?= Html::a('<i class="fa fa-list"></i> <span>Payment Terms</span>', ['payment-terms/index']) ?></li>
                        <li class="<?php if (Yii::$app->controller->id == 'payment-methods' && Yii::$app->controller->id == 'index') echo 'active'; ?>"><?= Html::a('<i class="fa fa-credit-card"></i> <span>Payment Methods</span>', ['payment-methods/index']) ?></li>
                        <li class="<?php if (Yii::$app->controller->id == 'addon' && Yii::$app->controller->id == 'index') echo 'active'; ?>"><?= Html::a('<i class="fa fa-plus"></i> <span>Addons</span>', ['addon/index']) ?></li>
<!--                        <li class="<?php /*if (Yii::$app->controller->id == 'contracts' && Yii::$app->controller->id == 'index') echo 'active'; */?>"><?/*= Html::a('<i class="fa fa-file"></i> <span>Contarcts</span>', ['contracts/index']) */?></li>
                        <li class="<?php /*if (Yii::$app->controller->id == 'invoices' && Yii::$app->controller->id == 'index') echo 'active'; */?>"><?/*= Html::a('<i class="fa fa-file"></i> <span>Invoices</span>', ['invoices/index']) */?></li>
                        <li class="<?php /*if (Yii::$app->controller->id == 'payments' && Yii::$app->controller->id == 'index') echo 'active'; */?>"><?/*= Html::a('<i class="fa fa-file"></i> <span>Payments</span>', ['payments/index']) */?></li>-->

                    <?php } ?>
                    <li class="<?php if (Yii::$app->controller->id == 'contracts' && Yii::$app->controller->id == 'index') echo 'active'; ?>"><?= Html::a('<i class="fa fa-file"></i> <span>Contracts</span>', ['contracts/index']) ?></li>
                    <li class="<?php if (Yii::$app->controller->id == 'payments' && Yii::$app->controller->id == 'index') echo 'active'; ?>"><?= Html::a('<i class="fa fa-file"></i> <span>Payments</span>', ['payments/index']) ?></li>
                    <li class="<?php if (Yii::$app->controller->id == 'invoices' && Yii::$app->controller->id == 'index') echo 'active'; ?>"><?= Html::a('<i class="fa fa-file"></i> <span>Invoices</span>', ['invoices/index']) ?></li>
                    <li class="<?php if (Yii::$app->controller->id == 'freeze' && Yii::$app->controller->id == 'index') echo 'active'; ?>"><?= Html::a('<i class="fa fa-file"></i> <span>Freeze</span>', ['freeze-contract/index']) ?></li>
                    <li><?= Html::a('<i class="fa fa-sign-out"></i> <span>Logout</span>', ['site/logout'], ['data-method' => 'post']) ?></li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>
        <div class="content-wrapper">
            <?php
            $announcements = Announcement::find()->where(['and', ['<=', 'DATE(start_date)', date("Y-m-d")], ['>=', 'DATE(end_date)', date("Y-m-d")], ['trashed' => 0]])->asArray()->all();
            if ($announcements != null) {
                foreach ($announcements as $announcement) {
                    echo '
			<div style="padding: 15px;margin: 0;padding-bottom: 0;">
				<div class="callout callout-' . ($announcement['color_class'] != null && $announcement['color_class'] != '' ? $announcement['color_class'] : 'info') . '" style="margin: 0;">
					<i class="fa fa-bullhorn"></i>&nbsp;&nbsp;' . $announcement['heading'] . '<br />' . nl2br($announcement['descp']) . '
				</div>
			</div>';
                }
            }
            ?>
            <?php
            /* Yii::$app->user->identity->noticeText; */
            ?>
            <section class="content-header">
                <h1><?= $this->title . (isset($this->subHeading) ? '<small>' . $this->subHeading . '</small>' : '') ?></h1>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </section>
            <section class="content">
                <?= Alert::widget() ?>
                <?= $content ?>
            </section>
        </div>
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.4.0
            </div>
            <strong>Copyright &copy; <?= date("Y") . ' ' . Yii::$app->params['siteName'] ?>.</strong> All rights
            reserved.
        </footer>
    </div>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>
<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true, 'tabindex' => false],
    'options' => ['tabindex' => false]
]);
echo "<div id='modalContent'><div style='text-align:center'><img src='" . Yii::getAlias('@web') . '/loader_gif.gif' . "'></div></div>";
yii\bootstrap\Modal::end();
?>