<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Boat */

$this->title = Yii::t('app', 'Update Boat: {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Boats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="boat-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
