<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use app\models\Booking;
use app\models\Addon;
use app\helpers\HelperFunction;
use app\models\TermsAndConditions;

$booking = new Booking();
$booking->city_id = $city_id;
$booking->marina_id = $marina_id;
$booking->boat_id = $model->id;
$booking->booking_date = $booking_date;
$terms_and_conditions = TermsAndConditions::find()->where(['id' => 1])->asArray()->one();
$terms = $terms_and_conditions['terms'];

/* @var $this yii\web\View */
/* @var $model app\models\Boat */

$this->registerJs('
	$("body").on("beforeSubmit", "form#form-book-' . $model->id . '", function () {
		_targetContainer="#form-book-' . $model->id . '";
		App.blockUI({
			message: "' . Yii::t('app', 'Please wait...') . '",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});
		 
		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
			  App.unblockUI($(_targetContainer));
			  return false;
		 }
		 var formData = new FormData(form[0]);
		 // submit form
		 $.ajax({
			  url: form.attr("action"),
			  type: "post",
			  data: formData,
			  success: function (data) {
				  if(data=="done"){
					  window.location.href="' . Url::to(['booking/index']) . '";
				  }else{
					  swal("' . Yii::t('app', 'Error') . '",data,"error");
				  }
				  App.unblockUI($(_targetContainer));
			  },
			  cache: false,
			  contentType: false,
			  processData: false
		 });
		 return false;
	});
');
?>
<?php $form = ActiveForm::begin(['id' => 'form-book-' . $model->id]); ?>
    <div class="box box-solid">
        <div class="box-body">
            <div class="image"><img src="<?= $model->imageSrc ?>" class="img-responsive"/></div>
            <h3><?= $model->title ?></h3>
            <?php if ($model->timeSlots != null) { ?>
                <div class="timings">
                    <?php
                    foreach ($model->timeSlots as $timeSlot) {
                        $disabled = "";
                        $checkAlready = Booking::find()->where(['city_id' => $booking->city_id, 'marina_id' => $booking->marina_id, 'boat_id' => $booking->boat_id, 'booking_date' => $booking->booking_date, 'time_slot_id' => $timeSlot->time_slot_id, 'trashed' => 0]);
                        if ($checkAlready->exists()) {
                            $disabled = ' disabled="disabled"';
                        }
                        ?>
                        <div>
                            <label<?= $disabled != '' ? ' class="line-through"' : '' ?>
                                    for="booking-time_slot_id-<?= $model->id . '-' . $timeSlot->time_slot_id ?>">
                                <input type="radio"
                                       id="booking-time_slot_id-<?= $model->id . '-' . $timeSlot->time_slot_id ?>"
                                       name="Booking[time_slot_id]"
                                       value="<?= $timeSlot->time_slot_id ?>"<?= $disabled ?> />
                                <?= $timeSlot->timeSlot->title ?>
                            </label>
                        </div>
                        <?php
                    }
                    ?>

                    <!--<hr style=" margin-top: 10px; margin-bottom: 10px">
                    <div>
                        <label for="booking-captain_id">
                            <input type="checkbox" id="booking-time_capgtain_id" name="Booking[captain]"
                                   value="1" <?/*= $captain_disable */?> />
                            Select Captain
                        </label>
                        <?php /*if ($captain_disable != '') { */?>
                            <a id="add_captain_aadon" style="cursor: pointer" data-toggle="modal" data-target="#captainModal"  class="pull-right">Add Captain</a>
                        <?php /*} */?>
                    </div>-->

                    <hr style=" margin-top: 10px; margin-bottom: 10px">
                    <div>
                        <label for="booking-captain_id">
                            <input type="checkbox" id="booking-time_capgtain_id" name="Booking[food]"
                                   value="2"  <?= $food_disable ?> />
                            Select Food (Lunch/Dinner)
                        </label>
                        <?php if ($food_disable != '') { ?>
                            <a id="add_food_aadon" style="cursor: pointer"  data-toggle="modal" data-target="#foodModal" class="pull-right">Add Food</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="hidden">
                    <?= $form->field($booking, 'city_id')->hiddenInput()->label(false) ?>
                    <?= $form->field($booking, 'marina_id')->hiddenInput()->label(false) ?>
                    <?= $form->field($booking, 'boat_id')->hiddenInput()->label(false) ?>
                    <?= $form->field($booking, 'booking_date')->hiddenInput()->label(false) ?>
                </div>
                <?php if ($model->addons != null) { ?>
                    <a href="#<?= 'addon-modal-' . $model->id ?>" class="btn btn-success" data-toggle="modal">Select
                        Addons</a>
                <?php } ?>
                <button class="btn btn-success">Book Selected Time</button>
            <?php } ?>
        </div>
    </div>

<?php
Modal::begin([
    'headerOptions' => ['class' => 'modal-header clearfix'],
    'id' => 'addon-modal-' . $model->id,
    'size' => 'modal-md',
    'header' => '<h4>' . Yii::t('app', 'Select Addons') . '</h4>',
]);
echo "<div class='modalContent'>" . $this->render('/boat/_addons', ['model' => $model,]) . "</div>";
Modal::end();
?>
<?php ActiveForm::end(); ?>

<div id="captainModal" class="modal fade" role="dialog">


    <?php
    $captain =  Addon::find()->where(['id' => 1])->one();
    $vat = HelperFunction::calculate_vat($captain->price);
    ?>

    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">


                <section class="MainArea" style="background:#fff;font-family: 'Century Gothic'">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                                <div class="col-3 col-sm-3 col-md-3 col-lg-8 col-xl-8" style="width: 60%; float: left;">
                                    <a href="/"><img src="<?= Yii::$app->params['siteUrl']?>/images/logo.jpg" alt="Elite-Captain"></a>
                                </div>

                                <div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-6" style="width: 60%; float: left;">
                                    <br/><br/>
                                    <strong><br/><?= "Elite Captain"; ?></strong>
                                    <strong><br/>Telephone: <?= (Yii::$app->params['app_phone'] <> null) ? Yii::$app->params['app_phone'] : "-"; ?><br/></strong>
                                    <strong>Email: <?= (Yii::$app->params['adminEmail'] <> null) ? Yii::$app->params['adminEmail'] : "-"; ?><br/></strong>
                                    <strong>VAT Registration no:
                                        <u><?= (Yii::$app->params['app_vat'] <> null) ? Yii::$app->params['app_vat'] : "-"; ?></u><br/></strong>
                                </div>

                                <div class="col-3 col-sm-3 col-md-3 col-lg-8 col-xl-8" style="width: 100%; float:left;">
                                    <br/><br/>
                                    <strong>Bill To:</strong><br/>
                                    <strong>Name: <?= ($member->first_name <> null) ? $member->first_name . ' ' . $member->last_name : $member->username; ?>
                                    </strong>
                                    <br/>
                                    <strong>Address: <?= ($member <> null) ? $member->address : "-"; ?><br/></strong>
                                    <strong>Telephone: <?= ($member <> null) ? $member->mobile : "-"; ?></strong>
                                    <br/>
                                    <strong>VAT Registration no:
                                        <u>   <?= ($member <> null) ? $member->vat_number : "-";  ?></u><br/><br/>
                                    </strong>
                                </div>

                                <div class="col-6 col-sm-6 col-md-6 col-xl-12">
                                    <div style="width: 100%;">
                                        <table class="table" cellpadding="5">
                                            <thead>
                                            <tr>
                                                <th class="col-md-8"
                                                    style=" background: #1d355f; color: #ffffff; text-align: center;">Addon
                                                </th>
                                                <th class="col-md-4"
                                                    style=" background: #1d355f; color: #ffffff; text-align: center">Amount
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                    <tr>
                                                        <td>
                                                            <h3> Captain for boat </h3>
                                                        </td>
                                                        <td style="text-align: center;">AED <?= $captain->price; ?></td>
                                                    </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div style="clear:both;border-top: #000 solid 1px;margin-top: 30px;">
                                        <table class="table" style=" float: left;" cellpadding="5">
                                            <tbody>

                                            <tr>
                                                <td style="width:70%;text-align: left;">
                                                    <strong>Subtotal</strong>
                                                    <br/><strong>VAT(5%)</strong>
                                                </td>
                                                <td style="width:30%;text-align: center;">
                                                    <strong><?= ($captain <> null) ? $captain->price : 0; ?></strong>
                                                    <br/><strong><?= ($captain->price <> null) ? $vat : 0; ?></strong>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="width:70%;text-align: left;">
                                                    <br/><strong>Total</strong>
                                                </td>
                                                <td style="width:30%;text-align: center;">
                                                    <br/>
                                                    <strong>
                                                        AED <?= ($captain <> null) ? $captain->price  + $vat : 0; ?>
                                                        <input type="hidden"
                                                               value="<?= ($captain <> null) ? $captain->price  + $vat : 0; ?>"
                                                               id="actual_total"/>
                                                    </strong>
                                                </td>
                                            </tr>


                                            <hr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-3 col-sm-3 col-md-3 col-lg-12 col-xl-12">
                                    <?php
                                    /*if ($invoice <> null) {
                                        if ($invoice->status == 'unpaid') {
                                            echo \yii\helpers\Html::a('Pay Online', [
                                                \Yii::$app->params['appUrl'] . "/site/invoice-payment", 'invoice_id' => $invoice->invoice_id
                                            ]);
                                            echo '<br/>';
                                        }
                                    }*/
                                    ?>
                                </div>
                            </div>
                            <div class=" col-sm-12 col-md-12 col-xl-12" style="clear:both;width: 30%;margin-top: 30px;">

                                <strong>Terms & Conditions</strong>
                                <p><?= $terms; ?></p>
                            </div>
                        </div>
                    </div>
                </section>





            </div>
            <div class="modal-footer">

             <?php   $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action' => Yii::$app->urlManager->createUrl(['booking/createinvoice'])]); ?>

                <input type="hidden"  class="form-control" name="Addon[contract_id]" value="<?= (isset($contract->id) && ($contract->id <> null))? $contract->id : '' ?>" >
                <input type="hidden"  class="form-control" name="Addon[user_id]" value="<?= (isset($member->id) && ($member->id <> null))? $member->id : '' ?>" >
                <input type="hidden"  class="form-control" name="Addon[total]" value="<?= (isset($captain->price) && ($captain->price <> null))? $captain->price : '' ?>" >
                <input type="hidden"  class="form-control" name="Addon[addon_id]" value="<?= (isset($captain->id) && ($captain->id <> null))? $captain->id : '' ?>" >
                <input type="hidden"  class="form-control" name="Addon[tax]" value="<?= (isset($vat) && ($vat <> null))? $vat : '' ?>" >
                <?= Html::submitButton('Generate Invoice', ['class' => 'btn btn-success']) ?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <?php $form::end(); ?>

            </div>
        </div>

    </div>
</div>


<div id="foodModal" class="modal fade" role="dialog">


    <?php
    $captain =  Addon::find()->where(['id' => 2])->one();
    $vat = HelperFunction::calculate_vat($captain->price);
    ?>

    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">


                <section class="MainArea" style="background:#fff;font-family: 'Century Gothic'">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                                <div class="col-3 col-sm-3 col-md-3 col-lg-8 col-xl-8" style="width: 60%; float: left;">
                                    <a href="/"><img src="<?= Yii::$app->params['siteUrl']?>/images/logo.jpg" alt="Elite-Captain"></a>
                                </div>

                                <div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-6" style="width: 60%; float: left;">
                                    <br/><br/>
                                    <strong><br/><?= "Elite Captain"; ?></strong>
                                    <strong><br/>Telephone: <?= (Yii::$app->params['app_phone'] <> null) ? Yii::$app->params['app_phone'] : "-"; ?><br/></strong>
                                    <strong>Email: <?= (Yii::$app->params['adminEmail'] <> null) ? Yii::$app->params['adminEmail'] : "-"; ?><br/></strong>
                                    <strong>VAT Registration no:
                                        <u><?= (Yii::$app->params['app_vat'] <> null) ? Yii::$app->params['app_vat'] : "-"; ?></u><br/></strong>
                                </div>

                                <div class="col-3 col-sm-3 col-md-3 col-lg-8 col-xl-8" style="width: 100%; float:left;">
                                    <br/><br/>
                                    <strong>Bill To:</strong><br/>
                                    <strong>Name: <?= ($member->first_name <> null) ? $member->first_name . ' ' . $member->last_name : $member->username; ?>
                                    </strong>
                                    <br/>
                                    <strong>Address: <?= ($member <> null) ? $member->address : "-"; ?><br/></strong>
                                    <strong>Telephone: <?= ($member <> null) ? $member->mobile : "-"; ?></strong>
                                    <br/>
                                    <strong>VAT Registration no:
                                        <u>   <?= ($member <> null) ? $member->vat_number : "-";  ?></u><br/><br/>
                                    </strong>
                                </div>

                                <div class="col-6 col-sm-6 col-md-6 col-xl-12">
                                    <div style="width: 100%;">
                                        <table class="table" cellpadding="5">
                                            <thead>
                                            <tr>
                                                <th class="col-md-8"
                                                    style=" background: #1d355f; color: #ffffff; text-align: center;">Addon
                                                </th>
                                                <th class="col-md-4"
                                                    style=" background: #1d355f; color: #ffffff; text-align: center">Amount
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <p> <?= $captain->title; ?> for boat </p>
                                                </td>
                                                <td style="text-align: center;">AED <?= $captain->price; ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div style="clear:both;margin-top: 30px;">
                                        <table class="table" style=" float: left;" cellpadding="5">
                                            <tbody>

                                            <tr>
                                                <td style="width:70%;text-align: left;">
                                                    <strong>Subtotal</strong>
                                                    <br/><strong>VAT(5%)</strong>
                                                </td>
                                                <td style="width:30%;text-align: center;">
                                                    <strong><?= ($captain <> null) ? $captain->price : 0; ?></strong>
                                                    <br/><strong><?= ($captain->price <> null) ? $vat : 0; ?></strong>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="width:70%;text-align: left;">
                                                    <br/><strong>Total</strong>
                                                </td>
                                                <td style="width:30%;text-align: center;">
                                                    <br/>
                                                    <strong>
                                                        AED <?= ($captain <> null) ? $captain->price  + $vat : 0; ?>
                                                        <input type="hidden"
                                                               value="<?= ($captain <> null) ? $captain->price  + $vat : 0; ?>"
                                                               id="actual_total"/>
                                                    </strong>
                                                </td>
                                            </tr>



                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <!--<div class="col-3 col-sm-3 col-md-3 col-lg-12 col-xl-12">
                                    <?php
/*                                    if ($invoice <> null) {
                                        if ($invoice->status == 'unpaid') {
                                            echo \yii\helpers\Html::a('Pay Online', [
                                                \Yii::$app->params['appUrl'] . "/site/invoice-payment", 'invoice_id' => $invoice->invoice_id
                                            ]);
                                            echo '<br/>';
                                        }
                                    }
                                    */?>
                                </div>-->
                            </div>
                            <div class=" col-sm-12 col-md-12 col-xl-12" style="clear:both;width: 30%;margin-top: 30px;">

                                <strong>Terms & Conditions</strong>
                                <p><?= $terms; ?></p>
                            </div>
                        </div>
                    </div>
                </section>





            </div>
            <div class="modal-footer">

                <?php   $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action' => Yii::$app->urlManager->createUrl(['booking/createinvoice'])]); ?>

                <input type="hidden"  class="form-control" name="Addon[contract_id]" value="<?= (isset($contract->id) && ($contract->id <> null))? $contract->id : '' ?>" >
                <input type="hidden"  class="form-control" name="Addon[user_id]" value="<?= (isset($member->id) && ($member->id <> null))? $member->id : '' ?>" >
                <input type="hidden"  class="form-control" name="Addon[total]" value="<?= (isset($captain->price) && ($captain->price <> null))? $captain->price : '' ?>" >
                <input type="hidden"  class="form-control" name="Addon[addon_id]" value="<?= (isset($captain->id) && ($captain->id <> null))? $captain->id : '' ?>" >
                <input type="hidden"  class="form-control" name="Addon[tax]" value="<?= (isset($vat) && ($vat <> null))? $vat : '' ?>" >

                <?= Html::submitButton('Generate Invoice', ['class' => 'btn btn-success']) ?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <?php $form::end(); ?>

            </div>
        </div>

    </div>
</div>



<style>
    .searchResult .view a {
        font-weight: bold;
    }

    #events, #news {
        background: #ffffff;
    }

    .searchResult .view {
        padding: 5px 5px 10px;
        margin-bottom: 10px;
        border-bottom: dashed 1px #ccc;
        -webkit-transition: background 0.2s;
        transition: background 0.2s;
    }
</style>