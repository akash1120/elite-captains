<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Booking;

$booking = new Booking();
$booking->city_id = $city_id;
$booking->marina_id = $marina_id;
$booking->booking_date = $booking_date;

/* @var $this yii\web\View */
/* @var $model app\models\Boat */

$this->registerJs('
	$("body").on("beforeSubmit", "form#form-book-'.$model->id.'", function () {
		_targetContainer="#form-book-'.$model->id.'";
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});
		 
		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
			  App.unblockUI($(_targetContainer));
			  return false;
		 }
		 var formData = new FormData(form[0]);
		 // submit form
		 $.ajax({
			  url: form.attr("action"),
			  type: "post",
			  data: formData,
			  success: function (response) {
				  App.unblockUI($(_targetContainer));
				  if(response=="done"){
					  swal("'.Yii::t('app','Succes').'","Booking completed successfully","success");
					  
				  }
			  },
			  cache: false,
			  contentType: false,
			  processData: false
		 });
		 return false;
	});
');
?>
<?php $form = ActiveForm::begin(['id'=>'form-book-'.$model->id]); ?>
<div class="box box-solid">
    <div class="box-body">
      <div class="image"><img src="<?= $model->imageSrc?>" class="img-responsive" /></div>
      <h3><?= $model->title?></h3>
      <?php if($model->timeSlots!=null){?>
      <div class="timings">
      	<?= $form->field($booking, 'time_slot_id')
			->radioList(
				ArrayHelper::map($model->timeSlots,'time_slot_id','timeSlot.title'),
				[
					'item' => function($index, $label, $name, $checked, $value) {
						$disabled='';

						$return = '<label for="modal-radio">';
						$return .= '	<input type="radio" id="btrb-'.$index.'" name="'.$name.'" value="'.$value.'"'.$disabled.' /> ';
						$return .= '	<span>'.ucwords($label).'</span>';
						$return .= '</label>';
						return $return;
					}
				]
			)
		->label(false);
		?>
      </div>
      <div class="hidden">
      	<?= $form->field($booking, 'city_id')->hiddenInput()->label(false) ?>
      	<?= $form->field($booking, 'marina_id')->hiddenInput()->label(false) ?>
      	<?= $form->field($booking, 'booking_date')->hiddenInput()->label(false) ?>
      </div>
      <button class="btn btn-success">Book Selected Time</button>
      <?php }?>
    </div>
</div>
<?php ActiveForm::end(); ?>