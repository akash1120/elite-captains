<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Boat */

$this->title = Yii::t('app', 'Create Boat');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Boats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="boat-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
