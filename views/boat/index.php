<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\widgets\CustomGridView;
use app\models\City;
use app\models\Marina;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BoatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Boats');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="boat-index">

    <?= CustomGridView::widget([
		'create'=>true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:10px;'],],
			['format' => ['image',['width'=>'100','height'=>'100']], 'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],'attribute'=>'imageSrc'],
			'title',
            ['attribute'=>'city_id','headerOptions'=>['class'=>'noprint','style'=>'width:80px;'],'value'=>function($model){return $model->city->title;},'filter'=>ArrayHelper::map(City::find()->where(['status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title')],
            ['attribute'=>'marina_id','headerOptions'=>['class'=>'noprint','style'=>'width:110px;'],'value'=>function($model){return $model->marina->title;},'filter'=>ArrayHelper::map(Marina::find()->where(['status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title')],
            ['format'=>'html', 'attribute'=>'time_slot_idz','value'=>function($model){return $model->timeSlotsText;}],
            ['format'=>'html', 'attribute'=>'addon_idz','value'=>function($model){return $model->addonsText;}],
            ['attribute'=>'rank','headerOptions'=>['class'=>'noprint','style'=>'width:50px;']],
            ['format'=>'html','attribute'=>'status','headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],'value'=>function($model){return $model->txtStatus;},'filter'=>Yii::$app->params['statusArr']],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>Yii::t('app','Actions'),
                'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],
                'contentOptions'=>['class'=>'noprint text-right'],
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fa fa-folder"></i>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'data-toggle'=>'tooltip',
                            'class'=>'btn btn-info btn-xs',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-edit"></i>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'data-toggle'=>'tooltip',
                            'class'=>'btn btn-success btn-xs',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-remove"></i>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'data-toggle'=>'tooltip',
                            'data-method'=>'post',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this?'),
                            'class'=>'btn btn-danger btn-xs',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
