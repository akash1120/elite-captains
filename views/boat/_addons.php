<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
?>
<?php if($model->addons!=null){?>
<div class="row">
	<?php
	$cols=12/count($model->addons);
	foreach($model->addons as $addon){
		$txtPrice='';
	?>
    <div class="col-xs-12 col-sm-<?= $cols?>">
    	<div class="box box-success">
        	<div class="box-header">
            	<?= $addon->addon->title?>
            </div>
        	<div class="box-body">
            	<label for="booking-addon_idz-<?= $model->id.'-'.$addon->addon_id?>">
                	<input type="checkbox" id="booking-addon_idz-<?= $model->id.'-'.$addon->addon_id?>" name="Booking[addon_idz][]" value="<?= $addon->addon_id?>" /> <?= $addon->addon->title?>
                </label>
            </div>
    	</div>
    </div>
    <?php
	}
	?>
</div>
<?php }?>