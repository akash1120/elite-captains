<?php
use yii\helpers\ArrayHelper;
use app\models\MarinaToAddon;
use app\models\Addon;
use app\models\TimeSlot;

?>
<?= $form->field($model, 'addon_idz',['template'=>'<h4>{label}</h4><div class="row">{input}</div>{error}{hint}'])->checkboxList(ArrayHelper::map(MarinaToAddon::find()->select(['id'=>Addon::tableName().'.id','addon_id'=>MarinaToAddon::tableName().'.addon_id','title'=>Addon::tableName().'.title'])->joinWith(['addon'])->where(['and',[MarinaToAddon::tableName().'.marina_id'=>$model->marina_id,Addon::tableName().'.status'=>1,Addon::tableName().'.trashed'=>0],['>','available_qty',0]])->orderBy(['title'=>SORT_ASC])->asArray()->all(),'id','title'), [
	'item' => function($index, $label, $name, $checked, $value) {
		$checked = $checked ? 'checked' : '';
		return "<div class=\"col-xs-6 col-sm-4\" style=\"padding-bottom:10px;\"><label style=\"font-weight:normal\"><input type=\"checkbox\" {$checked} name=\"{$name}\" value=\"{$value}\">&nbsp;&nbsp;{$label}</label></div>";
	}
]) ?>
<?= $form->field($model, 'time_slot_idz',['template'=>'<h4>{label}</h4><div class="row">{input}</div>{error}{hint}'])->checkboxList(ArrayHelper::map(TimeSlot::find()->where(['status'=>1, 'marina_id'=>$model->marina_id,'trashed'=>0])->orderBy(['id'=>SORT_ASC])->asArray()->all(),'id','title'), [
	'item' => function($index, $label, $name, $checked, $value) {
		$checked = $checked ? 'checked' : '';
		return "<div class=\"col-xs-6 col-sm-4\" style=\"padding-bottom:10px;\"><label style=\"font-weight:normal\"><input type=\"checkbox\" {$checked} name=\"{$name}\" value=\"{$value}\">&nbsp;&nbsp;{$label}</label></div>";
	}
]) ?>
        