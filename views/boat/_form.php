<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\City;
use app\models\Marina;

/* @var $this yii\web\View */
/* @var $model app\models\Boat */
/* @var $form yii\widgets\ActiveForm */

$defPorts=[];
$jScript='var cities = []';
$cities=City::find()->where(['status'=>1,'trashed'=>0])->asArray()->all();
if($cities!=null){
	foreach($cities as $city){
		$jScript.='
			var city_'.$city['id'].' = [
		';
		$marinas=Marina::find()->where(['city_id'=>$city['id'],'status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all();
		if($marinas!=null){
			foreach($marinas as $marina){
				$jScript.='{display: "'.$marina['title'].'", value: "'.$marina['id'].'", sel: "'.(($model->id!=null && $model->marina_id==$marina['id']) ? 'selected=\"selected\"' : '').'" },';
			}
		}
		$jScript.=']';
	}
}
if($model->city_id){
	$defPorts=ArrayHelper::map(Marina::find()->where(['city_id'=>$model->city_id,'status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title');
}
$this->registerJs($jScript.'
	$("#boat-city_id").change(function() {
		cval=$(this).val();
		$("#boat-marina_id").html("");
		array_list=eval("city_"+cval)
		$("#boat-marina_id").append("<option value=\"\">Select</option>");
		$(array_list).each(function (i) {
			$("#boat-marina_id").append("<option value=\""+array_list[i].value+"\""+(array_list[i].sel!="" ? array_list[i].sel : "")+">"+array_list[i].display+"</option>");
		});
	});
	$("#boat-marina_id").change(function() {
		$.ajax({
			url: "'.Url::to(['boat/marina-options','id'=>'']).'"+$(this).val(),
			dataType: "html",
			success: function(html) {
				$("#marina-options").html(html);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});
');
?>
<div class="boat-form box">
	<?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
    <div class="box-body">
        <div class="row">
        	<div class="col-xs-12 col-sm-6"><?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map($cities,'id','title'),['prompt'=>Yii::t('app','Select')]) ?></div>
        	<div class="col-xs-12 col-sm-6"><?= $form->field($model, 'marina_id')->dropDownList($defPorts) ?></div>
        </div>
        <div class="row">
        	<div class="col-xs-12 col-sm-6"><?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?></div>
        	<div class="col-xs-12 col-sm-6"><?= $form->field($model, 'descp')->textInput(['maxlength' => true]) ?></div>
        </div>
        <div class="row">
        	<div class="col-xs-12 col-sm-6"><?= $form->field($model, 'rank')->textInput() ?></div>
        	<div class="col-xs-12 col-sm-6"><?= $form->field($model, 'status')->dropDownList(Yii::$app->params['statusArr']) ?></div>
        </div>
		<?php if($model->imageSrc!=null && $model->imageSrc!=Yii::$app->params['default_image']){?>
        <div class="form-group">
            <label class="control-label"><?= Yii::t('app','Old Image')?></label>
            <div><img src="<?= $model->imageSrc?>" width="150" /></div>
        </div>
        <?php }?>
        <?= $form->field($model, 'image')->fileInput() ?>
        <div id="marina-options">
		<?= $this->render('_form_marina_options',['form'=>$form,'model'=>$model,'marina_id'=>$model->marina_id]);?>
		</div>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app','Cancel'),['index'],['class'=>'btn btn-default'])?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
