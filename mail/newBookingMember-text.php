<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $product app\models\Booking */
?>
Hello <?= $model->user->username?>,

This is the confirmation for your booking made on <?= Yii::$app->params['siteName']?>.

Below are the booking detail:

City: <?= $model->city->title?>
Marina: <?= $model->marina->title?>
Boat: <?= $model->boat->title?>
Date: <?= Yii::$app->formatter->asDate($model->booking_date)?>
Time: <?= $model->timeSlot->title?>