<?php
use app\models\Invoices;
use app\models\Package;

$member = null;
$invoice = null;

if ($payment <> null) {
    $invoice = Invoices::findOne($payment->invoice_id);
    $member = ($invoice <> null && $invoice->user <> null) ? $invoice->user : null;
    $contract = ($invoice <> null && $invoice->contract <> null) ? $invoice->contract : null;
}
?>
<div class="events-create  card card-box">
    <section class="MainArea" style="font-family: 'Century Gothic'">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                    <div class="col-3 col-sm-3 col-md-3 col-lg-8 col-xl-8" style="width: 60%; float: left;">
                        <a href="/"><img src="<?= Yii::$app->params['siteUrl'] ?>/images/logo.jpg" alt="Elite-Captain"></a>
                    </div>

                    <div class="col-3 col-sm-3 col-md-4 col-lg-4 col-xl-4" style="width: 35%; float: right;">
                        <h1 style="color: #000;"><b><?= 'Receipt'; ?></b></h1>
                        <strong><span>Date: <?= ($payment <> null) ? formatDate($payment->date) : ""; ?></span></strong><br/>
                        <strong><span>Reference #: <?= ($payment <> null) ? \yii\helpers\Html::a(str_pad($payment->id, 7, "0", STR_PAD_LEFT), '#', []) : 0; ?></span></strong>
                    </div>

                    <div class="col-3 col-sm-3 col-md-3 col-lg-8 col-xl-8" style="width: 100%; float: left;">
                        <br/><br/>
                        <strong><br/><?= "Elite Captain"; ?></strong>
                        <strong><br/>Telephone:<?= (Yii::$app->params['app_phone'] <> null) ? Yii::$app->params['app_phone'] : "-"; ?><br/></strong>
                        <strong>Email:<?= (Yii::$app->params['adminEmail'] <> null) ? Yii::$app->params['adminEmail'] : "-"; ?> <br/></strong>
                        <strong>VAT Registration no:
                            <u> <?= (Yii::$app->params['app_vat'] <> null) ? Yii::$app->params['app_vat'] : "-"; ?></u><br/></strong>
                    </div>

                    <div class="col-3 col-sm-3 col-md-3 col-lg-8 col-xl-8" style="width: 100%; float:left;">
                        <br/><br/>
                        <strong>Bill To:</strong><br/>
                        <strong>Name: <?= ($member->first_name <> null) ? $member->first_name . ' ' . $member->last_name : $member->username; ?>
                        </strong>
                        <br/>
                        <!--  <strong>Company: <? /*= ($member <> null ) ? ($member->accountCompany <> null) ? $member->accountCompany->name : "" : ""; */ ?></strong>
                        <br/>-->
                        <strong>Address: <?= ($member <> null) ? $member->address : "-"; ?><br/></strong>
                        <!--<strong>P.O
                            Box: <? /*= ($member <> null && $member->accountCompany <> null) ? $member->accountCompany->postal_code : $payment->shipping_postalcode; */ ?>
                            <br/></strong>-->
                        <strong>Telephone: <?= ($member <> null) ? $member->mobile : "-"; ?></strong>
                        <br/>
                          <strong>VAT Registration no:
                            <u>   <?= ($member <> null) ? $member->vat_number : "-";  ?></u><br/><br/>
                        </strong>
                    </div>

                    <div style="width: 100%;" class="col-3 col-sm-3 col-md-3 col-lg-12 col-xl-12">
                        <div style="width: 100%;min-height: 100px;">
                            <table class="table" style="width: 100%; float: left;" cellpadding="5">
                                <thead>
                                <tr>
                                    <th class="col-md-4"
                                        style="width:25%; background: #1d355f; color: #ffffff; text-align: left;">
                                        Invoice #
                                    </th>
                                    <th class="col-md-4"
                                        style="width:25%; background: #1d355f; color: #ffffff; text-align: left;">
                                        Description
                                    </th>
                                    <th class="col-md-4"
                                        style="width:25%; background: #1d355f; color: #ffffff; text-align: center;">
                                        Payable Amount
                                    </th>
                                    <th class="col-md-4"
                                        style="width:25%; background: #1d355f; color: #ffffff; text-align: center">
                                        Amount
                                        Paid
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if ($invoice <> null) {

                                    ?>
                                    <tr>
                                        <td>
                                            <?= ($invoice <> null) ? \yii\helpers\Html::a(str_pad($invoice->invoice_id, 7, "0", STR_PAD_LEFT), '#', []) : 0; ?>
                                        </td>
                                        <td>
                                            <?php

                                            if ($contract <> null) {
                                                if ($contract <> null) {

                                                    $package = Package::find()->where(['id' => $contract['package_id']])->one();
                                                    if ($package <> null) {
                                                        $_package = $package->title;
                                                    }
                                                    ?>
                                                    <?= ($_package <> null) ? $_package : ""; ?>
                                                    <br/> <?= '( From:  ' . formatDate($contract['start_date']) . ' to ' . formatDate($contract['end_date']).')'; ?>
                                                    <?php
                                                }
                                            }
                                            ?></td>
                                        <td style="text-align: center;">
                                            AED <?= ($invoice <> null) ? $invoice->total : 0; ?></td>
                                        <td style="text-align: center;">
                                            AED <?= ($payment <> null) ? $payment->amount : 0; ?></td>
                                    </tr>

                                    <?php if(isset($payment->discount) && ($payment->discount <> null) && ($payment->discount !=0)){ ?>
                                        <tr>
                                            <td>Discount</td>
                                            <td></td>
                                            <td></td>
                                            <td>AED <?= $payment->discount; ?></td>
                                        </tr>
                                        <?php } ?>

                                    <?php
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div style="text-align:center; width: 100%; margin-top: 50px; clear: both;">
                        <span style="color: red; font-size: 10px;"><i>This is system generated receipt</i></span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div style="text-align:center; width: 100%; margin-top: 50px; clear: both;">
        <?= "Elite Captain"; ?>
    </div>
</div>
<style>
    .searchResult .view a {
        font-weight: bold;
    }

    #events, #news {
        background: #ffffff;
    }

    .searchResult .view {
        padding: 5px 5px 10px;
        margin-bottom: 10px;
        border-bottom: dashed 1px #ccc;
        -webkit-transition: background 0.2s;
        transition: background 0.2s;
    }
</style>