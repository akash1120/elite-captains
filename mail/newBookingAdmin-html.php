<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $product app\models\Booking */
?>
<table width="560" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td style="color: #9a9a9a; font-family: Helvetica, Arial, sans-serif, 'Raleway'; font-weight: 600; vertical-align: top; font-size: 16px; text-align: left; line-height: 24px; text-transform: uppercase;">Hello Admin,</td>
    </tr>
    <tr>
        <td height="30" width="100%" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
    </tr>
    <tr>
        <td width="100%" style="color: #939393; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; line-height: 24px; font-weight: 400; vertical-align: top; font-size: 16px; text-align: left;">
        	A new booking is made on <?= Yii::$app->params['siteName']?>.<br /><br />
            Below are the booking detail:<br /><br />
			<strong>Member:</strong> <?= $model->user->username?><br />
            <strong>City:</strong> <?= $model->city->title?><br />
            <strong>Marina:</strong> <?= $model->marina->title?><br />
            <strong>Boat:</strong> <?= $model->boat->title?><br />
            <strong>Date:</strong> <?= Yii::$app->formatter->asDate($model->booking_date)?><br />
            <strong>Time:</strong> <?= $model->timeSlot->title?>
        </td>
    </tr>
</table>