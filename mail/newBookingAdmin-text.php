<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $product app\models\Booking */
?>
Hello Admin,

A new booking is made on <?= Yii::$app->params['siteName']?>.

Below are the booking detail:

Member: <?= $model->user->username?>
City: <?= $model->city->title?>
Marina: <?= $model->marina->title?>
Boat: <?= $model->boat->title?>
Date: <?= Yii::$app->formatter->asDate($model->booking_date)?>
Time: <?= $model->timeSlot->title?>