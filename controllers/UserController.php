<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\UserSearch;
use app\models\UserNote;
use app\models\UserNoteSearch;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends DefaultController
{
    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		$this->checkAdmin();
		$modelNote = new UserNote();
		$modelNote->user_id=$id;

        if ($modelNote->load(Yii::$app->request->post())) {
        	if($modelNote->save()){
            	Yii::$app->getSession()->setFlash('success', Yii::t('app','Note saved successfully'));
                return $this->redirect(['view','id'=>$id]);
            } else {
				if($modelNote->hasErrors()){
					foreach($modelNote->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								Yii::$app->getSession()->addFlash('error', $val);
							}
						}
					}
				}
            }
        }
		
        $searchModel = new UserNoteSearch();
		$searchModel->user_id=$id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'modelNote' => $modelNote,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		$this->checkAdmin();
        $this->findModel($id)->softDelete();
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Record deleted successfully'));
        return $this->redirect(Yii::$app->request->referrer ?: ['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
