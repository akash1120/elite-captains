<?php

namespace app\controllers;

use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\BookingForm;
use app\models\Booking;
use app\models\BookingSearch;
use app\models\BoatSearch;
use app\models\Contracts;
use app\models\ContarctAddons;
use app\models\Addon;
use app\models\Invoices;
use app\models\InvoiceNumbers;
use app\helpers\HelperFunction;

use yii\web\NotFoundHttpException;

/**
 * BookingController implements the CRUD actions for Booking model.
 */
class BookingController extends DefaultController
{
    /**
     *
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','all','history','create','load-boats','delete','createinvoice'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
				'denyCallback' => function ($rule, $action) {
					if(Yii::$app->request->isAjax){
						throw new \yii\web\HttpException(401, Yii::t('app','Please login.'));
					}else{
						return $this->redirect(['site/login']);
					}
				}
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Active Booking models.
     * @return mixed
     */
    public function actionIndex()
    {
		$this->checkLogin();
        $searchModel = new BookingSearch();
        $dataProvider = $searchModel->searchActive(Yii::$app->request->queryParams);
		$this->executeBulkAction();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Booking models.
     * @return mixed
     */
    public function actionAll($list='active')
    {
		$this->checkAdmin();
        $searchModel = new BookingSearch();
        $searchModel->listType=$list;
        $dataProvider = $searchModel->searchAll(Yii::$app->request->queryParams);
		$this->executeBulkAction();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
    /**
     * Run bulk action for bookings
     */
	public function executeBulkAction()
	{
		$action=Yii::$app->request->post('action');
		if($action!=null){
			if($action=="bdelete"){
				$selection=Yii::$app->request->post('selection');
				if($selection!=null){
					$connection = \Yii::$app->db;
					$connection->createCommand("update ".Booking::tableName()." set trashed='1',trashed_by='".Yii::$app->user->identity->id."',trashed_at='".date("Y-m-d H:i:s")."' where id in (".$selection.")")->execute();
        			Yii::$app->getSession()->addFlash('success', Yii::t('app','Selected bookings deleted successfully'));
					//return $this->redirect(Yii::$app->request->referrer ?: ['index'])->send();
				}
			}
		}
	}

    /**
     * Lists all Booking models.
     * @return mixed
     */
    public function actionHistory()
    {
		$this->checkLogin();
        $searchModel = new BookingSearch();
        $dataProvider = $searchModel->searchHistory(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Booking model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($city_id=null,$marina_id=null,$date=null)
    {
		$this->checkLogin();

		/*get current contract start*/
        $used_captain =  array();
        $used_food = array();
        $saved_addons =  array();
        $current_user = Yii::$app->user->identity->id ;
       /* echo $current_user;*/
        $member =User::find()->where(['id' => $current_user])->one();

        $current_contract = Contracts::find()->where([
            'user_id' => $current_user,
            'status' => 'active'
        ])->orderBy(['id' => SORT_DESC])->one();
      /*  echo "<pre>";
        print_r($current_contract);
        die;*/
        if($current_contract <>  null) {
            $used_captain =  Booking::find()->where(['contract_id' => $current_contract->id,'captain' => 1,'trashed' => 0])->asArray()->all();
            $used_food =  Booking::find()->where(['contract_id' => $current_contract->id,'food' => 2,'trashed' => 0])->asArray()->all();
            $used_donuts =  Booking::find()->where(['contract_id' => $current_contract->id,'donut' => 4,'trashed' => 0])->asArray()->all();

            $saved_addons = ContarctAddons::find()->where(['contract_id' => $current_contract->id])->asArray()->all();
            foreach ($saved_addons as $key =>  $addon){

                $saved_addons[$key]['addon_title'] = Addon::find()->where(['id' => $addon['addon_id']])->orderBy(['title' => SORT_ASC])->one()->title;
                $saved_addons[$key]['addon_price'] = Addon::find()->where(['id' => $addon['addon_id']])->orderBy(['title' => SORT_ASC])->one()->price;
                if($addon['addon_id'] == 1){
                    $saved_addons[$key]['addon_used'] = count($used_captain);
                }
                if($addon['addon_id'] == 2){
                    $saved_addons[$key]['addon_used'] = count($used_food);
                }
                if($addon['addon_id'] == 4){
                    $saved_addons[$key]['addon_used'] = count($used_donuts);
                }
            }
        }
        /*get current contract end*/

        $modelForm = new BookingForm();
		$model = new Booking();
		if($model->load(Yii::$app->request->post())) {

			$model->user_id=Yii::$app->user->identity->id;
			if(isset($current_contract->id)) {
                $model->contract_id = $current_contract->id;
            }



			if($model->save()){
				$model->sendBookingEmail();
        		Yii::$app->getSession()->addFlash('success', Yii::t('app','Booking saved successfully'));
				echo 'done';
				exit;
			}else{
				$errMsg='';
				if($model->getErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								if($errMsg!='')$errMsg.='<br />';
								$errMsg.=$val;
							}
						}
					}
				}
				echo $errMsg;
				exit;
			}
		}
		$searchModel=[];
		$dataProvider=[];
        if ($modelForm->load(Yii::$app->request->post())) {
            $searchModel = new BoatSearch();
			$searchModel->city_id=$modelForm->city_id;
			$searchModel->marina_id=$modelForm->marina_id;
			$searchModel->status=1;
			$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        }

        return $this->render('create', [
            'modelForm' => $modelForm,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'city_id' => $city_id,
            'marina_id' => $marina_id,
            'date' => $date,
            'saved_addons' => $saved_addons,
            'contract' => $current_contract,
            'member' => $member,
        ]);
    }

    /**
     * Deletes an existing Booking model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		$this->checkLogin();
        $this->findModel($id)->softDelete();
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Record deleted successfully'));
        return $this->redirect(Yii::$app->request->referrer ?: ['index']);
    }


    public function actionCreateinvoice()
    {

        if(Yii::$app->request->post()) {

            $post = Yii::$app->request->post();

            //Create Invoice
            $invoice = new Invoices();

            $last_invoice_number = 0;
            $invoice_number = InvoiceNumbers::find()->orderBy(['id' => SORT_DESC])->one();
            if ($invoice_number <> null) {
                $last_invoice_number = $invoice_number->number;
            }
            $price = $post['Addon']['total'];


            $vat = HelperFunction::calculate_vat($price);

            $invoice->user_id = $post['Addon']['user_id'];
            $invoice->invoice_id = $last_invoice_number + 1;  //TODO: create a table and get inovice number from there
            $invoice->contract_id = $post['Addon']['contract_id'];

            $invoice->invoice_date = date('Y-m-d');
            if($price > 0) {
            $invoice->subtotal = (string)$price;
            $invoice->tax = (string)$vat;
            $invoice->total = (string)($price + $vat);
            }else{
                $invoice->subtotal =  "0";
                $invoice->tax =  "0";
                $invoice->total = "0";
                $price = "0";
            }
            $invoice->status = "pending";
            $invoice->created_by = $post['Addon']['user_id'];
            $invoice->invoice_description = "";
            $invoice->last_payment_date = "";
            $invoice->type = "addon";
            $invoice->addon_id = $post['Addon']['addon_id'];
            $invoice->billing_cycle = '1';

            if (!$invoice->save()) {
                return false;
            }else {

                $contract_addon = ContarctAddons::find()->where(['addon_id' => $post['Addon']['addon_id'], 'contract_id' => $post['Addon']['contract_id']])->one();
                if(!empty($contract_addon)) {
                    $contract_addon->qunatity = $contract_addon->qunatity + 1;
                    // $contract_addon->save();
                    if (!$contract_addon->save()) {
                        return false;
                    } else {

                        Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Addon saved successfully'));
                        return $this->redirect(['booking/create']);
                    }
                }else{
                    $newaddon = new ContarctAddons();
                    $newaddon->contract_id = $post['Addon']['contract_id'];
                    $newaddon->customer_id = $post['Addon']['user_id'];
                    $newaddon->qunatity = 1;
                    $newaddon->price = $post['Addon']['total'];
                    $newaddon->addon_id = $post['Addon']['addon_id'];
                    if (!$newaddon->save()) {
                        return false;
                    }else {

                        Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Addon saved successfully'));
                        return $this->redirect(['booking/create']);
                    }

                }

            }
        }
    }




    /**
     * Finds the Booking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Booking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
		if(Yii::$app->user->identity->user_type==1 || Yii::$app->user->identity->user_type==2){
			$model = Booking::find()->where(['id'=>$id,'user_id'=>Yii::$app->user->identity->id,'trashed'=>0])->one();
		}else{
			$model = Booking::find()->where(['id'=>$id,'trashed'=>0])->one();
		}
        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
