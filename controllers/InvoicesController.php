<?php

namespace app\controllers;

use app\models\Contracts;
use app\models\Invoices;
use app\models\InvoicesSearch;
use app\models\RecieptsSearch;
use app\models\TermsAndConditions;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\Addon;
use app\models\ContarctAddons;
use app\models\PackageToAddon;
use yii\web\NotFoundHttpException;
use kartik\mpdf\Pdf;
use app\models\PaymentsSearch;
use app\models\Payments;

/**
 * InvoicesController implements the CRUD actions for Invoices model.
 */
class InvoicesController extends DefaultController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'view', 'update', 'delete', 'view-invoice','send-invoice-email','comming-invoices','viewpaymentdetail'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    if (Yii::$app->request->isAjax) {
                        throw new \yii\web\HttpException(401, Yii::t('app', 'Please login.'));
                    } else {
                        return $this->redirect(['site/login']);
                    }
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Invoices models.
     * @return mixed
     */
    public function actionIndex()
    {
       // $this->checkAdmin();
        $searchModel = new InvoicesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Invoices model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->checkAdmin();
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionViewpaymentdetail($id)
    {

       // $searchModel = new RecieptsSearch();
        $previous_payments= Payments::find()->where(['invoice_id' => $id])->asArray()->all();
        $paid_amount = 0;
        $discount = 0;
        if(isset($previous_payments) && $previous_payments <> null){
            foreach ($previous_payments as $key => $payment){
                $paid_amount = $paid_amount + $payment['amount'];
                $discount = $discount + $payment['discount'] ;
            }
        }
        $searchModel = new PaymentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere('invoice_id ='.$id);


        return $this->render('payment-detail', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'paid_amount' => $paid_amount,
            'discount' => $discount,
        ]);


        /*$this->checkAdmin();
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);*/
    }


    /**
     * Displays a single Invoices model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewInvoice($id, $mode = "")
    {
        $this->checkAdmin();
        $terms = TermsAndConditions::find()->where(['id' => 1])->asArray()->one();

        //$pdf = Yii::$app->pdf; // or new Pdf('utf-8', 'Letter', 0, '', 0, 0, 0, 0, 0, 0);
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            'cssInline' =>  '@media print { #cetificate_header { margin-top: 0px; }  }#cetificate_header { width: 100%;text-align: center;margin-top: 0px;}#cetificate_footer {border-right: 0.5px solid #ddd;border-bottom: 0.5px solid #ddd;}',
        ]); // or new Pdf('utf-8', 'Letter', 0, '', 0, 0, 0, 0, 0, 0);
        $mpdf = $pdf->api; // fetches mpdf api

        $model = $this->findModel($id);
        //$mpdf->cssInline = '@media print { #cetificate_header { margin-top: 0px; }  }#cetificate_header { width: 100%;text-align: center;margin-top: 0px;}#cetificate_footer {border-right: 0.5px solid #ddd;border-bottom: 0.5px solid #ddd;}';
        $mpdf->WriteHtml($this->renderPartial('invoice-view', [
            'invoice' => $this->findModel($id),
            'terms' => $terms
        ])); // call mpdf write html


        if ($mode == 'download')
            return $mpdf->Output('INV-' . $model->invoice_id.'.pdf', 'D');
        else
            return $mpdf->Output('INV-' . $model->invoice_id, 'I');


        /*return $this->render('invoice-view', [
            'invoice' => $this->findModel($id),
            'terms' => $terms
        ]);*/
    }


    /**
     * Creates a new Invoices model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->checkAdmin();
        $model = new Invoices();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information saved successfully'));
                return $this->redirect(['index']);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Invoices model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->checkAdmin();
        $model = $this->findModel($id);

        $saved_addons = ContarctAddons::find()->where(['contract_id' =>$model->contract_id])->asArray()->all();
        foreach ($saved_addons as $key=> $res) {
            $saved_addons[$key]['addon_title'] = Addon::find()->where(['id' => $res['addon_id']])->orderBy(['title' => SORT_ASC])->one()->title;
        }
        $model->saved_addons = $saved_addons;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information saved successfully'));
                return $this->redirect(['index']);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Invoices model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->checkAdmin();
        $this->findModel($id)->Delete();
        Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Record deleted successfully'));
        return $this->redirect(Yii::$app->request->referrer ?: ['index']);
    }


    /**
     * @param $member
     * @return string
     */
    public function actionSendInvoiceEmail($id, $mode = "")
    {
        $this->checkAdmin();
        $terms = TermsAndConditions::find()->where(['id' => 1])->asArray()->one();
        if($mode == 'contract'){
            $invoice_by_contract = Invoices::find()->where(['contract_id' => $id])->asArray()->one();
            $id = $invoice_by_contract['id'];
        }
        $model = $this->findModel($id);
        $model->sendInvoiceEmail($model,$terms);
        Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Invoice has been sent successfully.'));
        if($mode == 'contract'){
            return $this->redirect(['contracts/index']);
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionCommingInvoices()
    {
        $this->checkAdmin();
        $current_date = date('Y-m-d');
        $next_30_days= date('Y-m-d', strtotime($current_date. ' + 30 days'));
        $searchModel = new InvoicesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere('DATE(invoices.invoice_date) <= "'.$next_30_days.'"');
        $dataProvider->query->andWhere('invoices.status = "pending"');



        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Finds the Invoices model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invoices the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Invoices::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
