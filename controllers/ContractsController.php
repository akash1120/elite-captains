<?php

namespace app\controllers;


use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\Contracts;
use app\models\Package;
use app\models\Addon;
use app\models\ContarctAddons;
use app\models\PackageToAddon;
use app\models\ContractsSearch;
use app\models\User;
use yii\web\NotFoundHttpException;
use app\helpers\HelperFunction;
use app\models\Invoices;

/**
 * ContractsController implements the CRUD actions for Contracts model.
 */
class ContractsController extends DefaultController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','create','view','update','delete','getpackage','unfreeze','comming-renewals','checkjoiningfee'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
				'denyCallback' => function ($rule, $action) {
					if(Yii::$app->request->isAjax){
						throw new \yii\web\HttpException(401, Yii::t('app','Please login.'));
					}else{
						return $this->redirect(['site/login']);
					}
				}
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contracts models.
     * @return mixed
     */
    public function actionIndex()
    {
		//$this->checkAdmin();
        $createLink = true;
        if (Yii::$app->user->identity->user_type == 0) {
            $createLink = false;
        }
        $searchModel = new ContractsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'createLink' => $createLink,
        ]);
    }

    /**
     * Displays a single Contracts model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		//$this->checkAdmin();
		$this->checkLogin();
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Contracts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$this->checkAdmin();
        $model = new Contracts();

        if ($model->load(Yii::$app->request->post())) {

            if($model->save()){
            	Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
				return $this->redirect(['index']);
			}else{

				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								Yii::$app->getSession()->addFlash('error', $val);
							}
						}
					}
				}
			}
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Contracts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$this->checkAdmin();
        $model = $this->findModel($id);
        $city_ids = array();
        if($model->contractCities <> null && !empty($model->contractCities)){
            foreach ($model->contractCities as $city){
                $city_ids[] = $city->city_id;
            }
        }
        //$model->saved_cities = $city_ids;
        $model->saved_cities = implode(",",$city_ids);
       // $model->saved_cities = 1;


        $model->contractCities;

        //addons
        $saved_addons = ContarctAddons::find()->where(['contract_id' =>$id])->asArray()->all();
        foreach ($saved_addons as $key=> $res) {
            $saved_addons[$key]['addon_title'] = Addon::find()->where(['id' => $res['addon_id']])->orderBy(['title' => SORT_ASC])->one()->title;
            $saved_addons[$key]['addon_price'] = Addon::find()->where(['id' => $res['addon_id']])->orderBy(['title' => SORT_ASC])->one()->price;
        }
        $model->saved_addons = $saved_addons;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){

                /*on update end date , the current_expiry also updated*/
                if($model->current_expiry <> null) {
                    //$this->current_expiry = $this->end_date;
                    if(isset($model->id) && $model->id <> null){
                       /* $days = HelperFunction::dateDifference($model->current_expiry, $model->end_date);
                        if($days >= 0) {
                            $date = date('Y-m-d', strtotime($model->current_expiry . ' + ' . $days . ' days'));
                            $connection = \Yii::$app->db;
                            $connection->createCommand("update contracts set current_expiry='".$date."' where id=" . $model->id)->execute();
                        }*/

                        $days = $model->free_days - (int)$model->remaining_freeze_days;
                        if($days >= 0) {
                            $date = date('Y-m-d', strtotime($model->end_date . ' + ' . $days . ' days'));
                            $connection = \Yii::$app->db;
                            $connection->createCommand("update contracts set current_expiry='".$date."' where id=" . $model->id)->execute();
                        }


                    }

                }

            	Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
            	return $this->redirect(['index']);
			}else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								Yii::$app->getSession()->addFlash('error', $val);
							}
						}
					}
				}
			}
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Contracts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		$this->checkAdmin();
        $this->findModel($id)->Delete();
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Record deleted successfully'));
       // return $this->redirect(Yii::$app->request->referrer ?: ['index']);
        return $this->redirect(['index']);
    }
    public function actionGetpackage()
    {
        if (Yii::$app->request->get()) {
            $package_id = Yii::$app->request->get('id');
            $package_data = Package::findOne($package_id);
            $result = PackageToAddon::find()->where(['package_id' => $package_id])->asArray()->all();
            $html = '';
            $html2 = '';
            foreach ($result as $key => $res) {
                $result[$key]['addon_title'] = Addon::find()->where(['id' => $res['addon_id']])->orderBy(['title' => SORT_ASC])->one()->title;
                $result[$key]['addon_price'] = Addon::find()->where(['id' => $res['addon_id']])->orderBy(['title' => SORT_ASC])->one()->price;

                $html .= '<div class="col-xs-4 col-sm-4">
                    <div class="form-group field-contracts-addons-0-addon_name">
                      <input type="text" id="contracts-addons-' . $key . '-addon_name" class="form-control" value="' . $result[$key]['addon_title'] . '" name="Contracts[addons][' . $key . '][addon_name]" readonly>
                      <div class="help-block"></div>
                    </div>                        
                    <div class="form-group field-contracts-addons-0-addon_id">
                        <input type="text" id="contracts-addons-' . $key . '-addon_id" class="hide" value="' . $result[$key]['addon_id'] . '" name="Contracts[addons][' . $key . '][addon_id]">
                        <div class="help-block"></div>
                    </div>                    
                  </div>
                  <div class="col-xs-4 col-sm-4">
                    <div class="form-group field-contracts-addons-0-addons_quantity">
                    <input type="text" id="contracts-addons-' . $key . '-addons_quantity" class="form-control" value="' . $result[$key]['free_qty'] . '" name="Contracts[addons][' . $key . '][addons_quantity]">
                        <div class="help-block"></div>
                    </div>                   
                   </div>
                   <div class="col-xs-4 col-sm-4">
                   <div class="form-group field-contracts-addons-0-addons_price">
                       <input type="text" id="contracts-addons-' . $key . '-addons_price" class="form-control" value="' . $result[$key]['addon_price'] . '"  name="Contracts[addons][' . $key . '][addons_price]" readonly>
                        <div class="help-block"></div>
                  </div>                    
                 </div>';

            }
            $html2 = ' <label class="control-label" for="contracts-duration">Package Duration</label>
                        <select id="contracts-duration" class="form-control" name="Contracts[duration]">
                        <option value="365" data_price="' . $package_data->price . '">One Year</option>
                        <option value="182" data_price="' . $package_data->half_price . '">Half Year</option>
                       </select>';
            print_r(json_encode(['price' => $package_data->price, 'addons' => $html, 'duration' => $html2]));
            exit();
        }
    }

    public function actionCheckjoiningfee()
    {

        if (Yii::$app->request->get()) {
            $user_id =  Yii::$app->request->get('id');
            $user_data = User::findOne($user_id);
            if($user_data->new_member_check == 0){
                echo '0';
            }else{
                echo '1';
            }
        }
    }

    public function actionUnfreeze($id)
    {
        $this->checkAdmin();
        $model = $this->findModel($id);
        $model->is_freeze = 0;


            if($model->save()){
                    die('saved');
            }else{
                echo "<pre>";
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                print_r();
                                die;

                            }
                        }
                    }
                }
            }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Lists all Contracts models.
     * @return mixed
     */
    public function actionCommingRenewals()
    {
        $this->checkAdmin();
        $current_date = date('Y-m-d');
        $next_30_days= date('Y-m-d', strtotime($current_date. ' + 30 days'));
        $createLink = true;
        if (Yii::$app->user->identity->user_type == 0) {
            $createLink = false;
        }
        $searchModel = new ContractsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere('DATE(contracts.current_expiry) <= "'.$next_30_days.'"');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'createLink' => $createLink,
        ]);
    }



    /**
     * Finds the Contracts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contracts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contracts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
