<?php

namespace app\controllers;

use app\models\Contracts;
use app\models\Invoices;
use app\models\PaymentCheques;
use app\models\RecieptsSearch;
use Yii;
use app\models\Payments;
use app\models\PaymentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\TermsAndConditions;
use app\models\Reciepts;

/**
 * PaymentsController implements the CRUD actions for Payments model.
 */
class PaymentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Payments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Payments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
      public function actionView($id)
      {

          $searchModel = new RecieptsSearch();
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
          $dataProvider->query->andWhere('payment_id ='.$id);


          return $this->render('view', [
              'model' => $this->findModel($id),
              'searchModel' => $searchModel,
              'dataProvider' => $dataProvider,
          ]);
      }

    /**
     * Creates a new Payments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->checkAdmin();
        $payment = new Payments();
        if ($payment->load(Yii::$app->request->post())) {
            if ($payment->payment_method_id == '1') {
                $payment->frequency = '1';
            } else if ($payment->payment_method_id == '3') {
                $payment->frequency = $payment->number_of_cheques;
            }
            $payment->status = 'paid';
            $payment->currency = 'AED';
            if ($payment->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information saved successfully'));
                return $this->redirect(['index']);
            } else {
                if ($payment->hasErrors()) {
                    foreach ($payment->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        if ($invoice_id = Yii::$app->request->get('invoice_id')) {
            $invoice = Invoices::findOne($invoice_id);
            if ($invoice->status == 'paid') {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'This invoice is already paid.'));
                return $this->redirect(['invoices/index']);
            }
            $previous_payments= Payments::find()->where(['invoice_id' => $invoice_id])->asArray()->all();
            $paid_amount = 0;
            $discount = 0;
            if(isset($previous_payments) && $previous_payments <> null){
                foreach ($previous_payments as $key => $payment){
                    $paid_amount = $paid_amount + $payment['amount'];
                    $discount = $discount + $payment['discount'];
                }
            }

            $model = new Payments();
            $model->user_id = $invoice->user_id;
            return $this->render('create', [
                'model' => $model, 'invoice' => $invoice,'paid_amount' => $paid_amount,'discount' => $discount
            ]);


        }
        /* if ($model->load(Yii::$app->request->post()) && $model->save()) {
             return $this->redirect(['view', 'id' => $model->id]);
         }*/
    }

    /**
     * Updates an existing Payments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
      public function actionUpdate($id)
      {
          $this->checkAdmin();
          $model = $this->findModel($id);

          $invoice = Invoices::findOne($model->invoice_id);
          if ($invoice->status == 'paid') {
              Yii::$app->getSession()->addFlash('success', Yii::t('app', 'This invoice is already paid.'));
              return $this->redirect(['invoices/index']);
          }
          $previous_payments= Payments::find()->where(['invoice_id' => $model->invoice_id])->asArray()->all();
          $paid_amount = 0;
          $discount = 0;
          if(isset($previous_payments) && $previous_payments <> null){
              foreach ($previous_payments as $key => $payment){
                  $paid_amount = $paid_amount + $payment['amount'];
                  $discount = $discount + $payment['discount'];
              }
          }

          if ($model->load(Yii::$app->request->post()) && $model->save()) {
              return $this->redirect(['view', 'id' => $model->id]);
          }
          return $this->render('update', [
              'model' => $model,
              'invoice' => $invoice
              ,'paid_amount' => $paid_amount
              ,'discount' => $discount
          ]);
      }


    /**
     * Updates an existing Payments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionViewCheques($id)
    {
        $model = $this->findModel($id);
        if($model->payment_method_id == 3) {
            $saved_cheques = PaymentCheques::find()->where(['payment_id' => $id])->asArray()->all();
            $model->saved_cheques = $saved_cheques;
        }

        return $this->render('view_cheques', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing Payments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCheque($id)
    {

        $this->checkAdmin();

        if(Yii::$app->request->post()){
            $post = Yii::$app->request->post();
            if ($post['cheques'] <> null) {
                foreach ($post['cheques'] as $cheque) {

                    $cheques = PaymentCheques::findOne($cheque['id']);


                    $cheques->status = $cheque['status'];
                    if (!$cheques->save()) {
                        return false;
                    }
                    $contracts = Contracts::findOne($cheques->payment->invoice->contract_id);
                    if($cheque['status'] == 'declined'){
                        $contracts->status = 'declined';
                          if (!$contracts->save()) {

                              return false;
                          }
                    }else if($cheque['status'] != 'declined' && $cheque['status'] != 'pending'){

                        //update contract status
                        $current_date = date('y-m-d');
                        $contract = Contracts::findOne($cheques->payment->invoice->contract_id);
                        $contract_end_date = date('Y-m-d', strtotime($contract->end_date));
                        if ($contract->current_expiry <> null) {
                            $contract_end_date = date('Y-m-d', strtotime($contract->current_expiry));
                        }
                        if ((strtotime($current_date) >= strtotime($contract->start_date)) && (strtotime($current_date) <= strtotime($contract_end_date))) {
                            $contract->status = 'active';
                            if (!$contract->save()) {
                                return false;
                            }
                        }
                        $payment_data = Payments::findOne($cheques->payment_id);

                        //receipts entry

                        $model_reciepts = Reciepts::find()->where(['cheque_id' => $cheque['id']])->one();
                        if($model_reciepts == null){

                            $model_reciepts = new Reciepts();
                        }

                        $model_reciepts->payment_ref = $payment_data->payment_ref;
                        $model_reciepts->invoice_id = $payment_data->invoice_id;
                        $model_reciepts->payment_method_id = 3;
                        $model_reciepts->frequency = $payment_data->frequency;
                        $model_reciepts->currency = $payment_data->currency;
                        $model_reciepts->transaction_id = $payment_data->transaction_id;
                        $model_reciepts->status = 'paid';
                        $model_reciepts->user_id = $payment_data->user_id;
                        $model_reciepts->amount = $cheques->amount;
                        $model_reciepts->date = date('Y-m-d H:i:s');
                        $model_reciepts->payment_id = $payment_data->id;
                        $model_reciepts->cheque_number = $cheques->cheque_number;
                        $model_reciepts->cheque_date = $cheques->date;
                        $model_reciepts->cheque_id = $cheques->id;
                        $model_reciepts->cheque_status = $cheques->status;
                        if (!$model_reciepts->save()) {
                            return false;
                        }

                       /* $contracts->status = 'active';
                        if (!$contracts->save()) {
                            return false;
                        }*/
                    }
                }
            }
        }
        $model = $this->findModel($id);
        if($model->payment_method_id == 3) {
            $saved_cheques = PaymentCheques::find()->where(['payment_id' => $id])->asArray()->all();
            $model->saved_cheques = $saved_cheques;
        }
        return $this->render('update_cheques', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Payments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*  public function actionDelete($id)
      {
          $this->findModel($id)->delete();

          return $this->redirect(['index']);
      }*/

    /**
     * @param $member
     * @return string
     */
    public function actionSendReceiptEmail($id)
    {
        $this->checkAdmin();
        $terms = TermsAndConditions::find()->where(['id' => 1])->asArray()->one();
        $model = $this->findModel($id);
        $model->sendReceiptEmail($model, $terms);
        Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Receipt has been sent successfully.'));
        return $this->redirect(['payments/index']);
    }

    public function actionGetcheques()
    {
        if (Yii::$app->request->get()) {
            $no_of_cheques = Yii::$app->request->get('id');
            $amount = Yii::$app->request->get('total');

            $html = '';
            $per_cheque_amount = round($amount / $no_of_cheques);
            for ($i = 0; $i < $no_of_cheques; $i++) {

                $html .= '<div class="col-xs-4 col-sm-4">
                                        <div class="form-group field-payments-cheques-' . $i . '-cheque_number">
                                            <input type="text" id="payments-cheques-' . $i . '-cheque_number"
                                                   class="form-control " name="Payments[cheques][' . $i . '][cheque_number]" required="">
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4">
                                        <div class="form-group field-payments-cheques-' . $i . '-date">
                                            <input type="text" id="payments-cheques-' . $i . '-date" class="form-control dtpicker"
                                                   name="Payments[cheques][' . $i . '][date]" required="">
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4">
                                        <div class="form-group field-payments-cheques-' . $i . '-amount">
                                            <input type="text" id="payments-cheques-' . $i . '-amount" class="form-control price_input"
                                                   name="Payments[cheques][' . $i . '][amount]" value="" required="">
                                            <div class="help-block"></div>
                                        </div>
                                    </div>';

            }
            echo json_encode($html);
            die;
        }
    }

    /**
     * Finds the Payments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @redirects user if not admin
     */
    public function checkAdmin()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        if (Yii::$app->user->identity->user_type == 0) {
            return $this->redirect(['site/index']);
        }
    }

}
