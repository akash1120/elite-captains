<?php

namespace app\controllers;

use app\models\Contracts;
use app\models\FreezeContract;
use app\models\FreezeContractSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * FreezeContractController implements the CRUD actions for FreezeContract model.
 */
class FreezeContractController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'view', 'update', 'delete', 'pause', 'remove-days'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    if (Yii::$app->request->isAjax) {
                        throw new \yii\web\HttpException(401, Yii::t('app', 'Please login.'));
                    } else {
                        return $this->redirect(['site/login']);
                    }
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FreezeContract models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new FreezeContractSearch();
        $current_user = Yii::$app->user->identity->id;

        if (Yii::$app->user->identity->parent != 0) {
            $current_user = Yii::$app->user->identity->parent;
        }
        if (!Yii::$app->user->identity->user_type) {
            $searchModel->user_id = $current_user;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FreezeContract model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FreezeContract model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FreezeContract();
        $current_user = Yii::$app->user->identity->id;

         if( Yii::$app->user->identity->parent != 0){
             $current_user = Yii::$app->user->identity->parent;
         }
        $active_contract = Contracts::find()->where([
            'user_id' => $current_user,
            'status' => 'active'
        ])->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view',
                'id' => $model->id,
            ]);
        } else {
        }

        return $this->render('create', [
            'model' => $model,
            'contract' => $active_contract
        ]);
    }

    /**
     * Updates an existing FreezeContract model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->identity->user_type) {
            //return $this->goHome();
        }

        $model = $this->findModel($id);
        $active_contract = Contracts::find()->where([
            'id' => $model->contract_id,
            'status' => 'active'
        ])->all();
        //$contract = Contracts::findOne($model->contract_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'contract' => $active_contract
        ]);
    }

    /**
     * Deletes an existing FreezeContract model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

          $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionRemoveDays($id)
    {
        $model = $this->findModel($id);
        $model->status = 'paused';
        if (!$model->save()) {
            return false;
        }


          $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionPause($id)
    {

        if (!Yii::$app->user->identity->user_type) {
            //return $this->goHome();
        }

        $model = $this->findModel($id);
        $model->status = 'paused';
        if (!$model->save()) {
            return false;
        }
        Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Contract freeze days has been paused'));
        return $this->redirect(['index']);
    }


    /**
     * Finds the FreezeContract model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FreezeContract the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FreezeContract::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @return mixed
     */
    public function checkAdmin()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        if (Yii::$app->user->identity->user_type == 0) {
            return $this->redirect(['site/index']);
        }
    }
}
