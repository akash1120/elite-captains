<?php

namespace app\controllers;

use app\models\Contracts;
use app\models\Invoices;
use app\models\PaymentCheques;
use Yii;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;
use app\models\Booking;
use app\models\Package;
use app\models\City;
use app\models\Marina;
use app\models\Boat;
use app\models\User;

class DefaultController extends Controller
{
    /**
     * Change Status of record .
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionStatus($id)
    {
		$this->checkLogin();
		$this->checkAdmin();
		$model=$this->findModel($id);
        if($model!=null){
			if($model->status==0){
				$type=1;
			}else{
				$type=0;
			}
			$connection = \Yii::$app->db;
			$connection->createCommand("update ".$model->tableName()." set status=:utype where id=:id",[':utype'=>$type, ':id'=>$model->id])->execute();
			die("finish");
		}
    }
	
    /**
     * @redirects user if not login
     */
	public function checkLogin()
	{
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
	}
	
    /**
     * @redirects user if not admin
     */
	public function checkAdmin()
	{
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
		if(Yii::$app->user->identity->user_type==0){
            return $this->redirect(['site/index']);
		}
	}
	
    /**
     * @redirects user if not super admin
     */
	public function checkSuperAdmin()
	{
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
		if(Yii::$app->user->identity->user_type!=2){
            return $this->redirect(['site/index']);
		}
	}
	
	/**
	 * Generate Token
	 */
	public function generateToken($length)
	{
		$key = '';
		$keys = array_merge(range(0, 9), range('a', 'z'));
	
		for ($i = 0; $i < $length; $i++) {
			$key .= $keys[array_rand($keys)];
		}
	
		return $key;
	}
	
    /**
     * generates randon name string
     *
     * @return string
     */
	public function generateName()
	{
		return str_replace(array(" ","."),"",microtime());
	}
	
    /**
     * check if given date is week end
     * @return boolean
     */
	public function isWeekend($date)
	{
		$weekDay = date('w', strtotime($date));
		return ($weekDay == 5 || $weekDay == 6);
	}
	
    /**
     * calculate the number of day in the week
     * @return string
     */
	public function WeekendDay($date)
	{
		$weekDay = date('w', strtotime($date));
		return $weekDay;
	}
	
    /**
     * calculate start and end date of week for given date
     * @return array
     */
	public function getWeekStartEnd($date)
	{
		$whichDay=$this->WeekendDay($date);
		list($y,$m,$d)=explode("-",$date);
		return [
			date("Y-m-d",mktime(0,0,0,$m,($d-$whichDay),$y)),//Week Start Date
			date("Y-m-d",mktime(0,0,0,$m,(($d-$whichDay)+6),$y)),//Week End Date
		];
	}
	
    /**
     * calculate admin dashboard stats
     * @return array
     */
	public function getAdminStats()
	{
		$allBookings = Booking::find()->where(['trashed'=>0])->count('id');
		$futureBookings = Booking::find()->where(['and',['>=','DATE(booking_date)',date("Y-m-d")],['trashed'=>0]])->count('id');
		$pastBookings = Booking::find()->where(['and',['<','DATE(booking_date)',date("Y-m-d")],['trashed'=>0]])->count('id');
		$allPackages = Package::find()->where(['trashed'=>0])->count('id');
		$allCities = City::find()->where(['trashed'=>0])->count('id');
		$allMarinas = Marina::find()->where(['trashed'=>0])->count('id');
		$allBoats = Boat::find()->where(['trashed'=>0])->count('id');
		$allUsers = User::find()->where(['user_type'=>0,'trashed'=>0])->count('id');
        $current_date = date('Y-m-d');
        $next_30_days= date('Y-m-d', strtotime($current_date. ' + 30 days'));
        $allUpcomingRenewals = Contracts::find()->where(['and',['<=','DATE(current_expiry)',$next_30_days]])->count('id');
        $allUpcomingCheques = PaymentCheques::find()->where(['and',['<=','DATE(date)',$next_30_days]])->count('id');
        $allUpcomingInvoices = Invoices::find()->where(['and',['<=','DATE(invoice_date)',$next_30_days]])->count('id');

		return [
			'allBookings'=>$allBookings,
			'futureBookings'=>$futureBookings,
			'pastBookings'=>$pastBookings,
			'allPackages'=>$allPackages,
			'allCities'=>$allCities,
			'allMarinas'=>$allMarinas,
			'allBoats'=>$allBoats,
			'allUsers'=>$allUsers,
			'allCommingRenewals'=>$allUpcomingRenewals,
			'allUpcomingCheques'=>$allUpcomingCheques,
			'allUpcomingInvoices'=>$allUpcomingInvoices,
		];
	}
}
