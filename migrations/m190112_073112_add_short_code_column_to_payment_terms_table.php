<?php

use yii\db\Migration;

/**
 * Handles adding short_code to table `{{%payment_terms}}`.
 */
class m190112_073112_add_short_code_column_to_payment_terms_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment_terms','short_code', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
