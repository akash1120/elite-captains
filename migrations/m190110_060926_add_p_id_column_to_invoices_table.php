<?php

use yii\db\Migration;

/**
 * Handles adding p_id to table `{{%invoices}}`.
 */
class m190110_060926_add_p_id_column_to_invoices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('invoices','p_id', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
