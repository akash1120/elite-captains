<?php

use yii\db\Migration;

/**
 * Class m181230_161821_update_contract_addons
 */
class m181230_161821_update_contract_addons extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('contarct_addons','addon_id',$this->integer(10)->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m181230_161821_update_contract_addons cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181230_161821_update_contract_addons cannot be reverted.\n";

        return false;
    }
    */
}
