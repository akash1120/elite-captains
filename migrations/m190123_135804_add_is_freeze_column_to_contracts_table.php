<?php

use yii\db\Migration;

/**
 * Handles adding is_freeze to table `{{%contracts}}`.
 */
class m190123_135804_add_is_freeze_column_to_contracts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contracts','is_freeze', $this->tinyInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
