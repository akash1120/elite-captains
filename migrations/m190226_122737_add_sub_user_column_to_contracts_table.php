<?php

use yii\db\Migration;

/**
 * Handles adding sub_user to table `{{%contracts}}`.
 */
class m190226_122737_add_sub_user_column_to_contracts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contracts','sub_user', $this->Integer()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
