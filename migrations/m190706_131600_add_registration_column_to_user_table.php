<?php

use yii\db\Migration;

/**
 * Handles adding registration to table `{{%user}}`.
 */
class m190706_131600_add_registration_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user','resgistration_no', $this->string()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
