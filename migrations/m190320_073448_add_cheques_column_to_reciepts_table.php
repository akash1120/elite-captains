<?php

use yii\db\Migration;

/**
 * Handles adding cheques to table `{{%reciepts}}`.
 */
class m190320_073448_add_cheques_column_to_reciepts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('reciepts','cheque_number', $this->string()->defaultValue(null));
        $this->addColumn('reciepts','cheque_date', $this->string()->defaultValue(null));
        $this->addColumn('reciepts','cheque_id', $this->integer()->defaultValue(null));
        $this->addColumn('reciepts','cheque_status', "ENUM('pending', 'paid','cash','online') NOT NULL DEFAULT 'pending'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
