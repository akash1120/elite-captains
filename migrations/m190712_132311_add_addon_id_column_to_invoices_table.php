<?php

use yii\db\Migration;

/**
 * Handles adding addon_id to table `{{%invoices}}`.
 */
class m190712_132311_add_addon_id_column_to_invoices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('invoices','addon_id', $this->integer()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
