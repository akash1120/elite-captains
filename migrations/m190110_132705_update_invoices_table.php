<?php

use yii\db\Migration;

/**
 * Class m190110_132705_update_invoices_table
 */
class m190110_132705_update_invoices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('invoices','payment_reference', $this->string());
        $this->addColumn('invoices','pt_customer_email', $this->string());
        $this->addColumn('invoices','pt_customer_password', $this->string());
        $this->addColumn('invoices','pt_token', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190110_132705_update_invoices_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190110_132705_update_invoices_table cannot be reverted.\n";

        return false;
    }
    */
}
