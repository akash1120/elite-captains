<?php

use yii\db\Migration;

/**
 * Handles adding frequency to table `{{%contracts}}`.
 */
class m190112_084707_add_frequency_column_to_contracts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contracts','frequency', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
