<?php

use yii\db\Migration;

/**
 * Handles adding recurring_amount to table `{{%contracts}}`.
 */
class m190124_084539_add_recurring_amount_column_to_contracts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contracts','recurring_amount', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
