<?php

use yii\db\Migration;

/**
 * Handles adding current_expiry to table `{{%contracts}}`.
 */
class m190123_134556_add_current_expiry_column_to_contracts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contracts','current_expiry', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
