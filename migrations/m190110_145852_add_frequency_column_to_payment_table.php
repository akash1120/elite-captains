<?php

use yii\db\Migration;

/**
 * Handles adding frequency to table `{{%payment}}`.
 */
class m190110_145852_add_frequency_column_to_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payments','transaction_id', $this->string());
        $this->addColumn('payments','', $this->string());
        $this->addColumn('payments','currency', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
