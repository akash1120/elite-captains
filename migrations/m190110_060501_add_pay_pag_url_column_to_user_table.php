<?php

use yii\db\Migration;

/**
 * Handles adding pay_pag_url to table `{{%user}}`.
 */
class m190110_060501_add_pay_pag_url_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user','pay_page_url', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
