<?php

use yii\db\Migration;

/**
 * Handles adding pause_date to table `{{%freeze_contract}}`.
 */
class m190227_085330_add_pause_date_column_to_freeze_contract_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('freeze_contract','pause_date', $this->string()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
