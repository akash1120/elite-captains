<?php

use yii\db\Migration;

/**
 * Class m181231_063431_create_terms_and_conditions
 */
class m181231_063431_create_terms_and_conditions extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('terms_and_conditions', [
            'id' => $this->primaryKey(),
            'terms' => $this->text()->defaultValue(null),
            'client_notes' => $this->text()->defaultValue(null),
            'admin_notes' =>  $this->text()->defaultValue(null),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m181231_063431_create_terms_and_conditions cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181231_063431_create_terms_and_conditions cannot be reverted.\n";

        return false;
    }
    */
}
