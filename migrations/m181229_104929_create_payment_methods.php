<?php

use yii\db\Migration;

/**
 * Class m181229_104929_create_payment_methods
 */
class m181229_104929_create_payment_methods extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('payment_methods', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'short_code' => $this->string()->defaultValue(null),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m181229_104929_create_payment_methods cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181229_104929_create_payment_methods cannot be reverted.\n";

        return false;
    }
    */
}
