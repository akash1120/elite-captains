<?php

use yii\db\Migration;

/**
 * Handles adding contract to table `{{%booking}}`.
 */
class m190710_111047_add_contract_column_to_booking_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('booking','contract_id', $this->integer()->defaultValue(0));
        $this->addColumn('booking','captain', $this->integer()->defaultValue(0));
        $this->addColumn('booking','food', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
