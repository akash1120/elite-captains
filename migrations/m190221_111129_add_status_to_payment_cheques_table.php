<?php

use yii\db\Migration;

/**
 * Class m190221_111129_add_status_to_payment_cheques_table
 */
class m190221_111129_add_status_to_payment_cheques_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment_cheques','status', "ENUM('pending', 'paid','cash','online') NOT NULL DEFAULT 'pending'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190221_111129_add_status_to_payment_cheques_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190221_111129_add_status_to_payment_cheques_table cannot be reverted.\n";

        return false;
    }
    */
}
