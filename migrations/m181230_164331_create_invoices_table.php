<?php

use yii\db\Migration;

/**
 * Handles the creation of table `invoices`.
 */
class m181230_164331_create_invoices_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('invoices', [
            'id' => $this->primaryKey(),
            'user_id' =>  $this->integer()->notNull(),
            'invoice_id' =>  $this->integer()->notNull(),
            'contract_id' =>  $this->integer()->notNull(),
            'invoice_date' =>  $this->dateTime(),
            'subtotal' =>  $this->string()->notNull(),
            'tax' =>  $this->string()->notNull(),
            'total' =>  $this->string()->notNull(),
            'status' => "ENUM('pending', 'paid','cancelled') NOT NULL DEFAULT 'pending'",
            'created_by' => $this->integer()->notNull(),
            'invoice_description' => $this->string()->defaultValue(null),
            'admin_notes' => $this->string()->defaultValue(null),
            'client_notes' => $this->string()->defaultValue(null),
            'invoice_terms' =>  $this->string()->defaultValue(null),
            'billing_cycle' =>  $this->string()->defaultValue(null),
            'last_payment_date' =>  $this->dateTime(),
            'update_at' => $this->timestamp()->notNull(),
            'create_at' => $this->timestamp()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('invoices');
    }
}
