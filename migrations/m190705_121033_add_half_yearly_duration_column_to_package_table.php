<?php

use yii\db\Migration;

/**
 * Handles adding half_yearly_duration to table `{{%package}}`.
 */
class m190705_121033_add_half_yearly_duration_column_to_package_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('package','half_price', $this->integer()->defaultValue(0));
        $this->addColumn('package','half_year_duration', $this->integer()->defaultValue(182));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
