<?php

use yii\db\Migration;

/**
 * Class m181229_104946_create_payment_terms
 */
class m181229_104946_create_payment_terms extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('payment_terms', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'no_of_days' =>  $this->integer()->notNull()->defaultValue(0),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m181229_104946_create_payment_terms cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181229_104946_create_payment_terms cannot be reverted.\n";

        return false;
    }
    */
}
