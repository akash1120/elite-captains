<?php

use yii\db\Migration;

/**
 * Handles adding pt_invoice_id to table `{{%payments}}`.
 */
class m190110_140258_add_pt_invoice_id_column_to_payments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payments','pt_invoice_id', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
