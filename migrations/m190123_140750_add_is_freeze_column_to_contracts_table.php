<?php

use yii\db\Migration;

/**
 * Handles adding is_freeze to table `{{%contracts}}`.
 */
class m190123_140750_add_is_freeze_column_to_contracts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contracts','is_freeze', $this->tinyInteger()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
