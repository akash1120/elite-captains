<?php

use yii\db\Migration;

/**
 * Handles adding registration_no to table `{{%user}}`.
 */
class m190706_134814_add_registration_no_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user','registration_no', $this->string()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
