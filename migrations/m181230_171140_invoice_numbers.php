<?php

use yii\db\Migration;

/**
 * Class m181230_171140_invoice_numbers
 */
class m181230_171140_invoice_numbers extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('invoice_numbers', [
            'id' => $this->primaryKey(),
            'number' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m181230_171140_invoice_numbers cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181230_171140_invoice_numbers cannot be reverted.\n";

        return false;
    }
    */
}
