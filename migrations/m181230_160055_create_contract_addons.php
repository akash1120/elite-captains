<?php

use yii\db\Migration;

/**
 * Class m181230_160055_create_contract_addons
 */
class m181230_160055_create_contract_addons extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('contarct_addons', [
            'id' => $this->primaryKey(),
            'contract_id' => $this->integer()->notNull(),
            'customer_id' =>  $this->integer()->notNull(),
            'qunatity' =>  $this->integer()->notNull()->defaultValue(1),
            'price' =>  $this->string()->notNull()->defaultValue(0),
        ]);

       /* $this->addForeignKey(
            'fk_contract_id',
            'contarct_addons',
            'contract_id',
            'Contracts',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_customer_id',
            'contarct_addons',
            'customer_id',
            'user',
            'id',
            'CASCADE'
        );*/
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m181230_160055_create_contract_addons cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181230_160055_create_contract_addons cannot be reverted.\n";

        return false;
    }
    */
}
