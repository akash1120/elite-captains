<?php

use yii\db\Migration;

/**
 * Class m181230_165228_update_Contracts_table
 */
class m181230_165228_update_Contracts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('Contracts','terms', $this->text());
        $this->addColumn('Contracts','client_notes', $this->text());
        $this->addColumn('Contracts','admin_notes', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m181230_165228_update_Contracts_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181230_165228_update_Contracts_table cannot be reverted.\n";

        return false;
    }
    */
}
