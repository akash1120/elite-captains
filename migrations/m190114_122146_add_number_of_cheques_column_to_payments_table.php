<?php

use yii\db\Migration;

/**
 * Handles adding number_of_cheques to table `{{%payments}}`.
 */
class m190114_122146_add_number_of_cheques_column_to_payments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payments','number_of_cheques', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
