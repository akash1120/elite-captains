<?php

use yii\db\Migration;

/**
 * Handles adding vat to table `{{%user}}`.
 */
class m190113_080959_add_vat_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user','vat_number', $this->string());
        $this->addColumn('user','first_name', $this->string());
        $this->addColumn('user','last_name', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
