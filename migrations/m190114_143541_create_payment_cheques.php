<?php

use yii\db\Migration;

/**
 * Class m190114_143541_create_payment_cheques
 */
class m190114_143541_create_payment_cheques extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('payment_cheques', [
            'id' => $this->primaryKey(),
            'payment_id' =>  $this->integer()->notNull(),
            'customer_id' =>  $this->integer()->notNull(),
            'invoice_id' =>  $this->integer()->notNull(),
            'cheque_number' => $this->string(),
            'date' =>  $this->dateTime(),
            'amount' => $this->string(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190114_143541_create_payment_cheques cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190114_143541_create_payment_cheques cannot be reverted.\n";

        return false;
    }
    */
}
