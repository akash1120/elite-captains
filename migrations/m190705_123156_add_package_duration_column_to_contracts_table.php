<?php

use yii\db\Migration;

/**
 * Handles adding package_duration to table `{{%contracts}}`.
 */
class m190705_123156_add_package_duration_column_to_contracts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contracts','duration', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
