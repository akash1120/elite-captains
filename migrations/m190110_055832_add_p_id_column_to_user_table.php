<?php

use yii\db\Migration;

/**
 * Handles adding p_id to table `{{%user}}`.
 */
class m190110_055832_add_p_id_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user','p_id', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
