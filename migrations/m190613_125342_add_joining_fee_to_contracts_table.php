<?php

use yii\db\Migration;

/**
 * Class m190613_125342_add_joining_fee_to_contracts_table
 */
class m190613_125342_add_joining_fee_to_contracts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contracts','joining_fee', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190613_125342_add_joining_fee_to_contracts_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190613_125342_add_joining_fee_to_contracts_table cannot be reverted.\n";

        return false;
    }
    */
}
