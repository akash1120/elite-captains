<?php

use yii\db\Migration;

/**
 * Handles dropping is_freeze from table `{{%contracts}}`.
 */
class m190123_140642_drop_is_freeze_column_from_contracts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('contracts', 'is_freeze');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
