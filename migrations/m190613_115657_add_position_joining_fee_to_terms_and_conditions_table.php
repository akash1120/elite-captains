<?php

use yii\db\Migration;

/**
 * Class m190613_115657_add_position_joining_fee_to_terms_and_conditions_table
 */
class m190613_115657_add_position_joining_fee_to_terms_and_conditions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('terms_and_conditions','joining_fee', $this->integer()->defaultValue(0));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190613_115657_add_position_joining_fee_to_terms_and_conditions_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190613_115657_add_position_joining_fee_to_terms_and_conditions_table cannot be reverted.\n";

        return false;
    }
    */
}
