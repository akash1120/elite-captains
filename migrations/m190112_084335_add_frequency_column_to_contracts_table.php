<?php

use yii\db\Migration;

/**
 * Handles adding frequency to table `{{%contracts}}`.
 */
class m190112_084335_add_frequency_column_to_contracts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
