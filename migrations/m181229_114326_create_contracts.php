<?php

use yii\db\Migration;

/**
 * Class m181229_114326_create_Contracts
 */
class m181229_114326_create_Contracts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('Contracts', [
            'id' => $this->primaryKey(),
            'user_id' =>  $this->integer()->notNull(),
            'package_id' =>  $this->integer()->notNull(),
            'start_date' =>  $this->dateTime(),
            'end_date' =>  $this->dateTime(),
            'payment_method_id' =>  $this->integer()->notNull(),
            'payment_term_id' =>  $this->integer()->notNull(),
            'payment_term_id' =>  $this->integer()->notNull(),
            'check_number' => $this->string()->defaultValue(null),
            'check_date' => $this->dateTime()->defaultValue(null),
            'card_number' => $this->string()->defaultValue(null),
            'card_expiry_date' => $this->dateTime()->defaultValue(null),
            'cvv' => $this->string()->defaultValue(null),
            'free_days' =>  $this->integer()->notNull()->defaultValue(0),
            'status' =>  "ENUM('pending', 'active','closed') NOT NULL DEFAULT 'pending'",
            'created_by' =>  $this->integer()->notNull(),
            'update_at' => $this->timestamp()->notNull(),
            'create_at' => $this->timestamp()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m181229_114326_create_Contracts cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181229_114326_create_Contracts cannot be reverted.\n";

        return false;
    }
    */
}
