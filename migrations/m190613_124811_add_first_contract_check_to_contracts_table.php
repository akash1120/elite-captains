<?php

use yii\db\Migration;

/**
 * Class m190613_124811_add_first_contract_check_to_contracts_table
 */
class m190613_124811_add_first_contract_check_to_contracts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contracts','first_contract_check', $this->smallInteger()->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190613_124811_add_first_contract_check_to_contracts_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190613_124811_add_first_contract_check_to_contracts_table cannot be reverted.\n";

        return false;
    }
    */
}
