<?php

use yii\db\Migration;

/**
 * Class m190613_122408_add_position_new_member_check_to_user_table
 */
class m190613_122408_add_position_new_member_check_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user','new_member_check', $this->smallInteger()->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190613_122408_add_position_new_member_check_to_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190613_122408_add_position_new_member_check_to_user_table cannot be reverted.\n";

        return false;
    }
    */
}
