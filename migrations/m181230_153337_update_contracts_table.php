<?php

use yii\db\Migration;

/**
 * Class m181230_153337_update_Contracts_table
 */
class m181230_153337_update_Contracts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('Contracts', 'price', $this->string(64)->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m181230_153337_update_Contracts_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181230_153337_update_Contracts_table cannot be reverted.\n";

        return false;
    }
    */
}
