<?php

use yii\db\Migration;

/**
 * Handles adding postal_code to table `{{%user}}`.
 */
class m190109_080249_add_postal_code_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user','postal_code', $this->string());
        $this->addColumn('user','address', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
