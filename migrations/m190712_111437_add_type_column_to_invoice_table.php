<?php

use yii\db\Migration;

/**
 * Handles adding type to table `{{%invoice}}`.
 */
class m190712_111437_add_type_column_to_invoice_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('invoices','type', "ENUM('contract', 'addon') NOT NULL DEFAULT 'contract'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
