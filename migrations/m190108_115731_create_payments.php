<?php

use yii\db\Migration;

/**
 * Class m190108_115731_create_payments
 */
class m190108_115731_create_payments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('payments', [
            'id' => $this->primaryKey(),
            'payment_ref' =>  $this->string(64)->notNull(),
            'invoice_id' =>  $this->integer()->notNull(),
            'trax_ref' => $this->string(64)->notNull(),
            'status' =>  "ENUM('paid', 'cancelled','declined') NOT NULL",
            'payment_method_id' =>  $this->integer()->notNull(),
            'user_id' =>  $this->integer()->notNull(),
            'received_by' =>  $this->integer()->notNull(),
            'amount' =>  $this->string(64)->notNull(),
            'note' => $this->text()->defaultValue(null),
            'date' =>  $this->dateTime(),
            'response_code' => $this->string()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190108_115731_create_payments cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190108_115731_create_payments cannot be reverted.\n";

        return false;
    }
    */
}
